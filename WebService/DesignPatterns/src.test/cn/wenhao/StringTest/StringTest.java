package cn.wenhao.StringTest;

import org.junit.Test;

public class StringTest {

	@Test
	public void test() {
		String str = new String("abc");
		String str1 = str.intern();
		String str2 = "abc";
		String str3 = new String("abc");
		System.out.println(str == str1);
		System.out.println(str1 == str2);
		System.out.println(str2 == str3);
		System.out.println(str3 == str);
	}

	@Test
	public void test2() {
		String s1 = new String("abc");
		String s2 = s1.intern();
		String s3 = "abc";
		System.out.println(s1 == s2); // false
		System.out.println(s2 == s3); // true
		System.out.println(s1 == s3); // false

	}

}
