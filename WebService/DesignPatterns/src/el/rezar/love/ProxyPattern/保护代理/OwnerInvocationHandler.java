package el.rezar.love.ProxyPattern.��������;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class OwnerInvocationHandler implements InvocationHandler {
	private PersonBean pb;

	public OwnerInvocationHandler(PersonBean pb) {
		this.pb = pb;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {

		try {
			String name = method.getName();
			if (name.startsWith("get") || !name.equals("setHotOrNotRating")) {
				return method.invoke(this.pb, args);
			} else {
				throw new IllegalAccessException("can not visit this method !");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

}
