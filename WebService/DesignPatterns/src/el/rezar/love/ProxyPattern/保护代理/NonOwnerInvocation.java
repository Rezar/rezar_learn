package el.rezar.love.ProxyPattern.保护代理;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 代理逻辑的执行器，用于访问非自身的一些行为
 * 
 * @author Administrator
 * 
 */
public class NonOwnerInvocation implements InvocationHandler {

	private PersonBean pb;

	// private static NonOwnerInvocation ni;

	// private static class TempClass {
	// NonOwnerInvocation noi = new NonOwnerInvocation();
	// }

	public NonOwnerInvocation(PersonBean pb) {
		this.pb = pb;
	}

	// public static void getInstance(PersonBean pb) {
	// if (ni == null) {
	// synchronized (NonOwnerInvocation.class) {
	// if (ni == null) {
	// ni = new TempClass().noi;
	// }
	// }
	// } else {
	//
	// }
	// }

	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {

		try {
			String methodName = method.getName();
			if (methodName.startsWith("set")
					&& !methodName.equals("setHotOrNotRating")) {
				throw new IllegalAccessException();
			} else {
				return method.invoke(this.pb, args);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
