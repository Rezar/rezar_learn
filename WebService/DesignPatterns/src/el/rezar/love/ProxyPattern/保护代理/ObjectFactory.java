package el.rezar.love.ProxyPattern.��������;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class ObjectFactory {

	public static PersonBean getPersonBean(PersonBean pb, InvocationHandler ih) {
		Class<? extends PersonBean> clazz = pb.getClass();
		return (PersonBean) Proxy.newProxyInstance(clazz.getClassLoader(),
				clazz.getInterfaces(), ih);
	}
}
