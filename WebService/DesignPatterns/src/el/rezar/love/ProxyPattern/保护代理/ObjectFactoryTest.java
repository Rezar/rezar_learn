package el.rezar.love.ProxyPattern.��������;

import java.lang.reflect.InvocationHandler;

import org.junit.Test;

public class ObjectFactoryTest {

	@Test
	public void testGetPersonBean() {
		PersonBean pb = new PersonBeanImpl("Rezar", "M", "Music", 9);
		InvocationHandler ih = new OwnerInvocationHandler(pb);
		pb = ObjectFactory.getPersonBean(pb, ih);
		// pb.setHotOrNotRating(10);
		pb.getGender();
		pb.getName();
	}

	@Test
	public void testGetPersonBean2() {
		PersonBean pb = new PersonBeanImpl("Rezar", "M", "Music", 9);
		InvocationHandler ih = new NonOwnerInvocation(pb);
		pb = ObjectFactory.getPersonBean(pb, ih);
		pb.setHotOrNotRating(10);
		pb.getGender();
		pb.getName();
		pb.setGender("FM");
	}

}
