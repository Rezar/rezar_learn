package el.rezar.love.ProxyPattern.�������;

import java.awt.Component;
import java.awt.Graphics;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class ImageProxy implements Icon {

	URL imageURL;
	ImageIcon imageIcon;
	Thread retrievalThread;
	boolean retrieving = false;

	public ImageProxy(URL cdUrl) {
		this.imageURL = cdUrl;
	}

	@Override
	public void paintIcon(final Component c, Graphics g, int x, int y) {

		if (this.imageIcon != null) {
			this.imageIcon.paintIcon(c, g, x, y);
		} else {
			g.drawString("Loading CD cover", x + 300, y + 190);
			if (!retrieving) {
				this.retrieving = true;
				this.retrievalThread = new Thread(new Runnable() {

					@Override
					public void run() {
						ImageProxy.this.imageIcon = new ImageIcon(imageURL,
								"CD Cover");
						c.repaint();
					}
				});
				this.retrievalThread.start();
			}
		}

	}

	@Override
	public int getIconWidth() {
		if (this.imageIcon != null) {
			return this.imageIcon.getIconWidth();
		} else
			return 800;
	}

	@Override
	public int getIconHeight() {
		if (this.imageIcon != null) {
			return this.imageIcon.getIconHeight();
		} else
			return 600;
	}

}
