package el.rezar.love.ProxyPattern.�������;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class ImageProxyTestDrive {

	ImageComponent imageComponent;
	JFrame frame = new JFrame("CD Cover Viewer");
	JMenuBar menuBar;
	JMenu menu;
	Hashtable<String, String> cds = new Hashtable<String, String>();

	public static void main(String[] args) throws Exception {
		new ImageProxyTestDrive();
	}

	public ImageProxyTestDrive() throws MalformedURLException {

		cds.put("Ambient: Music for airports",
				"http://images.cnitblog.com/blog/159936/201307/08121626-9d4dea10762a482f8813a4df931f4000.png");
		cds.put("BaiDu: Music for airports",
				"http://pic4.nipic.com/20090903/2125404_132352014851_2.jpg");
		cds.put("BaiDu: Music for airports2",
				"http://f5.topit.me/5/7f/f0/11148103452cff07f5o.jpg");

		URL initialURL = new URL(cds.get("Ambient: Music for airports"));
		menuBar = new JMenuBar();
		menu = new JMenu("Favorite CDs");
		menuBar.add(menu);
		frame.setJMenuBar(menuBar);

		for (Enumeration<String> e = cds.keys(); e.hasMoreElements();) {
			String name = e.nextElement();
			JMenuItem menuItem = new JMenuItem(name);
			menu.add(menuItem);
			menuItem.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					imageComponent.setIcon(new ImageProxy(getCDUrl(e
							.getActionCommand())));
				}

			});
		}

		Icon icon = new ImageProxy(initialURL);
		imageComponent = new ImageComponent(icon);
		frame.getContentPane().add(imageComponent);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		frame.setVisible(true);

	}

	private URL getCDUrl(String actionCommand) {

		try {
			return new URL(cds.get(actionCommand));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
