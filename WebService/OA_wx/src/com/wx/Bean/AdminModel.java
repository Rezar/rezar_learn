package com.wx.Bean;

/**
 * 管理员实体域对象
 * 
 * @author Administrator
 * 
 */
public class AdminModel {

	private int id;
	private String name;
	private String registerDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

}
