package el.Rezar.love.Array;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ArrayOfGenerics {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		List<String>[] ls = null;
		List<String>[] la = new List[10];
		ls = (List<String>[]) la;

		ls[0] = new ArrayList<String>();

		ls[0].add("abc");
		System.out.println(ls[0].get(0));

		// List<String> is a subtype of Object , so assignment is ok
		Object[] objs = ls;

		// compiles and runs without complaint
		List<Integer> list = new ArrayList<Integer>();
		list.add(23);
		objs[1] = list;

		List<Integer> listRet = (List<Integer>) objs[1];
		System.out.println(listRet.get(0));

	}

	@Test
	public void test() {
		Object o = new Integer[] { 1, 2, 3 };
		if (o.getClass().isArray()) {
			if (o.getClass().getComponentType().isPrimitive()) {
				System.out.println("基本数据类型");
			} else {
				System.out.println("引用数据类型");
			}
		}
	}

	@Test
	public void test2() {
		int[] intArray = new int[10];
		intArray[0] = 12;
		test(intArray);
		System.out.println(intArray[0]);
		Class<?> cla1 = new Integer[4].getClass();
		Class<?> cla2 = new Object[3].getClass();
		System.out.println(cla1);
		System.out.println(cla2);
		System.out.println(cla1 == cla2);
	}

	public void test(Object args) {
		if (args.getClass().isArray())
			Array.set(args, 0, 34);
	}

}
