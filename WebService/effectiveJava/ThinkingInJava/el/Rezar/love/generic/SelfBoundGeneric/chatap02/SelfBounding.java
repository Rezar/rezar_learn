package el.Rezar.love.generic.SelfBoundGeneric.chatap02;

import java.util.Collection;

public class SelfBounding {

	public <T> void fill(Collection<T> coll, T... ts) {
		if (coll == null) {
			throw new IllegalArgumentException(
					"collection coll can not be null!");
		}
		for (int i = 0; i < ts.length; i++) {
			coll.add(ts[i]);
		}
	}

	public static void main(String[] args) {
		A a = new A();
		a.set(new A());
		a = a.set(new A()).get();
		SelfBounded<A> a_ = a.set(a);
		// output is true
		System.out.println(a_ == a);
		C c = new C();
		c = c.setAndGet(new C()).get();

		F f = new F();
		@SuppressWarnings({ "unchecked", "rawtypes" })
		SelfBounded f_ = f.set(f); // ���ص�Ϊ�����ͣ���SelfBounded
		SelfBounded<?> f__ = f_.get(); // ���ص�ΪSelfBounded,���Ͳ�������ʾ��ΪSelfBounded
		System.out.println(f_ == f__);

	}

}

class A extends SelfBounded<A> {
}

class B extends SelfBounded<B> {
}

class C extends SelfBounded<C> {
	C setAndGet(C arg) {
		set(arg);
		return get();
	}
}

class D {
}

/**
 * Error:<br/>
 * Bound mismatch: The type D is not a valid substitute for the bounded
 * parameter <T extends SelfBounded<T>> of the type SelfBounded<T>
 * 
 * @author Administrator
 * 
 */
// class E extends SelfBounded<D> {
// }

// but you can also do this
class F extends SelfBounded {
}
