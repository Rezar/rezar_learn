package el.Rezar.love.generic.SelfBoundGeneric;

public class BasicHolder<T> {

	T element;

	void set(T ele) {
		this.element = ele;
	}

	T get() {
		return this.element;
	}

	void f() {
		System.out.println(this.getClass().getSimpleName());
	}

}
