package el.Rezar.love.generic.SelfBoundGeneric.chatap02;

public class SelfBounded<T extends SelfBounded<T>> {

	T element;

	SelfBounded<T> set(T ele) {
		this.element = ele;
		return this;
	}

	T get() {
		return this.element;
	}

}
