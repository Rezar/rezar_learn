package el.Rezar.love.generic.GenericsParaminterface;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import el.Rezar.love.Utils.CollectionUtils;

public class CollectionUtilsTest {

	static void f(Map<String, List<Integer>> infos) {
		System.out.println(infos.getClass());
	}

	@Test
	public void test() {
		/**
		 * ��������ʽ������˵�����ڵ���ĳ�����ͷ�����ʱ��ָ����ȷ�Ĳ�������
		 */
		f(CollectionUtils.<String, List<Integer>> map());
	}

	@Test
	public void test2() {
		List<Integer> list = CollectionUtils.makeList(new Integer[] { 1, 2, 3,
				4, 5, 6 });
		System.out.println(list);
	}

	@Test
	public void test3() {
		Collection<Coffee> coffees = CollectionUtils.fill(
				CollectionUtils.<Coffee> list(), new CoffeeGenerator(), 5);
		for(Coffee coffee : coffees){
			System.out.println(coffee);
		}
	}

}
