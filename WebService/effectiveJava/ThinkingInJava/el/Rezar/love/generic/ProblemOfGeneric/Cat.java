package el.Rezar.love.generic.ProblemOfGeneric;

/**
 * Error : The interface Comparable cannot be implemented more than once with
 * different arguments: Comparable<ComparablePet> and Comparable<Cat><br/>
 * if the interface is Comparable<Cat>
 * 
 * @author Administrator
 * 
 */
public class Cat extends ComparablePet implements Comparable<ComparablePet> {

	@Override
	public int compareTo(ComparablePet o) {
		return super.compareTo(o);
	}

}
