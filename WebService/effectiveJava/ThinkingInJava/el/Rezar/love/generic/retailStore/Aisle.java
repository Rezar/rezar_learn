package el.Rezar.love.generic.retailStore;

import java.util.ArrayList;

public class Aisle extends ArrayList<Shelf> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3128448425378454765L;

	public Aisle(int nShelves, int nProducts) {
		for (int i = 0; i < nShelves; i++) {
			add(new Shelf(nProducts));
		}
	}

}
