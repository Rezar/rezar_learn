package el.Rezar.love.generic.retailStore;

import java.util.Random;

import el.Rezar.love.generic.GenericsParaminterface.Generator;


public class Product {

	private final int id;
	private String description;
	private double price;

	public Product(int IdNumber, String description, double price) {
		this.id = IdNumber;
		this.description = description;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", description=" + description
				+ ", price=" + price + "]";
	}

	/**
	 * 
	 * �۸��������flagΪtrue�� ����ԭ�м۸�Ļ����ϼӼۣ�����Ϊ����
	 * 
	 * @param change
	 * @param flag
	 */
	public void chagePrice(double change, boolean flag) {
		if (flag) {
			this.price += change;
		} else {
			this.price -= change;
		}
	}

	public static Generator<Product> generator = new Generator<Product>() {
		private Random r = new Random(47);

		@Override
		public Product next() {
			return new Product(r.nextInt(1000), "Test", Math.round(r
					.nextDouble() * 1000.0) + 0.99);
		}
	};

}
