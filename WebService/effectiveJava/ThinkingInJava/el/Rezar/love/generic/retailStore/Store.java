package el.Rezar.love.generic.retailStore;

import java.util.ArrayList;
import java.util.List;

import el.Rezar.love.Utils.CollectionUtils;

public class Store extends ArrayList<Aisle> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 197870910112759267L;

	private List<CheckoutStand> checkouts = CollectionUtils.list();
	private Office office = new Office();

	public Store(int nAisles, int nShelves, int nProducts) {
		for (int i = 0; i < nAisles; i++) {
			add(new Aisle(nShelves, nProducts));
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Aisle a : this) {
			for (Shelf s : a) {
				for (Product p : s) {
					sb.append(p);
					sb.append("\n");
				}
			}
		}
		return sb.toString();
	}

}
