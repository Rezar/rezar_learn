package el.Rezar.love.generic.GenericBound;

import java.awt.Color;

public interface HasColor {

	Color getColor();

}
