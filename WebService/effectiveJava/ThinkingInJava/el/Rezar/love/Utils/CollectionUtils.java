package el.Rezar.love.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import el.Rezar.love.generic.GenericsParaminterface.Generator;


/**
 * ���ɼ��϶���Ĺ������
 * 
 * 
 * @author Administrator
 * 
 */
public class CollectionUtils {

	public static <K, V> Map<K, V> map() {
		return new HashMap<K, V>();
	}

	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> map(Class<?> mapClass) {
		try {
			return (Map<K, V>) mapClass.newInstance();
		} catch (Exception e) {
			return map();
		}
	}

	public static <V> List<V> list() {
		return new ArrayList<V>();
	}

	@SuppressWarnings("unchecked")
	public static <V> List<V> list(Class<?> listClass) {
		try {
			return (List<V>) listClass.newInstance();
		} catch (Exception e) {
			return list();
		}
	}

	public static <V> Set<V> set() {
		return new HashSet<V>();
	}

	@SuppressWarnings("unchecked")
	public static <V> Set<V> set(Class<?> setClass) {
		try {
			return (Set<V>) setClass.newInstance();
		} catch (Exception e) {
			return set();
		}
	}

	/**
	 * ������Arrays.asList()����
	 * 
	 * @param args
	 * @return
	 */
	public static <T> List<T> makeList(T... args) {
		List<T> list = list();
		for (T arg : args) {
			list.add(arg);
		}
		return list;
	}

	/**
	 * ����һ������������������һ������
	 * 
	 * @param coll
	 * @param gen
	 * @param n
	 * @return
	 */
	public static <T> Collection<T> fill(Collection<T> coll, Generator<T> gen,
			int n) {
		for (int i = 0; i < n; i++) {
			coll.add(gen.next());
		}
		return coll;
	}

	/**
	 * �Զ����һ��T���͵�����
	 * 
	 * @param a
	 * @param gen
	 * @return
	 */
	public static <T> T[] fill(T[] a, Generator<T> gen) {
		for (int i = 0; i < a.length; i++) {
			a[i] = gen.next();
		}
		return a;
	}

}
