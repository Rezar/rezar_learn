package el.Rezar.love.Utils.Tuple;

public class FourTuple<S1, S2, S3,S4> extends ThreeTuple<S1, S2, S3> {

	public final S4 fourth;
	
	public FourTuple(S1 first, S2 second, S3 thrid , S4 fourth) {
		super(first, second, thrid);
		this.fourth = fourth;
	}

}
