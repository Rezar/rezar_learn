package el.Rezar.love.Utils.Tuple;

public class FiveTuple<S1, S2, S3, S4, S5> extends FourTuple<S1, S2, S3, S4> {

	public final S5 five;

	public FiveTuple(S1 first, S2 second, S3 thrid, S4 fourth, S5 five) {
		super(first, second, thrid, fourth);
		this.five = five;
	}

}
