package el.Rezar.love.Utils;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * 对集合的一般操作：和/并/交/差
 * 
 * @author Administrator
 * 
 */
public class Sets {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected static <T> Set<T> copy(Set<T> s) {
		if (s instanceof EnumSet)
			return ((EnumSet) s).clone();
		return new HashSet<T>(s);
	}

	public static <T> Set<T> union(Set<T> a, Set<T> b) {
		Set<T> result = copy(a);
		result.addAll(b);
		return result;
	}

	/**
	 * intersection（交集）
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static <T> Set<T> intersection(Set<T> a, Set<T> b) {
		Set<T> result = copy(a);
		result.retainAll(b);
		return result;
	}

	// Subtract(减掉) subset from superset（超集）:
	public static <T> Set<T> difference(Set<T> superset, Set<T> subset) {
		Set<T> result = copy(superset);
		result.removeAll(subset);
		return result;
	}

	// Reflexive--everything not in the intersection（交集）:
	public static <T> Set<T> complement(Set<T> a, Set<T> b) {
		return difference(union(a, b), intersection(a, b));
	}

}
