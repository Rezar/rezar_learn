package el.Rezar.love.Utils.Array;

import java.util.ArrayList;
import java.util.Collection;

import el.Rezar.love.Utils.CollectionUtils;
import el.Rezar.love.����.GenericsParaminterface.Generator;

public class CollectionData<T> {

	private Collection<T> coll;

	public CollectionData(Generator<T> gen, int length) {
		this.coll = new ArrayList<T>();
		this.coll = CollectionUtils.<T> list();
		CollectionUtils.fill(coll, gen, length);
	}

	public T[] toArray(T[] a) {
		return this.coll.toArray(a);
	}

}
