package el.Rezar.love.Utils.Array;

import java.lang.reflect.Array;

/**
 * 
 * ===================================<br/>
 * Generic type cannot be used for primitive type, and we really want to use the
 * generator to fill an array of basic data, in order to solve this problem, we
 * create a converter, it can receive any wrapper array of objects and converts
 * it to corresponding basic data types
 * 
 * @author Administrator
 * 
 */
public class ConvertTo {

	public static boolean[] primitive(Boolean[] in) {
		return (boolean[]) copy(int.class, in);
	}

	public static char[] primitive(Character[] in) {
		return (char[]) copy(int.class, in);
	}

	public static short[] primitive(Short[] in) {
		return (short[]) copy(int.class, in);
	}

	public static int[] primitive(Integer[] in) {
		return (int[]) copy(int.class, in);
	}

	public static long[] primitive(Long[] in) {
		return (long[]) copy(int.class, in);
	}

	public static float[] primitive(Float[] in) {
		return (float[]) copy(int.class, in);
	}

	public static double[] primitive(Double[] in) {
		return (double[]) copy(int.class, in);
	}

	public static byte[] primitive(Byte[] in) {
		return (byte[]) copy(int.class, in);
	}

	/**
	 * ��������ֵ��Copy
	 * 
	 * @param retArray
	 * @param in
	 */
	private static Object copy(Class<?> clazz, Object in) {
		int length = Array.getLength(in);
		Object retArray = Array.newInstance(clazz, length);
		// System.out.println(retArray.getClass());
		for (int i = 0; i < length; i++) {
			Array.set(retArray, i, Array.get(in, i));
		}
		return retArray;
	}
}
