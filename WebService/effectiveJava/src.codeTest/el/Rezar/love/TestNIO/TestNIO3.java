package el.Rezar.love.TestNIO;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.junit.Test;

public class TestNIO3 {

	@Test
	public void test1() throws IOException {
		byte[] array = new byte[40];
		ByteBuffer buffer = ByteBuffer.wrap(array);
		byte[] dst = "0123456789".getBytes();
		buffer.put(dst);
		System.out.println("before rewind : " + buffer);
		buffer.rewind();
		System.out.println("after rewind : " + buffer);
		byte[] dst2 = new byte[5];
		buffer.get(dst2);
		System.out.println("after rewind2 : " + buffer);
		System.out.println(Charset.forName("utf-8").encode(
				buffer.asCharBuffer()).toString());
		System.out.println("dst2 is : " + new String(dst2));
	}

}
