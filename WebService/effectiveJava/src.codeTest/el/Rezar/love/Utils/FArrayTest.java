package el.Rezar.love.Utils;

import org.junit.Test;

public class FArrayTest {

	@Test
	public void testFill() {
		String[] strings = FArray.fill(new String[8],
				new RandomGenerator.String(10));
		for (String str : strings) {
			System.out.println(str);
		}

		Integer[] integers = FArray.fill(new Integer[7],
				new RandomGenerator.Integer());
		for (int i : integers) {
			System.out.println(i);
		}

	}

}
