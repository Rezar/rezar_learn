package el.Rezar.love.CharptOne;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;

public class Test_1Test {

	@Test
	public void testNewInstance() {
		Map<String, List<Integer>> myMap = CollectionUtils.newInstance();
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		myMap.put("rezar", list);
		System.out.println(myMap.get("rezar").get(0));
	}

	@Test
	public void testNewInstance2() {
		Map<String, List<Integer>> myMap = CollectionUtils
				.newInstance(ConcurrentHashMap.class);
		System.out.println(myMap.getClass().getName());
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		myMap.put("rezar", list);
		System.out.println(myMap.get("rezar").get(0));
	}

}
