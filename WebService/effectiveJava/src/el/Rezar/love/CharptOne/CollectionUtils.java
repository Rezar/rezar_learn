package el.Rezar.love.CharptOne;

import java.util.HashMap;
import java.util.Map;

public class CollectionUtils {

	/**
	 * 使用类型推导
	 * 
	 * @return
	 */
	public static <K, V> HashMap<K, V> newInstance() {
		return new HashMap<K, V>();
	}

	/**
	 * 根据指定的Map类型来进行Map对象的初始化
	 * 
	 * @param mapClass
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <K, V> Map<K, V> newInstance(
			Class<? extends Map> mapClass) {
		Map<K, V> ret = null;
		try {
			ret = (Map<K, V>) mapClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			return newInstance();
		}
		return ret;
	}

}
