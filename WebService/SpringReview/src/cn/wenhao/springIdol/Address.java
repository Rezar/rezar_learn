package cn.wenhao.springIdol;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 此时，如果在入参数量一致的情况下，如果两个构造函数都添加了required属性，且有一个保持为true，此时，会发生异常。
 * 
 * <br/>
 * 
 * 如果在数量保持一致的情况下，两者都设为了false，则不会发生以上异常。 且会优先使用能找到与参数类型匹配的构造函数, <br/>
 * 也即在下面的这种情况下，会优先使用第一个构造函数进行对象的初始化
 * 
 * @Autowired(required = false)<br/>
 *                     public Address(Integer value) {<br/>
 *                     System.out.println("with no argu method is runing !!!");<br/>
 *                     System.out.println("value is : " + value);<br/>
 *                     }<br/>
 * @Autowired(required = false)<br/>
 *                     public Address(String address) {<br/>
 *                     System.out.println("with argu constructor is running !!!"
 *                     );<br/>
 *                     this.address = address;<br/>
 *                     }<br/>
 * 
 * 
 * 
 * <br/>
 * 
 *                     测试发现，只要配置了一个required 为true，其余的构造函数若配置了required，都会导致异常发生
 *                     
 *                     <br/>
 * 
 * 
 * @author Administrator
 * 
 */
public class Address {

	private String address = "北京朝阳";

	public Address(Integer value) {
		System.out.println("with no argu method is runing !!!");
		System.out.println("value is : " + value);
	}

	@Autowired(required = false)
	public void setAddress(String address) {
		System.out.println("set method is running !");
		this.address = address;
	}

	@Autowired(required = true)
	public Address(String address, String address2) {
		System.out.println("with argu constructor is running !!!");
		this.address = address;
	}


	@Override
	public String toString() {
		return "Address [address=" + address + "]";
	}

}
