package cn.wenhao.springIdol;

/**
 * 通过静态的工程方法来注入资源
 * 
 * @author Administrator
 * 
 */
public class Stag {

	private Stag() {

	}

	private static class StageSingletonHolder {
		static Stag instance = new Stag();
	}

	public static Stag getInstance() {
		return StageSingletonHolder.instance;
	}

	public void println() {
		System.out.println("this stage is reading for you !");
	}

}
