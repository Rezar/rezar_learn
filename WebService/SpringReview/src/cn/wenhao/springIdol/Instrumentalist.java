package cn.wenhao.springIdol;

/**
 * ͨ��setter/getter
 * 
 * @author Administrator
 * 
 */
public class Instrumentalist implements Performer {

	private String song;
	private Instrument instrunebt;

	public Instrumentalist() {

	}

	@Override
	public void preform() {
		System.out.println("Playing " + song + "");
		this.instrunebt.play();
	}

	public String getSong() {
		return song;
	}

	public void setSong(String song) {
		this.song = song;
	}

	public Instrument getInstrunebt() {
		return instrunebt;
	}

	public void setInstrunebt(Instrument instrunebt) {
		this.instrunebt = instrunebt;
	}

}
