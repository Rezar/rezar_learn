package cn.wenhao.springIdol;

import java.lang.reflect.Field;

public class TestSpEL {

	public double age;

	public void println() {
		Class<? extends TestSpEL> clazz = this.getClass();
		Field[] fields = clazz.getFields();

		for (Field field : fields) {
			field.setAccessible(true);
			try {
				Object value = field.get(this);
				if (value != null) {
					System.out.println(field.getName() + " : " + value);
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

	}

	public void setAge(double age) {
		this.age = age;
	}

}
