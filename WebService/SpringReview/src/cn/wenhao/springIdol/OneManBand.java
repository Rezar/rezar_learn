package cn.wenhao.springIdol;

import java.util.Collection;

public class OneManBand implements Performer {

	private Collection<Instrument> instruments;

	@Override
	public void preform() {
		for (Instrument is : this.instruments) {
			is.play();
		}
	}

	public void setInstruments(Collection<Instrument> instruments) {
		this.instruments = instruments;
	}

}
