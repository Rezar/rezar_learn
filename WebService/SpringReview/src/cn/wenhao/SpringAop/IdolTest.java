package cn.wenhao.SpringAop;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.wenhao.springIdol.Juggler;


public class IdolTest {

	ApplicationContext ctx = new ClassPathXmlApplicationContext(
			"spring_aop.xml");

	@Test
	public void test() {
		Juggler ju = (Juggler) ctx.getBean("juggler");
		ju.preform();
		if (ju instanceof Contestant) {
			((Contestant) ju).receiveAward();
		}
	}
}
