package cn.wenhao.SpringAop;

/**
 * 为所有的Performer引入的新的接口
 * 
 * @author Administrator
 * 
 */
public interface Contestant {

	void receiveAward();

}
