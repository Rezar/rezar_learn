package cn.wenhao.Pre_Test;

/**
 * 使用吟唱诗人来记录骑士的事迹(相当于日志服务)
 * 
 * @author Administrator
 * 
 */
public class Minstrel {

	public void singBeforeQuest() {
		System.out.println("Fa la la ; the knight is so brave !");
	}

	public void singAfterQuest() {
		System.out
				.println("Tee hee he ; the breave knight did embark on a quest !");

	}

}
