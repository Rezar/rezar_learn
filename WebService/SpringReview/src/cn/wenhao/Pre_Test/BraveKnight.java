package cn.wenhao.Pre_Test;

public class BraveKnight implements Knight {

	private Quest questTask;

	public BraveKnight(Quest questTask) {
		this.questTask = questTask;
	}

	@Override
	public void embarkOnQuest() throws Exception {
		this.questTask.embark();
	}

}
