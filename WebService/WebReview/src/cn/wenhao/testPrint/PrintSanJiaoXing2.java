package cn.wenhao.testPrint;

public class PrintSanJiaoXing2 {

	/**
	 * 输出一个三角形，主要要进行输出：<br/>
	 * 
	 * A:每行开始的空行，由当前第几行可以确定之前需要空多少
	 */

	public static int printSpace(int currentLine, int totalLine, String ch) {
		int i = 0;
		for (; i < (totalLine - currentLine); i++) {
			System.out.print(ch);
		}
		return i;
	}

	public static void main(String[] args) {
		int totalLine = 10;
		String ch = "*";
		printSanjiaoxing(totalLine, ch);
	}

	private static void printSanjiaoxing(int totalLine, String ch) {
		for (int i = 0; i < totalLine; i++) {
			printSpace(i, totalLine - 1, " ");
			int end = i * 2 - 1;
			System.out.print(ch);
			if (i != totalLine - 1)
				printChStr(end, ch, false);
			else
				printChStr(end, ch, true);
			System.out.println();
		}
	}

	private static void printChStr(int end, String ch, boolean b) {
		if (end < 0) {
			return;
		}
		if (b) {
			for (int i = 0; i <= end / 2; i++) {
				System.out.print(" " + ch);
			}
		} else {
			printChInIndex(end, ch);
		}

	}

	public static void printChInIndex(int index, String ch) {
		for (int i = 0; i < index; i++) {
			System.out.print(" ");
		}
		System.out.print(ch);
	}

}
