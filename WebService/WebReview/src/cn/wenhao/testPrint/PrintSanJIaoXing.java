package cn.wenhao.testPrint;

public class PrintSanJIaoXing {

	/**
	 * 根据一个i值来打印出对应的空格
	 * 
	 * @param i
	 */
	public static void printSpace(int len, int totalLine) {

		for (int i = 0; i < (totalLine - len); i++) {
			System.out.print(" ");
		}
	}

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			printSpace(i, 10);
			System.out.print("*");
			if (i == 9) {
				for (int j = 0; j < i - 1; j++) {
					System.out.print(" *");
				}
			} else {
				for (int j = 0; j < i * 2 - 1; j++) {
					System.out.print(" ");
				}
			}
			if (i != 0 && i != 9) {
				System.out.println("*");
			} else if (i == 9) {
				System.out.println(" *");
			} else {
				System.out.println();
			}
		}
	}
}
