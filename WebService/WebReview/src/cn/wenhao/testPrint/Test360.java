package cn.wenhao.testPrint;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Test360 {

	static class relat {
		String a;
		String b;

		public relat(String a, String b) {
			this.a = a;
			this.b = b;
		}

		public String getA() {
			return a;
		}

		public void setA(String a) {
			this.a = a;
		}

		public String getB() {
			return b;
		}

		public void setB(String b) {
			this.b = b;
		}

	}

	/**
	 * 辅助类，用于记录当前组中的数据
	 * 
	 * @author Administrator
	 * 
	 */
	static class myObject {

		int n, m;
		/* 该组中包含的所有关系 */
		List<relat> relats = new ArrayList<relat>();

		public myObject() {
		}

		public myObject(int n, int m) {
			this.n = n;
			this.m = m;
		}

		public int getN() {
			return n;
		}

		public void setN(int n) {
			this.n = n;
		}

		public int getM() {
			return m;
		}

		public void setM(int m) {
			this.m = m;
		}

		public void add(relat relat) {
			this.relats.add(relat);
		}

		/**
		 * 查找该组中只认识自己的人
		 * */
		public List<String> findLonely() {
			List<String> ids = new ArrayList<String>();
			for (relat r : this.relats) {
				String id = r.getA();
				boolean notLonely = findB(id);
				if (!notLonely) {
					ids.add(id);
				}
			}
			return ids;
		}

		/**
		 * 如果某个人有除了自己之外的其它的人的关系，返回true,表示该人不是只认识自己的人
		 * 
		 * @param id
		 * @return
		 */
		private boolean findB(String id) {
			for (relat r : this.relats) {
				if (r.getA().equals(id) && !r.getB().equals(id)) {
					return true;
				}
			}
			return false;
		}
	}

	public static void main(String[] args) {
		/* 控制台输入 */
		Scanner sc = new Scanner(System.in);
		int currentLine = 0;
		int T = 0;
		int n = 0, m = 0;
		boolean isGroupFirst = false;
		List<List<String>> resultList = new ArrayList<List<String>>();
		myObject mo = new myObject();
		/* 利用while循环来控制输入 */
		while (sc.hasNextLine()) {
			String nextLine = sc.nextLine();
			/* if input "exit" or nothing input , will stop */
			if (nextLine.equals("exit") || nextLine.equals("")) {
				break;
			}
			if (currentLine == 0) {
				T = Integer.parseInt(nextLine);
				isGroupFirst = true;
				currentLine = 2;
				continue;
			}
			/* 确定每一组的开始 */
			if (isGroupFirst) {
				String[] strs_ = nextLine.split(" ");
				n = stringToIntger(strs_[0]);
				m = stringToIntger(strs_[1]);
				mo.setN(n);
				mo.setM(m);
				isGroupFirst = false;
			}
			/* 确定每一组的结束 */
			if (m > 0) {
				String[] strs_ = nextLine.split(" +");
				relat r = new relat(strs_[0], strs_[1]);
				mo.add(r);
				m--;
			} else {
				T--;
				/* 查找当前组中只认识自己的人 */
				resultList.add(mo.findLonely());
				mo = new myObject();
			}
		}

		/* output */
		for (List<String> ret : resultList) {
			int size = ret.size();
			if (size == 0) {
				System.out.println("0");
				System.out.println();
			} else {
				System.out.println(size);
				System.out.println(ret.toString());
			}
		}

	}

	/**
	 * change to string to a number
	 * 
	 * @param string
	 * @return
	 */
	private static int stringToIntger(String string) {
		return Integer.parseInt(string);
	}

}
