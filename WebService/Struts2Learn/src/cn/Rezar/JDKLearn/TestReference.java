package cn.Rezar.JDKLearn;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

import org.junit.Test;

public class TestReference {

	@Test
	public void test1() throws InterruptedException {
		// 创建强引用
		String str = "hello";
		// 创建引用队列
		ReferenceQueue<String> rq = new ReferenceQueue<String>();
		// 创建一个弱引用，并且与str关联
		WeakReference<String> wr = new WeakReference<String>(str, rq);

		str = null;
		System.gc();
		System.gc();

		String str1 = wr.get();
		System.out.println("str1 = " + str1);
		str1 = null;

		Reference<? extends String> rf = rq.poll();
		System.out.print(rf == null);

	}

	@Test
	public void test2() {
		// 此时，在内存紧张的情况下
		A a = new A();
		ReferenceQueue<A> rq = new ReferenceQueue<A>();
		WeakReference<A> wf = new WeakReference<TestReference.A>(a, rq);
		a = null;
		System.gc();
		System.gc();
		A a1 = wf.get();
		System.out.println("a1 is null : " + (a1 == null));
		Reference<? extends A> rf = rq.poll();
		System.out.println("rf is null : " + (rf == null));
	}

	class A {
		int[] a;

		public A() {
			a = new int[10000000];
		}
	}

}
