package cn.Rezar.JDKLearn;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;

public class References {

	private static final ReferenceQueue<Grocery> rq = new ReferenceQueue<Grocery>();

	public static void checkQueue() {
		Reference<? extends Grocery> rf = rq.poll();
		if (rf != null) {
			System.out.println("in queue " + rf + ":" + rf.get());
		}
	}

	public static void main(String[] args) throws InterruptedException {
		final int size = 10;

		Set<SoftReference<Grocery>> sa = new HashSet<SoftReference<Grocery>>();
		for (int i = 0; i < size; i++) {
			SoftReference<Grocery> sr = new SoftReference<Grocery>(new Grocery(
					"soft " + i), rq);
			System.out.println("just create soft: " + sr.get());
			sa.add(sr);
		}
		System.gc();
		checkQueue();

		Set<WeakReference<Grocery>> wa = new HashSet<WeakReference<Grocery>>();
		for (int i = 0; i < size; i++) {
			WeakReference<Grocery> wr = new WeakReference<Grocery>(new Grocery(
					"weak " + i), rq);
			System.out.println("just create weak:" + wr.get());
			wa.add(wr);
		}
		System.gc();
		checkQueue();

		Set<PhantomReference<Grocery>> pa = new HashSet<PhantomReference<Grocery>>();
		for (int i = 0; i < size; i++) {
			PhantomReference<Grocery> pr = new PhantomReference<Grocery>(
					new Grocery("phantom " + i), rq);
			System.out.println("just create phantom : " + pr.get());
			pa.add(pr);
		}
		System.gc();
		checkQueue();

		/**
		 * 在 as数组的长度过大的时候，会进行softReference引用的内存空间回收
		 */
		A[] as = new A[5];
		for (int i = 0; i < as.length; i++) {
			as[i] = new A();
		}

		System.gc();
		Thread.sleep(10000);
		System.gc();

	}

}

class Grocery {

	private static final int SIZE = 10000;

	private double[] ds = new double[SIZE];

	private String id;

	public Grocery(String id) {
		this.id = id;
	}

	public String toString() {
		return this.id;
	}

	@Override
	public void finalize() throws Throwable {
		System.out.println("grocery id :" + this.id + " isFinalize ! ");
	}

}
