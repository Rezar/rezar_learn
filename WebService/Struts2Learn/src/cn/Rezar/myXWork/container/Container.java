package cn.Rezar.myXWork.container;

import java.io.Serializable;
import java.util.Set;

import com.opensymphony.xwork2.inject.Scope;

/**
 * 容器类
 * 
 * @author Administrator
 * 
 */
public interface Container extends Serializable {

	/**
	 * 定义默认的对象获取标识
	 */
	String DEFAULT_NAME = "default";

	/**
	 * 进行对象依赖注入的基本操作接口，作为参数的Object将被XWork容器进行处理。<br/>
	 * object内部生命有@Inject的字段和方法，都将被注入受到容器托管的对象 ，从而建立起依赖关系
	 * 
	 * @param object
	 */
	void inject(Object object);

	/**
	 * 创建一个类的实例被进行对象依赖注入
	 * 
	 * @param implementation
	 * @return
	 */
	<T> T inject(Class<T> implementation);

	/**
	 * 根据type和name作为唯一表示，获取容器中Java类的实例
	 * 
	 * @param type
	 * @param name
	 * @return
	 */
	<T> T getInstance(Class<T> type, String name);

	/**
	 * 根据type和默认的name（default）作为唯一的标识，获取容器中java类的实例
	 * 
	 * @param type
	 * @return
	 */
	<T> T getInstance(Class<T> type);

	/**
	 * 根据type获取与这个type所对应的容器中所有注册过的name
	 * 
	 * @param type
	 * @return
	 */
	Set<String> getInstanceNames(Class<?> type);

	/**
	 * 设置当前线程的作用范围的策略
	 * 
	 * @param scopeStrategy
	 */
	void setScopeStrategy(Scope.Strategy scopeStrategy);

	/**
	 * 删除当前线程的作用范围的策略
	 */
	void removeScopeStrategy();

}
