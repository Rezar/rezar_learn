package cn.Rezar.myXWork.container.containerImpl;

import java.util.Set;

import com.opensymphony.xwork2.inject.Scope.Strategy;

import cn.Rezar.myXWork.container.Container;

public class ContainerImpl implements Container {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5325037623601002117L;

	@Override
	public void inject(Object object) {

	}

	@Override
	public <T> T inject(Class<T> implementation) {
		return null;
	}

	@Override
	public <T> T getInstance(Class<T> type, String name) {

		return null;
	}

	@Override
	public <T> T getInstance(Class<T> type) {

		return null;
	}

	@Override
	public Set<String> getInstanceNames(Class<?> type) {

		return null;
	}

	@Override
	public void setScopeStrategy(Strategy scopeStrategy) {

	}

	@Override
	public void removeScopeStrategy() {

	}

}
