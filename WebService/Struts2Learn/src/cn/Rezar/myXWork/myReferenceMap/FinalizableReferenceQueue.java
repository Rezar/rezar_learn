package cn.Rezar.myXWork.myReferenceMap;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;

/**
 * Starts a background thread that cleans up after reclaimed referents.
 * 
 * @author Administrator
 * 
 */
public class FinalizableReferenceQueue extends ReferenceQueue<Object> {

	private FinalizableReferenceQueue() {
	}

	void cleanUp(Reference<?> reference) {
		try {
			((FinalizableReference) reference).finalizeReferent();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void start() {
		Thread thread = new Thread("FinalizableReferenceQueue") {

			@Override
			public void run() {
				while (true) {
					try {
//						Reference<?> obj = remove();
//						System.out.println(" Remove " + obj);
//						cleanUp(obj);
						cleanUp(remove());
					} catch (InterruptedException e) {
						/* do nothing */
					}
				}
			}
		};
		thread.setDaemon(true);
		thread.start();
		System.out.println("FinalizableReferenceQueue is run in background !");
	}

	static ReferenceQueue<Object> instance = createAndStart();

	static FinalizableReferenceQueue createAndStart() {
		FinalizableReferenceQueue queue = new FinalizableReferenceQueue();
		queue.start();
		return queue;
	}

	public static ReferenceQueue<Object> getInstance() {
		return instance;
	}

}
