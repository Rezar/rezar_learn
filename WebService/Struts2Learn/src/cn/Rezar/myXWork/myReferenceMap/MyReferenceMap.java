package cn.Rezar.myXWork.myReferenceMap;

import static cn.Rezar.myXWork.myReferenceMap.ReferenceType.STRONG;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class MyReferenceMap<K, V> implements Map<K, V>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* 创建数据存放的委托容器 */
	transient ConcurrentMap<Object, Object> delegate;

	/**
	 * 
	 */
	final ReferenceType keyReferenceType;
	final ReferenceType valueReferenceType;

	public MyReferenceMap(ReferenceType keyReferenceType,
			ReferenceType valueReferenceType) {
		ensureNotNull(keyReferenceType, valueReferenceType);
		if (keyReferenceType == ReferenceType.PHANTOM
				|| valueReferenceType == ReferenceType.PHANTOM) {
			throw new IllegalArgumentException("phantom references not support");
		}
		this.delegate = new ConcurrentHashMap<Object, Object>();
		this.keyReferenceType = keyReferenceType;
		this.valueReferenceType = valueReferenceType;
	}

	V internalGet(K key) {
		/**
		 * Here, using a wrapper, use the wrapper hashCode methods to weed out
		 * the bottom of the Object's hashCode method, used to compare any two
		 * objects, because of using the System. IdentityHashCode x (Object)
		 * method, this method can judge whether two objects is agreed to an
		 * Object,Use wrapper pattern, after the class at the bottom of the
		 * hashCode is to use the System. Identifyxxx method to obtain, and the
		 * Key in the objects created by the use of hashCode generation is
		 * consistent, guaranteed to get to the same object (the same hashCode,
		 * and equals () method returns true)
		 */
		Object valueReference = this.delegate.get(makeKeyReferenceAware(key));
		/**
		 * Here to return to the corresponding value is the Key of a subclass of
		 * a Reference, need to change, before returning to the value to get the
		 * original value of the Reference
		 */
		return valueReference == null ? null
				: (V) dereferenceValue(valueReference);
	}

	/**
	 * 
	 * @param strategy
	 * @param key
	 * @param value
	 * @return
	 */
	V execute(Strategy strategy, K key, V value) {
		ensureNotNull(key, value);
		/**
		 * Based on the current cache object Reference type to create the
		 * corresponding Reference subclass object
		 */
		Object keyReference = referenceKey(key);
		Object valueReference = strategy.execute(this, keyReference,
				referenceValue(keyReference, value));
		return valueReference == null ? null
				: (V) dereferenceValue(valueReference);
	}

	@Override
	public V put(K key, V value) {
		return execute(putStrategy(), key, value);
	}

	public V putIfAbsent(K key, V value) {
		// TODO (crazy bob) if the value has been gc'ed(garbage collector) but
		// the entry hasn't been
		// cleaned up yet, this put will fail.
		/**
		 * If the original value has been reclaimed by the garbage collector,
		 * but entry has not been cleared, the put operation will fail
		 */

		return this.execute(this.putIfAbsentStrategy(), key, value);
	}

	@Override
	public V remove(Object key) {
		ensureNotNull(key);
		Object keyReference = referenceKey(key);
		Object valueReference = this.delegate.remove(keyReference);
		return valueReference == null ? null
				: (V) dereferenceValue(valueReference);
	}

	private Strategy putStrategy() {
		return PutStrategy.PUT;
	}

	/**
	 * Creates a reference for a value.
	 * 
	 * @param keyReference
	 */
	Object referenceValue(Object keyReference, V value) {

		switch (this.valueReferenceType) {
		case STRONG:
			return value;
		case SOFT:
			return new SoftValueReference(keyReference, value);
		case WEAK:
			return new WeakValueReference(keyReference, value);
		default:
			throw new AssertionError();
		}

	}

	class WeakValueReference extends FinalizableSoftReference<Object> implements
			InternalReference {

		Object keyReferenct;

		public WeakValueReference(Object keyReference, Object referent) {
			super(referent);
			this.keyReferenct = keyReference;
		}

		/**
		 * 只有目前将键的条目映射到给定值时，才移除该键的条目。
		 */
		@Override
		public void finalizeReferent() {
			delegate.remove(keyReferenct, this);
		}

		@Override
		public boolean equals(Object obj) {
			return referenceEquals(this, obj);
		}

	}

	/**
	 * 
	 * @author Administrator
	 * 
	 */
	class SoftValueReference extends FinalizableSoftReference<Object> implements
			InternalReference {

		/**
		 * At this point, add a KeyReference in SoftValueReference, restore this
		 * Value is used in GC, can in the callback method finalizeReferent
		 * according to the Value of the Key to delete the entry from the Map
		 */
		Object keyReferenct;

		public SoftValueReference(Object keyReference, Object referent) {
			super(referent);
			this.keyReferenct = keyReference;
		}

		/**
		 * 只有目前将键的条目映射到给定值时，才移除该键的条目。
		 */
		@Override
		public void finalizeReferent() {
			/**
			 * Only the key items mapped to the given value, the will be removes
			 * the key item.This is equivalent to: if (map.containsKey(key) &&
			 * map.get(key).equals(value)) { map.remove(key); return true; }
			 * else return false; The difference is that the operation is atomic
			 * way (atomically).
			 */
			delegate.remove(keyReferenct, this);
		}

		/**
		 * Compare the two value object is the same
		 */
		@Override
		public boolean equals(Object obj) {
			return referenceEquals(this, obj);
		}

	}

	/**
	 * Creates a reference for a key.
	 */
	private Object referenceKey(Object key) {
		switch (this.keyReferenceType) {
		case STRONG:
			return key;
		case SOFT:
			return new SoftKeyReference(key);
		case WEAK:
			return new WeakKeyReference(key);
		default:
			throw new AssertionError();
		}
	}

	class WeakKeyReference extends FinalizableWeakReference<Object> implements
			InternalReference {

		int hashCode;

		public WeakKeyReference(Object referent) {
			super(referent);
		}

		@Override
		public void finalizeReferent() {
			System.out.println("value is remove !");
			delegate.remove(this);
		}

		@Override
		public int hashCode() {
			return this.hashCode;
		}

		@Override
		public boolean equals(Object o) {
			return referenceEquals(this, o);
		}

	}

	class SoftKeyReference extends FinalizableSoftReference<Object> implements
			InternalReference {

		int hashCode;

		public SoftKeyReference(Object referent) {
			super(referent);
			this.hashCode = keyHashCode(referent);
		}

		@Override
		public void finalizeReferent() {
			delegate.remove(this);
		}

		@Override
		public int hashCode() {
			return this.hashCode;
		}

		@Override
		public boolean equals(Object o) {
			return referenceEquals(this, o);
		}

	}

	/**
	 * Used to compare two objects are the same object
	 * 
	 * @param r
	 * @param o
	 * @return
	 */
	static boolean referenceEquals(Reference<?> r, Object o) {
		// compare reference to reference
		if (o instanceof InternalReference) {
			if (o == r) {
				return true;
			}

			Object referent = ((Reference<?>) o).get();
			return referent != null && referent == r.get();
		}
		/**
		 * What an object would not InternalReference instance?<br/>
		 * Because some object is created in the deposit in the Map, is a
		 * subtype of the Reference, and when used to query, the incoming value
		 * is to use the wrappers covers a stratigraphic comparison method of
		 * the object, such as covering the underlying hashCode used to compare
		 * the Key KeyReferenceAwareWrapper wrappers, and covers the underlying
		 * equals () method is used to compare two value objects are consistent?
		 */
		return r.get() == ((ReferenceAwareWrapper) o).unwrap();
	}

	/**
	 * 
	 * @param referent
	 * @return
	 */
	static int keyHashCode(Object referent) {
		return System.identityHashCode(referent);
	}

	/**
	 * Marker interface to differentiate external and internal references.
	 */
	interface InternalReference {
	}

	/**
	 * 包访问权限，允许子类和当前的包进行访问
	 * 
	 * @author Administrator
	 * 
	 */
	protected interface Strategy {
		public Object execute(MyReferenceMap<?, ?> map, Object keyReference,
				Object valueReference);
	}

	/**
	 * 通过一个策略模式来代表不同的数据插入操作
	 * 
	 * @author Administrator
	 * 
	 */
	private enum PutStrategy implements Strategy {
		PUT {
			@Override
			public Object execute(MyReferenceMap<?, ?> map,
					Object keyReference, Object valueReference) {
				return map.delegate.put(keyReference, valueReference);
			}
		},
		REPLACE {

			@Override
			public Object execute(MyReferenceMap<?, ?> map,
					Object keyReference, Object valueReference) {
				return map.delegate.replace(keyReference, valueReference);
			}
		},
		PUT_IF_ABSENT {

			@Override
			public Object execute(MyReferenceMap<?, ?> map,
					Object keyReference, Object valueReference) {
				return map.delegate.putIfAbsent(keyReference, valueReference);
			}
		}
	}

	/**
	 * Converts a reference to a value.
	 * 
	 * @param valueReference
	 * @return
	 */
	V dereferenceValue(Object valueReference) {
		return (V) dereference(this.valueReferenceType, valueReference);
	}

	/**
	 * Returns the referent for reference given its reference type.
	 * 
	 * @param valueReferenceType
	 * @param valueReference
	 * @return
	 */
	private Object dereference(ReferenceType valueReferenceType,
			Object valueReference) {
		return valueReferenceType == STRONG ? valueReference
				: ((Reference<?>) valueReference).get();
	}

	/**
	 * 创建一个键的包装器对象
	 * 
	 * @param key
	 * @return
	 */
	private Object makeKeyReferenceAware(Object key) {
		return this.keyReferenceType == STRONG ? key
				: new KeyReferenceAwareWrapper(key);
	}

	/**
	 * 覆盖了父类的hashCode方法，用于比较两个键对象
	 * 
	 * @author Administrator
	 * 
	 */
	static class KeyReferenceAwareWrapper extends ReferenceAwareWrapper {

		KeyReferenceAwareWrapper(Object wrapped) {
			super(wrapped);
		}

		@Override
		public int hashCode() {
			/*
			 * 返回给定对象的哈希码，该代码与默认的方法 hashCode() 返回的代码一样，无论给定对象的类是否重写 hashCode()。
			 */
			return System.identityHashCode(wrapped);
		}

	}

	/**
	 * 用于比较键和值，在Hash的情况下，hashCode用于键的比较，而equals用于值的比较, Used to compare keys and
	 * values to referenced keys and values without creating more references.
	 * 
	 * @author Administrator
	 * 
	 */
	static class ReferenceAwareWrapper {
		Object wrapped;

		ReferenceAwareWrapper(Object wrapped) {
			this.wrapped = wrapped;
		}

		Object unwrap() {
			return this.wrapped;
		}

		public int hashCode() {
			return this.wrapped.hashCode();
		}

		public boolean equals(Object obj) {
			return obj.equals(this);
		}
	}

	@Override
	public int size() {
		return this.delegate.size();
	}

	@Override
	public boolean isEmpty() {
		return this.delegate.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		ensureNotNull(key);
		/**
		 * Here are using makeKeyReference () method, in does not create the
		 * Reference object of Reference types, using a wrapper objects created
		 * to match the Key in the Map
		 */
		Object keyReference = makeKeyReferenceAware(key);
		return this.delegate.containsKey(keyReference);
	}

	@Override
	public boolean containsValue(Object value) {
		ensureNotNull(value);
		for (Object valueReference : this.delegate.values()) {
			if (value.equals(dereferenceValue(valueReference))) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(Object key) {
		ensureNotNull(key);
		return internalGet((K) key);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
			this.put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public void clear() {
		this.delegate.clear();
	}

	@Override
	public Set<K> keySet() {
		return Collections.unmodifiableSet(dereferenceKetSet(this.delegate
				.keySet()));
	}

	/**
	 * Dereferences a set of key references.
	 * 
	 * @param keySet
	 * @return
	 */
	@SuppressWarnings("unchecked")
	Set<K> dereferenceKetSet(Set<?> keySet) {
		/**
		 * All the Key values of deposited into a new Set in the collection, to
		 * avoid the original Set returned to the client code, original Set Set
		 * changes affect the client code or original Set Set the client data
		 * changed the Set Set of data inconsistencies
		 */
		return this.keyReferenceType == STRONG ? keySet
				: dereferenceCollection(this.keyReferenceType, keySet,
						new HashSet());
	}

	/**
	 * Dereferences elements in {@code in} using {@code referenceType} and puts
	 * them in {@code out}. Returns {@code out}.,<br/>
	 * 
	 * Set to generic code is for gm
	 */
	<T extends Collection<Object>> T dereferenceCollection(
			ReferenceType referenceType, T in, T out) {
		for (Object reference : in) {
			out.add(dereference(referenceType, reference));
		}
		return out;
	}

	@Override
	public Collection<V> values() {
		return Collections
				.unmodifiableCollection(dereferenceValues(this.delegate
						.values()));
	}

	public boolean replace(K key, V oldValue, V newValue) {
		ensureNotNull(key, oldValue, newValue);
		Object referenceAwareOldValue = this.makeValueReferenceAware(oldValue);
		// There is no need to use the makeKeyReferenceAwareWrapper packaging?
		Object keyReference = /* referenceKey(key); */null;
		// i think here is :
		/**
		 * If there is not use packing objects, but the newly created a subclass
		 * of a Reference type, at this point, the subclass object will be
		 * incorporated into FinalizableReferenceQueue management scope, the
		 * object will continue in the background of the processing to be GC
		 * cleaning object, then the callback to save the Reference type
		 * instance cleaning method in the Queue, removing the Key from the Map
		 * values corresponding Entry, but at the moment the Key with the
		 * outside world to to find a Key, use by GC after cleaning, at this
		 * joint will be stored in the Map data in a clear, originally cause
		 * data lost,but in fact , we can see the JDK API and we will find out
		 * how can we do it like use the referenceKey() method to create a
		 * keyReference, because : {<br/>
		 * Only now map the key items to a certain value, to replace the key
		 * item.This is equivalent to: <br/>
		 * if (map.containsKey(key)) {<br/>
		 * return map.put(key, value); <br/>
		 * } else<br/>
		 * return null; <br/>
		 * }
		 * 
		 * <br/>
		 * As we see, the Map collections will to save the key_value again,?<br/>
		 * but , However, when the same HashCode and equals method returns true,
		 * the Key will to save and load Map collections?
		 */
		keyReference = this.makeKeyReferenceAware(key);
		return this.delegate.replace(keyReference, referenceAwareOldValue,
				referenceValue(keyReference, newValue));
	}

	/**
	 * wrap a value
	 * 
	 * @param oldValue
	 * @return
	 */
	private Object makeValueReferenceAware(V oldValue) {
		return this.valueReferenceType == STRONG ? oldValue
				: new ReferenceAwareWrapper(oldValue);
	}

	public V replace(K key, V value) {
		return execute(replaceStrategy(), key, value);
	}

	protected Strategy putIfAbsentStrategy() {
		return PutStrategy.PUT_IF_ABSENT;
	}

	protected Strategy replaceStrategy() {
		return PutStrategy.REPLACE;
	}

	@SuppressWarnings("unchecked")
	Collection<? extends V> dereferenceValues(Collection<Object> values) {
		return this.valueReferenceType == STRONG ? values
				: dereferenceCollection(this.valueReferenceType, values,
						new ArrayList(values.size()));
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		Set<Map.Entry<K, V>> entrySet = new HashSet<Map.Entry<K, V>>();
		for (Map.Entry<Object, Object> entry : this.delegate.entrySet()) {
			Map.Entry<K, V> dereferenced = dereferenceEntry(entry);
			if (dereferenced != null) {
				entrySet.add(dereferenced);
			}
		}
		return Collections.unmodifiableSet(entrySet);
	}

	java.util.Map.Entry<K, V> dereferenceEntry(
			java.util.Map.Entry<Object, Object> entry) {
		K key = this.dereferenceKey(entry.getKey());
		V value = this.dereferenceValue(entry.getValue());
		return (key == null || value == null) ? null : new Entry(key, value);
	}

	class Entry implements Map.Entry<K, V> {
		K key;
		V value;

		public Entry(K key, V value) {
			this.key = key;
			this.value = value;
		}

		@Override
		public K getKey() {
			return this.key;
		}

		@Override
		public V getValue() {
			return this.value;
		}

		@Override
		public V setValue(V value) {
			return MyReferenceMap.this.put(key, value);
		}

		@Override
		public int hashCode() {
			return key.hashCode() * 31 + value.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof MyReferenceMap.Entry)) {
				return false;
			}

			Entry entry = (Entry) obj;
			return key.equals(entry.key) && value.equals(entry.value);
		}

		@Override
		public String toString() {
			return "Entry [key=" + key + ", value=" + value + "]";
		}

	}

	K dereferenceKey(Object key) {
		return (K) dereference(this.keyReferenceType, key);
	}

	static void ensureNotNull(Object key) {
		if (key == null) {
			throw new NullPointerException("");
		}
	}

	/**
	 * 确保传入的参数不包含空值
	 * 
	 * @param objects
	 */
	static void ensureNotNull(Object... objects) {
		if (objects.length > 0) {
			for (Object obj : objects) {
				if (obj == null) {
					throw new NullPointerException("args is null");
				}
			}
		}
	}

	private void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
		out.writeInt(size());
		for (Map.Entry<Object, Object> entry : delegate.entrySet()) {
			Object key = dereferenceKey(entry.getKey());
			Object value = dereferenceValue(entry.getValue());

			// don't persist gc'ed entries.
			if (key != null && value != null) {
				out.writeObject(key);
				out.writeObject(value);
			}
		}
		out.writeObject(null);
	}

	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException {
		in.defaultReadObject();
		int size = in.readInt();
		this.delegate = new ConcurrentHashMap<Object, Object>(size);
		while (true) {
			K key = (K) in.readObject();
			if (key == null) {
				break;
			}
			V value = (V) in.readObject();
			put(key, value);
		}
	}

}
