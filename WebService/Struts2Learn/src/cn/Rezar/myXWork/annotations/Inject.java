package cn.Rezar.myXWork.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.CONSTRUCTOR,
		ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {

	/**
	 * 进行依赖注入的名称，如果不声明，这个名称会被设置为default
	 * 
	 * @return
	 */
	String value() default "default";

	/**
	 * 是否必须进行依赖注入，仅仅对方法和参数有效
	 * 
	 * @return
	 */
	boolean required() default true;

}
