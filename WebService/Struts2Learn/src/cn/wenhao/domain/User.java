package cn.wenhao.domain;

import cn.wenhao.Utils.annotation.WhetherNotNull;

public class User {

	@WhetherNotNull
	private int id;
	@WhetherNotNull
	private String userName;
	@WhetherNotNull
	private String userAddress;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", userAddress="
				+ userAddress + "]";
	}

}
