package cn.wenhao.Utils.interfaces;

/**
 * Domain对象的校验方法接口
 * 
 * @author Administrator
 * 
 */
public interface IDomainValidate {
	public boolean validate();
}
