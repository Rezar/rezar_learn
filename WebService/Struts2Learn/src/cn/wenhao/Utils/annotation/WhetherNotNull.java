package cn.wenhao.Utils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注解，代表一个属性是否可以为空或者长度为零,字符串专指
 * @author Administrator
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface WhetherNotNull {

	boolean mustNotNull() default true;

	boolean mustNotEmpty() default true;

}
