package cn.wenhao.Utils.annotation.validate;

import java.lang.reflect.Field;

import cn.wenhao.Utils.interfaces.ValidateForAnnotation;

/**
 * 对属性值进行处理的过滤链对象
 * 
 * @author Administrator
 * 
 */
public class AValidate {

	private AValidate ava_next;
	private ValidateForAnnotation vf;

	public AValidate() {
	}

	protected AValidate(ValidateForAnnotation vf) {
		this.vf = vf;
	}

	public void setAvalidate(AValidate ava_next) {
		this.ava_next = ava_next;
	}

	public final <T> void doValidate(Field field, T obj) {
		this.vf.validate(field, obj, this.ava_next);
	}

}
