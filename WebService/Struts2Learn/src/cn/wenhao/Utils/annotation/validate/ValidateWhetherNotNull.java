package cn.wenhao.Utils.annotation.validate;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import cn.wenhao.Utils.annotation.WhetherNotNull;
import cn.wenhao.Utils.interfaces.ValidateForAnnotation;

public class ValidateWhetherNotNull implements ValidateForAnnotation {

	@Override
	public <T> boolean validate(Field field, T obj, AValidate avalidate) {
		Annotation[] annts = field.getAnnotations();
		for (Annotation annt : annts) {
			if (annt instanceof WhetherNotNull) {
				boolean mustNotEmp = ((WhetherNotNull) annt).mustNotEmpty();
				boolean mustNotNull = ((WhetherNotNull) annt).mustNotNull();
				field.setAccessible(true);
				try {
					Object value = field.get(obj);
					System.out
							.println(field.getName() + ",value is : " + value);
					if (mustNotNull) {
						if (value == null) {
							throw new RuntimeException(field.getName()
									+ "must not be null!");
						}
					}
					if (mustNotEmp) {
						if (value instanceof String) {
							if (((String) value).length() == 0) {
								throw new RuntimeException(field.getName()
										+ "must not be empty !");
							}
						}
					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}
}
