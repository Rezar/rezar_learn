package cn.wenhao.Utils.propertiesUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import sun.misc.BASE64Encoder;

@SuppressWarnings("restriction")
public class StringUtils {

	public static String base64Encoder(String input) {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("md5");
			byte[] buff = digest.digest(input.getBytes());
			BASE64Encoder encoder = new BASE64Encoder();
			String before = encoder.encode(buff);
			return before;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return input;
	}

}
