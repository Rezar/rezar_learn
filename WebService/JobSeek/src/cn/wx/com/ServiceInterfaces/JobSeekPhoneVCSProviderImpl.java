package cn.wx.com.ServiceInterfaces;

import cn.wx.config.CommonFieldValue;

/**
 * 求职者手机验证码服务的提供者实现
 * 
 * @author Administrator
 * 
 */
public class JobSeekPhoneVCSProviderImpl implements
		PhoneValidationCodeServiceProviderInterface {

	static {
		ServiceManager.registerProvider(CommonFieldValue.JOBSEEKER,
				new JobSeekPhoneVCSProviderImpl());
	}

	@Override
	public IPhoneValidationCodeService getService() {
		return new JobSeekerPhoneValidationCodeServiceImpl();
	}

}
