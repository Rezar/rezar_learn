package cn.wx.domain;

import java.util.Date;

/**
 * 此处为求职者申请职位后的反馈消息
 * 
 * @author Administrator
 * 
 */
public class ApplyForFeedBackMessage {

	private String id;
	/* 公司名称 */
	private User company;
	/* 工作地点 */
	private Address workAddress;
	/* 职位名称 */
	private String jobName;
	/* 工作薪水 */
	private double salary;
	/* 邀约发送时间 */
	private Date sendDate;
	/**
	 * 查看状态 <br/>
	 * 0:not read <br/>
	 * 1:read already
	 * 
	 * */
	private int state;
	/* 职位申请者 */
	private User jobSeeker;

	public User getJobSeeker() {
		return jobSeeker;
	}

	public void setJobSeeker(User jobSeeker) {
		this.jobSeeker = jobSeeker;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getCompanyName() {
		return company;
	}

	public void setCompanyName(User company) {
		this.company = company;
	}

	public Address getWorkAddress() {
		return workAddress;
	}

	public void setWorkAddress(Address workAddress) {
		this.workAddress = workAddress;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

}
