package cn.wx.domain;

/**
 * 用户的建议
 * 
 * @author Administrator
 * 
 */
public class Advice {

	private String id;
	/* 用户的邮箱，选填 */
	private String email;
	private String content;
	private User usr;

	public User getUsr() {
		return usr;
	}

	public void setUsr(User usr) {
		this.usr = usr;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Advice [id=" + id + ", email=" + email + ", content=" + content
				+ "]";
	}

}
