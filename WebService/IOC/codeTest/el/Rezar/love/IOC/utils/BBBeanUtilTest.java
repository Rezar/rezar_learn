package el.Rezar.love.IOC.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import el.Rezar.love.IOC.bean.Person;
import el.Rezar.love.IOC.bean.Rezar;
import el.Rezar.love.IOC.utils.interfaces.DataConverter;

public class BBBeanUtilTest {

	@Test
	public void testPopluteFields() {
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("age", 23);
		values.put("name", "Rezar");
		values.put("person", new Person());
		values.put("date", "1972-12-07");
		BBeanUtil.registerConvert(Date.class, new DataConverter<Date>() {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-DD");

			@Override
			public Date convert(Object from) {
				if (from instanceof String) {
					String dateStr = (String) from;
					try {
						Date date = format.parse(dateStr);
						System.out.println("date is : " + date);
						return date;
					} catch (ParseException e) {
						e.printStackTrace();
						return null;
					}
				}
				return null;
			}
		});
		Rezar rezar = new Rezar();
		BBeanUtil.popluteFields(rezar, values);
		System.out.println(rezar);

	}
	
	

}
