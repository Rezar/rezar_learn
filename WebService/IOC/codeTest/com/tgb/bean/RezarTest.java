package com.tgb.bean;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.junit.Test;

import el.Rezar.love.IOC.bean.Rezar;

public class RezarTest {

	@Test
	public void test() {
		Class<?> clazz = Rezar.class;
		Method[] methods = clazz.getDeclaredMethods();
		for (Method method : methods) {
			String methodName = method.getName();
			Annotation[][] annts = method.getParameterAnnotations();
			System.out.println("Size is : " + annts[1].length);
			Class<?>[] types = method.getParameterTypes();

			for (Class<?> type : types) {
				System.out.println(type.getName());
			}
			System.out.println("-----------------------");

			for (int i = 0; i < annts.length; i++) {
				for (int j = 0; j < annts[i].length; j++) {
					Annotation annt = annts[i][j];
					System.out.println(annt.getClass().getName());
				}
			}
		}
	}

}
