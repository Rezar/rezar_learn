package el.Rezar.love.IOC.handler;

import java.util.Map;

/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/6/23
 * @Description :装饰者模式的对象接口
 */
public interface IHandler {
    //扫描指定目录下的资源，将其转化为服务描述信息，容器启动时，用这些服务描述信息初始化服务。
	/**
	 * 
	 * @param resource:指定目录下的资源
	 * @return
	 */
    public Map<String,Object> convert(String resource);

}
