package el.Rezar.love.IOC.handler;

import java.util.Map;

public class HandlerDecorator implements IHandler {

	protected IHandler handler;

	public void setHandler(IHandler handler) {
		this.handler = handler;
	}

	@Override
	public Map<String, Object> convert(String resource) {
		if (handler != null) {
			return handler.convert(resource);
		}
		return null;
	}

}
