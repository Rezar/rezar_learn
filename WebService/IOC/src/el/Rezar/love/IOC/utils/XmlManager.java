package el.Rezar.love.IOC.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 本类是专门解析XML文件的，主要用于为系统读取自己的配置文件时提供最方便的解析操作
 * 
 * @author Rezar
 * 
 */
public class XmlManager {

	/**
	 * 得到某节点下某个属性的值
	 * 
	 * @param element
	 *            要获取属性的节点
	 * @param attributeName
	 *            要取值的属性名称
	 * @return 要获取的属性的值
	 * @author Rezar
	 */
	public static String getAttribute(Element element, String attributeName) {
		return element.getAttribute(attributeName);
	}

	/**
	 * 获取指定节点下的文本
	 * 
	 * @param element
	 *            要获取文本的节点
	 * @return 指定节点下的文本
	 * @author Rezar
	 */
	public static String getText(Element element) {
		return element.getFirstChild().getNodeValue();
	}

	/**
	 * 解析某个xml文件，并在内存中创建DOM树
	 * 
	 * @param xmlFile
	 *            要解析的XML文件
	 * @return 解析某个配置文件后的Document
	 * @throws Exception
	 *             xml文件不存在
	 */
	public static Document parse(String xmlFile) throws Exception {
		// 绑定XML文件，建造DOM树
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document domTree = db.parse(xmlFile);
		return domTree;
	}

	/**
	 * 获得某节点下的某个子节点（指定子节点名称，和某个属性的值）<br>
	 * 即获取parentElement下名字叫childName，并且属性attributeName的值为attributeValue的子结点
	 * 
	 * @param parentElement
	 *            要获取子节点的那个父节点
	 * @param childName
	 *            要获取的子节点名称
	 * @param attributeName
	 *            要指定的属性名称
	 * @param attributeValue
	 *            要指定的属性的值
	 * @return 符合条件的子节点
	 * @throws Exception
	 *             子结点不存在或有多个符合条件的子节点
	 * @author HX_2008-12-01
	 */
	public static Element getChildElement(Element parentElement,
			String childName, String attributeName, String attributeValue)
			throws Exception {
		NodeList list = parentElement.getElementsByTagName(childName);
		int count = 0;
		Element curElement = null;
		for (int i = 0; i < list.getLength(); i++) {
			Element child = (Element) list.item(i);
			String value = child.getAttribute(attributeName);
			if (true == value.equals(attributeValue)) {
				curElement = child;
				count++;
			}
		}
		if (0 == count) {
			throw new Exception("找不到个符合条件的子节点！");
		} else if (1 < count) {
			throw new Exception("找到多个符合条件的子节点！");
		}

		return curElement;
	}

	/**
	 * 得到某节点下的某个子节点（通过指定子节点名称）<br>
	 * 即获取parentElement下名字叫childName的子节点
	 * 
	 * @param parentElement
	 *            要获取子节点的父节点
	 * @param childName
	 *            要获取的子节点名称
	 * @return 符合条件的子节点
	 * @throws Exception
	 *             找不到符合条件的子结点或找到多个符合条件的子节点
	 */
	public static Element getChildElement(Element parentElement,
			String childName) throws Exception {
		NodeList list = parentElement.getElementsByTagName(childName);
		Element curElement = null;
		if (1 == list.getLength()) {
			curElement = (Element) list.item(0);
		} else if (0 == list.getLength()) {
			throw new Exception("找不到个符合条件的子节点！");
		} else {
			throw new Exception("找到多个符合条件的子节点！");
		}
		return curElement;
	}

	/**
	 * 返回父節點下的所有指定名称的子节点
	 * 
	 * @param parentElement
	 * @param childName
	 * @return
	 */
	public static List<Element> getChildElements(Element parentElement,
			String childName) {
		List<Element> list = new ArrayList<Element>();
		NodeList nodeList = parentElement.getElementsByTagName(childName);
		if (nodeList != null && nodeList.getLength() > 0) {
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				list.add((Element) node);
			}
		}
		return list;
	}

	/**
	 * 根据父节点以及子节点的名称，将子节点中所有的数据封装到一个Map中，然后将各子节点的信息封装到List中进行返回
	 * 
	 * @param parentElement
	 * @param childName
	 * @return
	 */
	public static List<Map<String, String>> getChildElementsInfo(
			Element parentElement, String childName) {
		List<Map<String, String>> retList = new ArrayList<Map<String, String>>();
		List<Element> eleList = getChildElements(parentElement, childName);
		for (Element ele : eleList) {
			NamedNodeMap nnm = ele.getAttributes();
			Map<String, String> infos = new HashMap<String, String>();
			for (int i = 0; i < nnm.getLength(); i++) {
				Node node = nnm.item(i);
				String attName = node.getNodeName();
				String attValue = node.getNodeValue();
				/*
				 * System.out.println("AttName : " + attName + " ,AttValue : " +
				 * attValue);
				 */
				infos.put(attName, attValue);
			}
			retList.add(infos);
		}
		return retList;
	}

	/**
	 * 根据某个节点获取到该节点中的所有的属性值
	 * 
	 * @return
	 */
	public static Map<String, String> getElementAttributes(Element ele) {
		NamedNodeMap nnm = ele.getAttributes();
		Map<String, String> infos = new HashMap<String, String>();
		for (int i = 0; i < nnm.getLength(); i++) {
			Node node = nnm.item(i);
			String attName = node.getNodeName();
			String attValue = node.getNodeValue();
			infos.put(attName, attValue);
		}
		return infos;
	}
}
