package el.Rezar.love.IOC.utils;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlManagerTest {

	String xmlFile = File.separator + "struts.xml";
	Document doc = null;

	@Before
	public void before() throws Exception {
		doc = XmlManager.parse(FilePathOridentedUtil.getFilePath(xmlFile));
	}

	@Test
	public void testGetAttribute() {
	}

	@Test
	public void testGetText() {

	}

	@Test
	public void testParse() throws Exception {
		String xmlFile = File.separator + "MyXml.xml";
		Document doc = XmlManager.parse(FilePathOridentedUtil
				.getFilePath(xmlFile));
		System.out.println(doc.hasChildNodes());
	}

	@Test
	public void testGetChildElementElementStringStringString() {
		Element parentElement = doc.getDocumentElement();
		List<Map<String, String>> rets = XmlManager.getChildElementsInfo(
				parentElement, "constant");
		for (Map<String, String> map : rets) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				System.out.println(entry.getKey() + ":" + entry.getValue());
			}
			System.out.println("-------------------------");
		}
	}

	@Test
	public void testGetChildElementElementString() {
		Element parentElement = doc.getDocumentElement();
		System.out.println(parentElement.getNodeName());
		String childName = "bean";
		List<Element> lists = XmlManager.getChildElements(parentElement,
				childName);
		for (Element ele : lists) {
			System.out.println(ele.getAttribute("name"));
		}
	}

}
