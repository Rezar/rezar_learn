package el.Rezar.love.IOC.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import el.Rezar.love.IOC.utils.interfaces.DataConverter;

/**
 * Bean操作的工具类
 * 
 * @author Administrator
 * 
 */
public class BBeanUtil {

	private final static HashMap<Class<?>, DataConverter<?>> converts = new HashMap<Class<?>, DataConverter<?>>();

	/**
	 * 提供注册接口来对特定的数据进行转换
	 * 
	 * @param typeTo
	 * @param converter
	 */
	public static <T> void registerConvert(Class<T> typeTo,
			DataConverter<T> converter) {
		synchronized (BBeanUtil.class) {
			converts.put(typeTo, converter);
		}
	}

	/**
	 * 从转换器中去掉一个转换器对象
	 */
	public static void removeConvert(Class<?> typeTo) {
		synchronized (BBeanUtil.class) {
			converts.remove(typeTo);
		}
	}

	/**
	 * 清空当前对象拥有的对象值转换器
	 */
	public static void clear() {
		synchronized (BBeanUtil.class) {
			converts.clear();
		}
	}

	/**
	 * 通过一个Map集合给指定的值进行赋值
	 * 
	 * @param obj
	 * @param attrValues
	 */
	public static void popluteFields(Object obj,
			Map<? extends Object, ? extends Object> attrValues) {
		Class<?> clazz = obj.getClass();

		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
			PropertyDescriptor pds[] = beanInfo.getPropertyDescriptors();
			for (PropertyDescriptor pd : pds) {
				String name = pd.getName();
				if (attrValues.containsKey(name)) {
					Object mapValue = attrValues.get(name);
					Class<?> type = pd.getPropertyType();
					/* 在进行转换的时候，不允许注销一个转换器对象 */
					synchronized (BBeanUtil.class) {
						DataConverter<?> converter = findDataConverter(type);
						if (converter != null) {
							mapValue = converter.convert(mapValue);
						}
					}
					try {
						pd.getWriteMethod().invoke(obj, mapValue);
					} catch (Exception e) {
						System.out.println("赋值失败：" + name);
						// e.printStackTrace();
					}
				}
			}
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 通过某个字段给一个对象的某个属性进行赋值
	 * 
	 * @param fieldName
	 * @param obj
	 * @param refValue
	 */
	public static void setFieldValue(String fieldName, Object obj,
			Object refValue) {

		Class<?> clazz = obj.getClass();

		try {
			PropertyDescriptor pd = getPropertyDescriptor(fieldName, clazz);
			pd.getWriteMethod().invoke(obj, refValue);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * 根据某个对象的类对象已经其内部的某个属性的名称来获取该属性的一个属性描述对象
	 * 
	 * @param fieldName
	 * @param clazz
	 * @return
	 */
	private static PropertyDescriptor getPropertyDescriptor(String fieldName,
			Class<?> clazz) {
		try {
			BeanInfo bi = Introspector.getBeanInfo(clazz);
			bi.getPropertyDescriptors();
			PropertyDescriptor[] pds = bi.getPropertyDescriptors();
			for (PropertyDescriptor pd : pds) {
				String fName = pd.getName();
				if (fName.equals(fieldName)) {
					return pd;
				}
			}
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 将一个值转换为原始类型的值
	 * 
	 * @param value
	 * @param to
	 * @return
	 */
	public static Object change(String value, Class<?> to) {
		if (to == int.class || to == Integer.class) {
			return Integer.parseInt(value);
		} else if (to == float.class || to == Float.class) {
			return Float.parseFloat(value);
		} else if (to == byte.class || to == Byte.class) {
			return Byte.parseByte(value);
		} else if (to == double.class || to == Double.class) {
			return Double.parseDouble(value);
		} else if (to == short.class || to == Short.class) {
			return Short.parseShort(value);
		} else if (to == char.class || to == Character.class) {
			return value.charAt(0);
		} else {
			return value;
		}
	}

	/**
	 * 根据某个对象的某个字段获取到某个字段的类型,重载的方法
	 * 
	 * @param beanClass
	 * @param fieldName
	 * @return
	 */
	public static Class<?> getFieldType(Object obj, String fieldName) {
		Class<?> clazz = obj.getClass();
		Class<?> fieldType = getFieldType(clazz, fieldName);
		return fieldType;
	}

	/**
	 * 根据某个对象的某个字段获取到某个字段的类型
	 * 
	 * @param beanClass
	 * @param fieldName
	 * @return
	 */
	public static Class<?> getFieldType(Class<?> beanClass, String fieldName) {
		PropertyDescriptor pd = null;
		try {
			pd = new PropertyDescriptor(fieldName, beanClass);
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		assert (pd != null);
		return pd.getPropertyType();
	}

	/**
	 * 查找到一个类型转换器
	 * 
	 * @param type
	 * @return
	 */
	private static DataConverter<?> findDataConverter(Type type) {
		return converts.get(type);
	}

}
