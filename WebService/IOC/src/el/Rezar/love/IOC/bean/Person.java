package el.Rezar.love.IOC.bean;

import el.Rezar.love.IOC.annotation.Bean;
import el.Rezar.love.IOC.annotation.Param;
import el.Rezar.love.IOC.annotation.Service;

/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/6/22
 * @Description :
 */

@Bean(name = "person")
public class Person {
	@Bean(name = "car")
	private Car car;

	@Service(name = "ToSomeWhere")
	public String ToSomeWhere(@Param(name = "address") String address) {
		return car.DriveToSomeWhere(address);
	}

	@Service(name = "ToSomeWhere1")
	public String ToSomeWhere1(@Param(name = "address1") String address1) {
		return car.DriveToSomeWhere(address1);
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

}
