package el.Rezar.love.IOC.bean;

import el.Rezar.love.IOC.annotation.Bean;


/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/6/22
 * @Description :
 */

@Bean(name="car")
public class Car {
    public String DriveToSomeWhere(String address)
    {
        return "Drive to "+address;
    }
}
