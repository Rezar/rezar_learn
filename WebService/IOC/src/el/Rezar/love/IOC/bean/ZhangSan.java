package el.Rezar.love.IOC.bean;

import el.Rezar.love.IOC.annotation.Bean;
import el.Rezar.love.IOC.annotation.Param;
import el.Rezar.love.IOC.annotation.Service;

/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/7/6
 * @Description :
 */

@Bean(name = "zhangsan")
public class ZhangSan {
	@Bean(name = "person")
	private Person person;

	@Service(name = "ToSomeWhere")
	public String ToSomeWhere(@Param(name = "address") String address) {
		return person.ToSomeWhere(address);
	}

}
