package el.Rezar.love.IOC.date;

import java.util.Map;

/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/6/22
 * @Description :抽象的数据存储接口
 */
public interface IDataContext {

    public  void initData(Map<String,Object> map);

    public void set(String name,Object obj);

    public Object get(String name);

    public Map<String,Object> getAll();
}
