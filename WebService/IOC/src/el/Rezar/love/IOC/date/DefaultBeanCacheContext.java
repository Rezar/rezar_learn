package el.Rezar.love.IOC.date;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/6/22
 * @Description :默认服务提供者实例的缓存区
 */
public class DefaultBeanCacheContext implements IDataContext{
    private Map<String,Object> beanCacheMap;
    @Override
    public void initData(Map<String, Object> map) {
        beanCacheMap=new HashMap<String, Object>();
    }

    @Override
    public void set(String name, Object obj) {

        beanCacheMap.put(name,obj);
    }

    @Override
    public Object get(String name) {
        return beanCacheMap.get(name);
    }

    @Override
    public Map<String, Object> getAll() {
        return beanCacheMap;
    }
}
