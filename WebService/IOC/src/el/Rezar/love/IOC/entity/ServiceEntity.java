package el.Rezar.love.IOC.entity;

import java.util.List;

/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/6/23
 * @Description : 服务提供者所提供服务的载体
 */
public class ServiceEntity {
	private String name; // 服务名
	private String value; // 服务对应的方法名
	private List<ParamEntity> paramEntityList; // 服务对应方法的所有参数

	public ServiceEntity() {
	}

	public ServiceEntity(String name, String value,
			List<ParamEntity> paramEntityList) {

		this.name = name;
		this.value = value;
		this.paramEntityList = paramEntityList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<ParamEntity> getParamEntityList() {
		return paramEntityList;
	}

	public void setParamEntityList(List<ParamEntity> paramEntityList) {
		this.paramEntityList = paramEntityList;
	}

	@Override
	public String toString() {
		return "ServiceEntity [name=" + name + ", value=" + value
				+ ", paramEntityList=" + paramEntityList + "]";
	}

}
