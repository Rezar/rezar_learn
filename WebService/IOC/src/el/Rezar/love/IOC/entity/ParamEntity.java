package el.Rezar.love.IOC.entity;

/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/6/23
 * @Description : 服务参数的载体
 */
public class ParamEntity {
	private String name; // 参数名
	private String type; // 参数的类型
	private String value; // 参数的默认值
	private String ref; // 参数注入时候使用的对应引用的名称

	public ParamEntity() {
	}

	public ParamEntity(String name, String value) {

		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	@Override
	public String toString() {
		return "ParamEntity [name=" + name + ", type=" + type + ", value="
				+ value + ", ref=" + ref + "]";
	}

}
