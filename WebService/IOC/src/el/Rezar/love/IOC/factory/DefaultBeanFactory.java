package el.Rezar.love.IOC.factory;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import el.Rezar.love.IOC.annotation.Bean;
import el.Rezar.love.IOC.date.IDataContext;
import el.Rezar.love.IOC.entity.BeanEntity;
import el.Rezar.love.IOC.handler.HandlerDecorator;
import el.Rezar.love.IOC.utils.ReflectionUtil;

/**
 * 默认实现的一个Bean工厂
 * 
 * @author Administrator
 * 
 */
public class DefaultBeanFactory extends AbstractBeanFactory {

	/**
	 * 设置服务定义信息的存储对象
	 */
	@Override
	public void setBeanDefinitionContext(String contextClass, String resource) {
		this.beanDefinitionContext = (IDataContext) ReflectionUtil
				.getInstance(contextClass);
		Map<String, Object> beanDefininitionMap = this.handlerDecorator
				.convert(resource);
		this.beanDefinitionContext.initData(beanDefininitionMap);
	}

	/**
	 * 设置服务提供者的存储对象
	 */
	@Override
	public void setBeanCacheContext(String contextClass) {
		this.beanCacheContext = (IDataContext) ReflectionUtil
				.getInstance(contextClass);
		this.beanCacheContext.initData(new HashMap<String, Object>());
	}

	/**
	 * 获取某个服务提供者的Bean的描述信息
	 */
	@Override
	public BeanEntity getBeanEntity(String name) {
		return (BeanEntity) this.beanDefinitionContext.get(name);
	}

	@Override
	public boolean containsBeanCache(String name) {
		return !(this.beanCacheContext.get(name) == null);
	}

	@Override
	public Object getBeanCache(String name) {
		return this.beanCacheContext.get(name);
	}

	/**
	 * 创建一个Bean
	 */
	@Override
	public Object createBean(BeanEntity beanEntity) {

		Object bean = ReflectionUtil.getInstance(beanEntity.getType());
		if (beanEntity.getValue() != null) {
			return beanEntity.getValue();
		}
		Field[] fields = bean.getClass().getDeclaredFields();
		for (Field field : fields) {
			Bean beanAnnt = field.getAnnotation(Bean.class);
			if (beanAnnt == null) {
				continue;
			}
			field.setAccessible(true);
			try {
				field.set(bean, this.getBean(beanAnnt.name()));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		return bean;
	}

	@Override
	public void regiterBeanCache(String name, Object beanF) {
		this.beanCacheContext.set(name, beanF);
	}

	@Override
	public IDataContext getBeanDefinitionContext() {
		return this.beanDefinitionContext;
	}

	@Override
	public IDataContext getBeanCacheContext() {
		return this.beanCacheContext;
	}

	@Override
	public void setHandler(String defaultHandler, String handlerName) {
		this.handlerDecorator = (HandlerDecorator) ReflectionUtil
				.getInstance(defaultHandler);
		if (handlerName != null) {
			HandlerDecorator handler = (HandlerDecorator) ReflectionUtil
					.getInstance(handlerName);
			this.handlerDecorator.setHandler(handler);
		}
	}

}
