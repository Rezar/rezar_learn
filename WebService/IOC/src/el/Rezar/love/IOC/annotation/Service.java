package el.Rezar.love.IOC.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/6/22
 * @Description :用来标注服务名
 */

@Retention(RetentionPolicy.RUNTIME)
@Target( {
        ElementType.METHOD
})
public @interface Service {
    String name();
}
