package el.Rezar.love.serviceProviderFrameWork;

/**
 * 进出地铁服务接口，是服务框架定义的具体的业务
 * 
 * @author Administrator 服务定义接口
 */
public interface SubWayInterface {

	// 进入地铁
	public boolean in();

	// 离开地铁
	public boolean out();

}
