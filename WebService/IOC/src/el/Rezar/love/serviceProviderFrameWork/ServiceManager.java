package el.Rezar.love.serviceProviderFrameWork;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 服务提供者注册类
 * 
 * @author Administrator
 * 
 */
public class ServiceManager {

	private static final String DEFAULT_PROVIDER_NAME = "<def>";

	private ServiceManager() {
	}

	/* Maps service names to services */
	private static final Map<String, SubwayProviderInterface> providers = new ConcurrentHashMap<String, SubwayProviderInterface>();

	/**
	 * register a service Provider with name and subwayproviderinterface
	 * 
	 * @param name
	 * @param p
	 */
	public static void registerProvider(String name, SubwayProviderInterface p) {
		providers.put(name, p);
	}

	/**
	 * register a default provider
	 * 
	 * @param p
	 */
	public static void registerDefaultProvider(SubwayProviderInterface p) {
		providers.put(DEFAULT_PROVIDER_NAME, p);
	}

	/**
	 * access the service with default service'name
	 * 
	 * @return
	 */
	public static SubWayInterface newInstance() {
		return newInstance(DEFAULT_PROVIDER_NAME);
	}

	/**
	 * according to a service's name , return the mapping service
	 */
	public static SubWayInterface newInstance(String name) {
		SubwayProviderInterface p = providers.get(name);
		if (p == null) {
			throw new IllegalArgumentException(name
					+ " 'services was not register !");
		}
		return p.getService();
	}

}
