package cn.wenhao.learn01;

import org.objectweb.asm.Type;

public class TypeDescriptors {

	public static void main(String[] args) {
		System.out.println(Type.getDescriptor(Object.class));
		System.out.println(Type.getDescriptor(Object[].class));
		System.out.println(Type.getDescriptor(Object[][].class));
		System.out.println(Type.getDescriptor(TypeDescriptors.class));
		System.out.println(Type.getDescriptor(int[].class));
		System.out.println(Type.getDescriptor(boolean[].class));
		System.out.println(Type.getDescriptor(Integer.class));
		System.out.println(Type.getDescriptor(TypeDescriptors.class));
	}

}
