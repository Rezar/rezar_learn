package cn.wenhao.learn01;

import java.lang.reflect.Method;

import org.objectweb.asm.Type;

/**
 * 通过ASM工具获取一个方法的方法描述
 * @author Administrator
 *
 */
public class MethodDescriptors {

	
	public static void main(String[] args) throws SecurityException, NoSuchMethodException {
		Method method = String.class.getMethod("substring", int.class);
		System.out.println(Type.getMethodDescriptor(method));
		
		method = MethodDescriptors.class.getMethod("main", String[].class);
		System.out.println(Type.getMethodDescriptor(method));		
		
	}
	
}
