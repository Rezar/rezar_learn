package cn.wenhao.com.mybatis.model;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class UserTest {

	private static SqlSessionFactory sqlSessionFactory;
	private static Reader reader;

	static {
		try {
			reader = Resources.getResourceAsReader("configuration.xml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
	}

	@Test
	public void test() {
		SqlSession session = sqlSessionFactory.openSession();
		User user = session.selectOne(
				"cn.wenhao.mybatis.models.UserMapper.selectUserByID", 1);
		System.out.println(user);
	}

	@Test
	public void test_2() {
		SqlSession session = sqlSessionFactory.openSession();
		IUser iUser = session.getMapper(IUser.class);
		User user = iUser.selectUserByID(1);
		System.out.println(user);
	}

	@Test
	public void testSelectUsers() {
		SqlSession session = sqlSessionFactory.openSession();
		IUser iUser = session.getMapper(IUser.class);
		List<User> users = iUser.selectUsers("%ezar%");
		for (User user : users) {
			System.out.println(user);
		}
	}

	@Test
	public void testAddUser() {
		User user = new User();
		user.setUserAddress("江西九江都昌县");
		user.setUserAge(12);
		user.setUserName("Rezar");
		SqlSession session = sqlSessionFactory.openSession();
		IUser iUser = session.getMapper(IUser.class);
		iUser.addUser(user);
		session.commit();
		System.out.println("user is store over !");
	}

	@Test
	public void testDeleteUser() {
		SqlSession session = sqlSessionFactory.openSession();
		IUser user = session.getMapper(IUser.class);
		user.deleteUser(6);
		session.commit();
	}

	/**
	 * 测试多对一关联的情况下，查询结果的获取
	 */
	@Test
	public void testGerUserArticles() {
		SqlSession session = sqlSessionFactory.openSession();
		IUser iUser = session.getMapper(IUser.class);
		List<Article> articles = iUser.getUserArticles(1);
		for (Article a : articles) {
			System.out.println(a);
		}
	}

	@Test
	public void testForeach() {
		SqlSession session = sqlSessionFactory.openSession();
		IUser iUser = session.getMapper(IUser.class);
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(1);
		ids.add(2);
		List<User> users = iUser.getUsers(ids);
		for (User user : users) {
			System.out.println(user);
		}
	}

	@Test
	public void testForeach2() {
		SqlSession session = sqlSessionFactory.openSession();
		IUser iUser = session.getMapper(IUser.class);
		int[] ids = new int[] { 1, 2 };
		List<User> users = iUser.getUsers2(ids);
		for (User user : users) {
			System.out.println(user);
		}
	}

	@Test
	public void testForeach3() {
		SqlSession session = sqlSessionFactory.openSession();
		IUser iUser = session.getMapper(IUser.class);
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(1);
		ids.add(2);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ids);
		params.put("title", "test_title%");
		List<Article> articles = iUser.getArticles(params);
		for (Article user : articles) {
			System.out.println(user);
		}
	}

}
