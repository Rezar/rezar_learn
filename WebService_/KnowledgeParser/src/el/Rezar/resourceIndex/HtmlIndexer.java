package el.Rezar.resourceIndex;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;

import cn.wenhao.helperCode.IndexPart.otherHelpCode.IndexOperator.IIndexOperator;

/**
 * 对HTML页面进行索引的建立
 * 
 * @author Administrator
 * 
 */
public class HtmlIndexer implements IIndexOperator<HtmlResource> {

	@Override
	public boolean ifIsIndexAlready(HtmlResource obj, IndexUtil indexUtils_2) {
		return false;
	}

	@Override
	public Document indexObject(HtmlResource obj) {
		return null;
	}

	@Override
	public Document updateObject(HtmlResource obj, Term t) {
		return null;
	}

	@Override
	public Query deleteObject(HtmlResource obj) {
		return null;
	}

	@Override
	public List<String> getAllIndexField() {
		return null;
	}

}
