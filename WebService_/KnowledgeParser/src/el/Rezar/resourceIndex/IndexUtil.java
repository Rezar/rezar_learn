package el.Rezar.resourceIndex;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.NRTManager;
import org.apache.lucene.search.NRTManagerReopenThread;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.search.SearcherWarmer;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import cn.wenhao.helperCode.IndexPart.ISearcherAndIIndex.ISearcher;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.Resource;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.IndexOperator.IIndexOperator;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.CustomParser;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.INumericRangeQueryParser;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.NumericRangeQueryHelper;

/**
 * 用于对索引进行写入的操作
 * 
 * @author Administrator
 * 
 */
public class IndexUtil implements Resource {

	private File indexDirFile;

	private IndexWriter writer;

	private SearcherManager mgr = null;
	private NRTManager nrtMgr = null;

	private Directory dir;

	private PerFieldAnalyzerWrapper analyzerProvider;

	private NRTManagerReopenThread reopen;

	public IndexUtil(File indexDirFile,
			PerFieldAnalyzerWrapper analyzerProvider) throws IOException {
		this.indexDirFile = indexDirFile;
		this.analyzerProvider = analyzerProvider;
		this.dir = FSDirectory.open(this.indexDirFile);
		IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_35,
				this.analyzerProvider);
		this.writer = new IndexWriter(this.dir, iwc);
		nrtMgr = new NRTManager(writer, new SearcherWarmer() {
			@Override
			public void warm(IndexSearcher s) throws IOException {
				System.out.println("reopen");
			}
		});

		reopen = new NRTManagerReopenThread(nrtMgr, 5.0, 0.025);
		reopen.setDaemon(true);
		reopen.setName("NrtManager Reopen Thread");
		reopen.start();
		mgr = nrtMgr.getSearcherManager(true);
	}

	public void closeReopenThread() {
		try {
			this.reopen.close();
		} catch (Exception e) {
			System.out.println("NRTManagerReopenThread is close!");
		} finally {
		}
	}

	/**
	 * 通过�?��待索引的对象并�?过该对象对应的索引建立器进行相关域的索引
	 * 
	 * @param obj
	 * @param operator
	 * @throws IOException
	 */
	public <T> void indexObject(T obj, IIndexOperator<T> operator)
			throws IOException {
		synchronized (this.writer) {
			if (operator.ifIsIndexAlready(obj, this)) {
				// System.out.println("file is not change , need not to index !");
				return;
			}
		}
		Document doc = operator.indexObject(obj);
		if (doc != null) {
			synchronized (this.writer) {
				try {
					this.writer.addDocument(doc);
				} catch (Exception e) {
					System.out.println("AddDocuemnt is failure !!");
				} finally {
				}
			}
		}
	}

	public void indexObj(Document doc) {
		synchronized (this.writer) {
			try {
				this.writer.addDocument(doc);
			} catch (CorruptIndexException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public TopDocs searcherWithQuery(ISearcher searcher, int num)
			throws IOException {
		return this.searcher(searcher.makeQuery(), num);
	}

	/**
	 * 通过�?��QueryParser对象来进行搜�?
	 */
	public TopDocs searcherWithQueryParser(ISearcher searcher, int num,
			String fieldName, INumericRangeQueryParser rangeParser)
			throws ParseException, IOException {
		CustomParser parser = new CustomParser(Version.LUCENE_30, "content",
				this.analyzerProvider);
		parser.registerINumericRangeQueryParser(fieldName, rangeParser);
		Query query = searcher.makeQuery(parser);
		return this.searcher(query, num);
	}

	/**
	 * 通过�?��QueryParser对象来进行搜�?
	 */
	public TopDocs searcherWithQueryParser(ISearcher searcher, int num)
			throws ParseException, IOException {
		CustomParser parser = getCustomParser();
		Query query = searcher.makeQuery(parser);
		return this.searcher(query, num);
	}

	/**
	 * 通过�?��Query对象来进行搜�?
	 * 
	 * @param query
	 * @param num
	 * @return
	 * @throws IOException
	 */
	private TopDocs searcher(Query query, int num) throws IOException {
		// System.out.println("Query is : " + query.toString());
		TopDocs td = null;
		IndexSearcher searcher = this.mgr.acquire();
		td = searcher.search(query, num);
		return td;
	}

	/**
	 * 根据页码和分页大小获取上�?��的最后一个ScoreDoc
	 */
	private ScoreDoc getLastScoreDoc(int pageIndex, int pageSize, Query query,
			IndexSearcher searcher) throws IOException {
		if (pageIndex == 1)
			return null;// 如果是第�?��就返回空
		int num = pageSize * (pageIndex - 1);// 获取上一页的数量
		TopDocs tds = searcher.search(query, num);
		int size = tds.scoreDocs.length;
		if (num > size) {
			num = size;
		}
		return tds.scoreDocs[num - 1];
	}

	/**
	 * 根据�?��查询语句进行分页输出结果
	 * 
	 * @param queryStr
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	public List<Document> searchPageByAfter(ISearcher is,
			NumericRangeQueryHelper nrqh, int pageIndex, int pageSize) {
		IndexSearcher searcher = this.mgr.acquire();
		try {
			CustomParser parser = getCustomParser();
			parser.registerINumericRangeQueryParsers(nrqh.getNumericParser(),
					false);
			Query q = is.makeQuery(parser);
			System.out.println("Query is : " + q);
			// 先获取上�?��的最后一个元�?
			ScoreDoc lastSd = getLastScoreDoc(pageIndex, pageSize, q, searcher);
			// 通过�?���?��元素搜索下页的pageSize个元�?
			TopDocs tds = searcher.searchAfter(lastSd, q, pageSize);

			return this.searchToDocument(tds);
		} catch (org.apache.lucene.queryParser.ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				this.mgr.release(searcher);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 根据�?��Query来进行删除某条索�?
	 * 
	 * @param obj
	 * @param iio
	 * @throws IOException
	 */
	public <T> void deleteIndex(T obj, IIndexOperator<T> iio)
			throws IOException {
		Query query = iio.deleteObject(obj);
		this.nrtMgr.deleteDocuments(query);
	}

	/**
	 * 根据�?��对象来进行对象的更新
	 * 
	 * @param obj
	 * @param iio
	 * @throws IOException
	 */
	public <T> void updateIndex(T obj, IIndexOperator<T> iio)
			throws IOException {
		Term t = null;
		Document doc = iio.updateObject(obj, t);
		this.nrtMgr.updateDocument(t, doc);
	}

	/**
	 * 通过�?��查询对象来进行分页查�?
	 * 
	 * @param query
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	public TopDocs searchPageByAfter(ISearcher is, int pageIndex, int pageSize) {
		try {
			IndexSearcher searcher = this.mgr.acquire();
			Query query = is.makeQuery();
			// 先获取上�?��的最后一个元�?
			ScoreDoc lastSd = getLastScoreDoc(pageIndex, pageSize, query,
					searcher);
			// 通过�?���?��元素搜索下页的pageSize个元�?
			TopDocs tds = searcher.searchAfter(lastSd, query, pageSize);
			return tds;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 通过返回的查询对�?获取到各个对象对应的Document对象，然后将其放置在列表中进行返�?
	 * 
	 * @param tds
	 * @return
	 * @throws IOException
	 */
	public List<Document> searchToDocument(TopDocs tds) throws IOException {
		ArrayList<Document> dcs = new ArrayList<Document>();
		for (ScoreDoc sd : tds.scoreDocs) {
			Document doc = this.mgr.acquire().doc(sd.doc);
			dcs.add(doc);
		}
		return dcs;
	}

	/**
	 * 获取�?��查询解析对象
	 * 
	 * @return
	 */
	private CustomParser getCustomParser() {
		CustomParser parser = new CustomParser(Version.LUCENE_30, "content",
				this.analyzerProvider);
		return parser;
	}

	@Override
	public void close() throws InterruptedException {
		try {
			synchronized (this.writer) {
				// this.closeReopenThread();
				// this.dir.close();
				this.writer.commit();
				// Thread.sleep(200);
				this.writer.close();
				// this.nrtMgr.close();
			}
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Document getDocument(int doc) throws CorruptIndexException,
			IOException {
		IndexSearcher searcher = this.mgr.acquire();
		return searcher.doc(doc);
	}

	public IndexSearcher getIndexSearcher() {
		return this.mgr.acquire();
	}

	public Directory getDirectory() {
		return this.dir;
	}

}
