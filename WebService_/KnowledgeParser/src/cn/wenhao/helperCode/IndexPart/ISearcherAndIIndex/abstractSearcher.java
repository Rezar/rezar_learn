package cn.wenhao.helperCode.IndexPart.ISearcherAndIIndex;

import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Query;

public class abstractSearcher implements ISearcher {

	@Override
	public Query makeQuery() {
		throw new UnsupportedOperationException(
				"this method is not be override");
	}

	@Override
	public Query makeQuery(QueryParser parser) throws ParseException {
		throw new UnsupportedOperationException(
				"this method is not be override");
	}

}
