package cn.wenhao.helperCode.IndexPart.ISearcherAndIIndex;

import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;

public class Searcher extends abstractSearcher {

	@Override
	public Query makeQuery() {
		Query query = new TermQuery(new Term("fileName", "我们的爱"));
		return query;
	}

	@Override
	public Query makeQuery(QueryParser parser) {
		return null;
	}
}
