package cn.wenhao.helperCode.IndexPart.ISearcherAndIIndex;

import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Query;

/**
 * 搜索类的接口
 * 
 * <br/>
 * 先只提供两个方法，其余的方法后面添加
 * 
 * @author Administrator
 * 
 */
public interface ISearcher {

	public Query makeQuery();

	public Query makeQuery(QueryParser parser) throws ParseException;
}
