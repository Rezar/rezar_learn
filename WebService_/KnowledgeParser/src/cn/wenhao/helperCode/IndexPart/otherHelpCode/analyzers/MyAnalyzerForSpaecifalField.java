package cn.wenhao.helperCode.IndexPart.otherHelpCode.analyzers;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;

/**
 * 通过向一个分析器注册器为指定的域进行分析器的注册
 * 
 * @author Administrator
 * 
 */
public class MyAnalyzerForSpaecifalField {

	private PerFieldAnalyzerWrapper analyzerWrapper;

	public MyAnalyzerForSpaecifalField() {
		// 设置了默认的分析器时StandardAnalyzer
		this.analyzerWrapper = new PerFieldAnalyzerWrapper(
				new StandardAnalyzer(Version.LUCENE_30));
	}

	public MyAnalyzerForSpaecifalField(Analyzer defaultAnalyzer) {
		this.analyzerWrapper = new PerFieldAnalyzerWrapper(defaultAnalyzer);
	}

	public Analyzer getAnalyzer() {
		return this.analyzerWrapper;
	}

}
