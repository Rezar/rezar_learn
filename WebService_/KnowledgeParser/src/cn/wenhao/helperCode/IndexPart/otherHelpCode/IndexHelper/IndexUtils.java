package cn.wenhao.helperCode.IndexPart.otherHelpCode.IndexHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.NRTManager;
import org.apache.lucene.search.NRTManagerReopenThread;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.search.SearcherWarmer;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import cn.wenhao.helperCode.IndexPart.ISearcherAndIIndex.ISearcher;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.IndexOperator.IIndexOperator;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.CustomParser;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.INumericRangeQueryParser;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.NumericRangeQueryHelper;

/**
 * 根据一个对象和其对应的索引建立对象来构建一个文档添加进本系统的索引中去
 * 
 * @author Administrator
 * 
 */
public class IndexUtils {

	private File dirFile;
	private PerFieldAnalyzerWrapper analyzerProvider;
	private IndexWriter writer;
	private Directory dir;
	private boolean reset;
	private NRTManager nrtMgr = null;
	private SearcherManager sm = null;
	private NRTManagerReopenThread reopen;

	public IndexUtils(File indexDirFile, PerFieldAnalyzerWrapper analyzers)
			throws IOException {
		this(indexDirFile, analyzers, false);
	}

	public IndexUtils(File indexDirFile, PerFieldAnalyzerWrapper analyzers,
			boolean reset) throws IOException {
		this.dirFile = indexDirFile;
		if (!this.dirFile.exists()) {
			throw new FileNotFoundException(this.dirFile.getAbsolutePath()
					+ " not Found !");
		}
		this.dir = FSDirectory.open(dirFile);
		this.analyzerProvider = analyzers;
		this.reset = reset;
		this.writer = getWriter(reset);
		/**
		 * 在每次reader重新打开的时候，会调用这个对象的方法
		 */
		SearcherWarmer warmer = new SearcherWarmer() {
			@Override
			public void warm(IndexSearcher arg0) throws IOException {
				System.out.println("reOpen !");
			}
		};
		this.nrtMgr = new NRTManager(writer, Executors.newCachedThreadPool(),
				warmer);
		reopen = new NRTManagerReopenThread(nrtMgr, 5.0, 0.025);
		this.sm = this.nrtMgr.getSearcherManager(true);
		reopen.setDaemon(true);
		reopen.setName("NrtManager Reopen Thread");
		reopen.start();
		if (reset) {
			this.nrtMgr.deleteAll();
		}
	}

	public void closeNRTManagerReopenThread() {
		reopen.close();
	}

	/**
	 * 通过一个待索引的对象并通过该对象对应的索引建立器进行相关域的索引
	 * 
	 * @param obj
	 * @param operator
	 * @throws IOException
	 */
	public <T> void indexObject(T obj, IIndexOperator<T> operator)
			throws IOException {
		// synchronized (this.nrtMgr) {
		// if (operator.ifIsIndexAlready(obj, this)) {
		// System.out.println("file is not change , need not to index !");
		// return;
		// }
		// }
		Document doc = operator.indexObject(obj);
		// synchronized (this.nrtMgr) {
		if (doc != null) {
			try {
				this.nrtMgr.addDocument(doc);
			} catch (Exception e) {
				System.out.println("AddDocuemnt is failure !!");
			} finally {
			}
		}
		// }
	}

	public TopDocs searcherWithQuery(ISearcher searcher, int num)
			throws IOException {
		return this.searcher(searcher.makeQuery(), num);
	}

	/**
	 * 通过一个QueryParser对象来进行搜索
	 */
	public TopDocs searcherWithQueryParser(ISearcher searcher, int num,
			String fieldName, INumericRangeQueryParser rangeParser)
			throws ParseException, IOException {
		CustomParser parser = new CustomParser(Version.LUCENE_30, "content",
				this.analyzerProvider);
		parser.registerINumericRangeQueryParser(fieldName, rangeParser);
		Query query = searcher.makeQuery(parser);
		return this.searcher(query, num);
	}

	/**
	 * 通过一个QueryParser对象来进行搜索
	 */
	public TopDocs searcherWithQueryParser(ISearcher searcher, int num)
			throws ParseException, IOException {
		CustomParser parser = getCustomParser();
		Query query = searcher.makeQuery(parser);
		return this.searcher(query, num);
	}

	/**
	 * 通过一个Query对象来进行搜索
	 * 
	 * @param query
	 * @param num
	 * @return
	 * @throws IOException
	 */
	private TopDocs searcher(Query query, int num) throws IOException {
		// System.out.println("Query is : " + query.toString());
		TopDocs td = null;
		IndexSearcher searcher = this.sm.acquire();
		td = searcher.search(query, num);
		return td;
	}

	/**
	 * 根据页码和分页大小获取上一次的最后一个ScoreDoc
	 */
	private ScoreDoc getLastScoreDoc(int pageIndex, int pageSize, Query query,
			IndexSearcher searcher) throws IOException {
		if (pageIndex == 1)
			return null;// 如果是第一页就返回空
		int num = pageSize * (pageIndex - 1);// 获取上一页的数量
		TopDocs tds = searcher.search(query, num);
		int size = tds.scoreDocs.length;
		if (num > size) {
			num = size;
		}
		try {
			return tds.scoreDocs[num - 1];
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return null;
	}

	/**
	 * 根据一个查询语句进行分页输出结果
	 * 
	 * @param queryStr
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	public List<Document> searchPageByAfter(ISearcher is,
			NumericRangeQueryHelper nrqh, int pageIndex, int pageSize) {
		try {
			IndexSearcher searcher = this.sm.acquire();
			CustomParser parser = getCustomParser();
			parser.registerINumericRangeQueryParsers(nrqh.getNumericParser(),
					false);
			Query q = is.makeQuery(parser);
			System.out.println("Query is : " + q);
			// 先获取上一页的最后一个元素
			ScoreDoc lastSd = getLastScoreDoc(pageIndex, pageSize, q, searcher);
			if (lastSd == null) {
				return null;
			}
			// 通过最后一个元素搜索下页的pageSize个元素
			TopDocs tds = searcher.searchAfter(lastSd, q, pageSize);

			return this.searchToDocument(tds);
		} catch (org.apache.lucene.queryParser.ParseException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		}
		return null;
	}

	/**
	 * 根据一个Query来进行删除某条索引
	 * 
	 * @param obj
	 * @param iio
	 * @throws IOException
	 */
	public <T> void deleteIndex(T obj, IIndexOperator<T> iio)
			throws IOException {
		Query query = iio.deleteObject(obj);
		this.nrtMgr.deleteDocuments(query);
	}

	/**
	 * 根据一个对象来进行对象的更新
	 * 
	 * @param obj
	 * @param iio
	 * @throws IOException
	 */
	public <T> void updateIndex(T obj, IIndexOperator<T> iio)
			throws IOException {
		Term t = null;
		Document doc = iio.updateObject(obj, t);
		this.nrtMgr.updateDocument(t, doc);
	}

	/**
	 * 通过一个查询对象来进行分页查询
	 * 
	 * @param query
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	public TopDocs searchPageByAfter(ISearcher is, int num, int pageIndex,
			int pageSize) {
		try {
			IndexSearcher searcher = this.sm.acquire();
			Query query = is.makeQuery();
			// 先获取上一页的最后一个元素
			ScoreDoc lastSd = getLastScoreDoc(pageIndex, pageSize, query,
					searcher);
			// 通过最后一个元素搜索下页的pageSize个元素
			TopDocs tds = searcher.searchAfter(lastSd, query, pageSize);
			return tds;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 通过返回的查询对象 获取到各个对象对应的Document对象，然后将其放置在列表中进行返回
	 * 
	 * @param tds
	 * @return
	 * @throws IOException
	 */
	public List<Document> searchToDocument(TopDocs tds) throws IOException {
		ArrayList<Document> dcs = new ArrayList<Document>();
		for (ScoreDoc sd : tds.scoreDocs) {
			Document doc = this.sm.acquire().doc(sd.doc);
			dcs.add(doc);
		}
		return dcs;
	}

	public IndexSearcher getIndexSearcher() {
		return this.sm.acquire();
	}

	protected synchronized IndexWriter getWriter(boolean reset)
			throws IOException {
		if (this.writer == null) {
			this.writer = new IndexWriter(this.dir, new IndexWriterConfig(
					Version.LUCENE_35, this.analyzerProvider));
		}
		return this.writer;
	}

	protected Directory getDir() {
		return this.dir;
	}

	public Document getDocument(int docId) throws IOException {
		return this.sm.acquire().doc(docId);
	}

	public IndexWriter getWriter() {
		return this.writer;
	}

	public void resetIndex() throws IOException {
		synchronized (writer) {
			this.writer.deleteAll();
		}
	}

	protected synchronized void closeSource() throws CorruptIndexException,
			IOException {
		if (this.writer != null) {
			this.nrtMgr.close();
			this.writer.close();
		}

		if (this.dir != null) {
			this.dir.close();
		}
	}

	public Analyzer getAnalyzer() {
		return this.analyzerProvider;
	}

	/**
	 * 获取一个查询解析对象
	 * 
	 * @return
	 */
	private CustomParser getCustomParser() {
		CustomParser parser = new CustomParser(Version.LUCENE_30, "content",
				this.analyzerProvider);
		return parser;
	}

	public void close() throws IOException {
		this.nrtMgr.close();
	}

}
