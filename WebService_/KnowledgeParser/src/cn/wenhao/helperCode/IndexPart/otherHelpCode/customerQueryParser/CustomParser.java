package cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser;

import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;

public class CustomParser extends QueryParser {

	private Map<String, INumericRangeQueryParser> rangeParser = new HashMap<String, INumericRangeQueryParser>();

	public CustomParser(Version matchVersion, String f, Analyzer a) {
		super(matchVersion, f, a);
	}

	/**
	 * 通过一个Map集合向转换器中添加内容
	 * 
	 * @param parsers
	 * @param clear
	 */
	public void registerINumericRangeQueryParsers(
			Map<String, INumericRangeQueryParser> parsers, boolean clear) {
		if (clear) {
			this.rangeParser.clear();
		}
		this.rangeParser.putAll(parsers);
	}

	public void registerINumericRangeQueryParser(String fieldName,
			INumericRangeQueryParser nrq) {
		if (fieldName != null && nrq != null) {
			this.rangeParser.put(fieldName, nrq);
		}
	}

	@Override
	protected Query getWildcardQuery(String field, String termStr)
			throws ParseException {
		throw new ParseException("由于性能原因，已经禁用了通配符查询，请输入更精确的信息进行查询");
	}

	@Override
	protected Query getFuzzyQuery(String field, String termStr,
			float minSimilarity) throws ParseException {
		throw new ParseException("由于性能原因，已经禁用了模糊查询，请输入更精确的信息进行查询");
	}

	@Override
	protected Query getRangeQuery(String field, String part1, String part2,
			boolean inclusive) throws ParseException {

		INumericRangeQueryParser rangeParser = this.rangeParser.get(field);
		if (rangeParser != null) {
			return rangeParser.generatorNumericRangQuery(field, part1, part2,
					inclusive);
		}
		return super.newRangeQuery(field, part1, part2, inclusive);
	}
}
