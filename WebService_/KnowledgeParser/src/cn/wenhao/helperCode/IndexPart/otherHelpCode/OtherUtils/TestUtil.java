package cn.wenhao.helperCode.IndexPart.otherHelpCode.OtherUtils;

import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

public class TestUtil {

	public static int hitCount(IndexSearcher searcher, Query query)
			throws IOException {
		return searcher.search(query, 1).totalHits;
	}

	/**
	 * 
	 * According to a an IndexSearcher objects, and Query object and the number
	 * of results to display,to display the search results
	 * 
	 * @param searcher
	 * @param query
	 * @param num
	 */
	public static void printSearch(IndexSearcher searcher, Query query,
			int num, String... fieldNames) {
		System.out
				.println("====================开始打印检索结果=============================");
		String defaultStr = "content";
		boolean isDefault = false;
		if (fieldNames.length == 0) {
			isDefault = true;
		}
		TopDocs tops;
		try {
			tops = searcher.search(query, num);
			System.out.println("Total hits is : " + tops.totalHits);
			for (ScoreDoc sd : tops.scoreDocs) {
				Document doc = searcher.doc(sd.doc);
				if (isDefault) {
					System.out.println(doc.get(defaultStr));
				} else {
					System.out.print("[");
					for (String fieldName : fieldNames) {
						System.out.print(fieldName + ":" + doc.get(fieldName)
								+ "	");
					}
					System.out.println("]");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out
				.println("======================打印检索结果结束===========================\r\n");
	}

	public static boolean hitsIncludeTitle(IndexSearcher searcher,
			TopDocs hits, String title) throws IOException {
		for (ScoreDoc sd : hits.scoreDocs) {
			Document doc = searcher.doc(sd.doc);
			if (title.equals(doc.get("title"))) {
				return true;
			}
		}
		System.out.println("title : " + title + "'Not Found!'");
		return false;
	}

	public static void printSearch(IndexSearcher searcher, TopDocs tops,
			String... fieldNames) {
		System.out
				.println("====================开始打印检索结果=============================");
		String defaultStr = "content";
		boolean isDefault = false;
		if (fieldNames.length == 0) {
			isDefault = true;
		}
		try {
			System.out.println("Total hits is : " + tops.totalHits);
			for (ScoreDoc sd : tops.scoreDocs) {
				Document doc = searcher.doc(sd.doc);
				if (isDefault) {
					System.out.println(doc.get(defaultStr));
				} else {
					System.out.print("[");
					for (String fieldName : fieldNames) {
						System.out.print(fieldName + ":" + doc.get(fieldName)
								+ "	");
					}
					System.out.println("]");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out
				.println("======================打印检索结果结束===========================\r\n");
	}

}
