package cn.wenhao.helperCode.IndexPart.otherHelpCode.searcherForFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.wenhao.helperCode.IndexPart.otherHelpCode.IndexHelper.IndexUtils;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.NumericRangeQueryHelper;

/**
 * 用于提供给文件搜索服务的类
 * 
 * @author Administrator
 * 
 */
public abstract class ASearcherOperator<T> {

	protected IndexUtils iu;

	public ASearcherOperator(IndexUtils iu) {
		this.iu = iu;
	}

	/**
	 * 用于封装多个用户的查询条件
	 */
	private Map<String, String> muiltQueryValue = new HashMap<String, String>();

	public void putSearchQueryStr(String fieldName, String value) {
		this.muiltQueryValue.put(fieldName, value);
	}

	/**
	 * 此方法只适用于多个OR 操作<br/>
	 * 如果是AND操作，不应该使用这个方法
	 * 
	 * @return
	 */
	public String getQueryString() {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : this.muiltQueryValue.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			sb.append(key).append(":").append(value).append(" ");
		}
		this.muiltQueryValue.clear();
		return sb.toString();
	}

	/**
	 * 将一个返回的结果封装成自己的查询对象
	 * 
	 * @param tds
	 * @param searcher
	 * @return
	 */
	public abstract List<T> changeTopdsToT(NumericRangeQueryHelper nrqh,
			int pageIndex, int pageSize);

	public abstract List<T> changeTopdsToT(int pageIndex, int pageSize);

	public abstract List<T> changeTopdsToT(String queryStr,
			NumericRangeQueryHelper nrqh, int pageIndex, int pageSize);

	public abstract List<T> changeTopdsToT(String queryStr, int pageIndex,
			int pageSize);

}
