package cn.wenhao.helperCode.IndexPart.otherHelpCode;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ResourcePool<T extends Resource> {

	private final BlockingQueue<T> objPool;
	private final int poolSize;

	public ResourcePool(T[] initResouce) {
		this.objPool = new LinkedBlockingQueue<T>();
		this.objPool.addAll(Arrays.asList(initResouce));
		this.poolSize = initResouce.length;
	}

	public ResourcePool(InitResource<T> ir) {
		this(ir.getInitResource());
	}

	public T getResource() throws InterruptedException {
		return this.objPool.take();
	}

	/**
	 * 
	 * 暂时使用顺序占有资源的方式
	 * 
	 * @param resource
	 * @throws InterruptedException
	 */
	public void relase(T resource) throws InterruptedException {
		this.objPool.put(resource);
	}

	/**
	 * 关闭资源的方法
	 * 
	 * @throws InterruptedException
	 */
	public synchronized void close() throws InterruptedException {
		System.out.println("准备关闭所有的资源！");
		int size = this.objPool.size();
		boolean isOk = true;
		InterruptedException e_ = null;
		if (size == this.poolSize) {
			while (true) {
				T obj = this.objPool.remove();
				System.out.println();
				if (obj == null) {
					break;
				}
				try {
					obj.close();
				} catch (InterruptedException e) {
					isOk = false;
					e_ = e;
					e.printStackTrace();
				}
			}
		}
		if (isOk && e_ != null) {
			throw e_;
		}
		System.out.println("Queue reading to clear !");
		this.objPool.clear();
	}
}
