package cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.impl;

import org.apache.lucene.search.NumericRangeQuery;

import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.INumericRangeQueryParser;

public class IntNumericRangQuery implements INumericRangeQueryParser {

	@Override
	public NumericRangeQuery<? extends Number> generatorNumericRangQuery(
			String field, String start, String end, boolean inclusive) {
		try {
			Integer start_ = Integer.parseInt(start);
			Integer end_ = Integer.parseInt(end);
			NumericRangeQuery<Integer> nrq = NumericRangeQuery.newIntRange(
					field, start_, end_, inclusive, inclusive);
			return nrq;
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return null;
	}

}
