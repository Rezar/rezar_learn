package cn.wenhao.helperCode.IndexPart.otherHelpCode.searcherForFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Query;

import cn.wenhao.helperCode.IndexPart.ISearcherAndIIndex.ISearcher;
import cn.wenhao.helperCode.IndexPart.ISearcherAndIIndex.abstractSearcher;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.IndexHelper.IndexUtils;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.NumberType;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.NumericRangeQueryHelper;

public class FileSearcherOperator extends ASearcherOperator<File> {

	private static NumericRangeQueryHelper nrqh = new NumericRangeQueryHelper(
			false);
	static {
		nrqh.addNumericParser("fileSize", NumberType.LONG);
		nrqh.addNumericParser("lastModify", NumberType.LONG);
	}

	public FileSearcherOperator(IndexUtils iu) {
		super(iu);
	}

	@Override
	public List<File> changeTopdsToT(NumericRangeQueryHelper nrqh,
			int pageIndex, int pageSize) {

		List<File> files = new ArrayList<File>();
		ISearcher is = new abstractSearcher() {

			@Override
			public Query makeQuery(QueryParser parser) throws ParseException {
				String queryStr = getQueryString();
				return parser.parse(queryStr);
			}

		};
		List<Document> docs = this.iu.searchPageByAfter(is, nrqh, pageIndex,
				pageSize);
		for (Document doc : docs) {
			File file = docToFile(doc);
			if (file != null) {
				files.add(file);
			}
		}

		return files;
	}

	@Override
	public void putSearchQueryStr(String fieldName, String value) {
		super.putSearchQueryStr(fieldName, value);
	}

	@Override
	public String getQueryString() {
		return super.getQueryString();
	}

	@Override
	public List<File> changeTopdsToT(final String queryStr,
			NumericRangeQueryHelper nrqh, int pageIndex, int pageSize) {
		List<File> files = new ArrayList<File>();
		ISearcher is = new abstractSearcher() {
			@Override
			public Query makeQuery(QueryParser parser) throws ParseException {
				return parser.parse(queryStr);
			}
		};
		List<Document> docs = this.iu.searchPageByAfter(is, nrqh, pageIndex,
				pageSize);
		if (docs == null) {
			return files;
		}
		for (Document doc : docs) {
			File file = docToFile(doc);
			if (file != null) {
				files.add(file);
			}
		}

		return files;
	}

	private File docToFile(Document doc) {
		String filePath = doc.get("filePath");
		File file = new File(filePath);
		if (!file.exists()) {
			return null;
		}
		return file;
	}

	@Override
	public List<File> changeTopdsToT(int pageIndex, int pageSize) {
		return this.changeTopdsToT(nrqh, pageIndex, pageSize);
	}

	@Override
	public List<File> changeTopdsToT(String queryStr, int pageIndex,
			int pageSize) {
		return this.changeTopdsToT(queryStr, nrqh, pageIndex, pageSize);
	}

}
