package cn.wenhao.helperCode.IndexPart.otherHelpCode.OtherUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;

public class FileUtils {

	/**
	 * 获取文件的类型，即该文件的后缀
	 * 
	 * @param file
	 * @return
	 */
	public static String getFileKind(File file) {
		String fileName = file.getName();
		int index = fileName.lastIndexOf(".");
		String ret = fileName.substring(index + 1);
		return ret;
	}

	/**
	 * 根据传入的文件的大小，将文件转换为GB，MB，Byte的表达形式
	 * 
	 * @param size
	 * @param status
	 * @return
	 */
	public static long changeToGOrMOrByte(long size, SizeStatus status) {

		if (status == SizeStatus.GB) {
			return (size / 1024 / 1024 / 1024);
		} else if (status == SizeStatus.MB) {
			return (size / 1024 / 1024);
		} else {
			return size;
		}

	}

	/**
	 * 判断某个文件是否是文本文件类型
	 * 
	 * @param file
	 * @return
	 */
	public static boolean isTextFile(File file) {
		boolean isFile = false;
		Properties prop = new Properties();
		Reader inStream = null;
		try {
			inStream = new FileReader("TextExtension.properties");
			prop.load(inStream);
			String extension = prop.getProperty("Text");
			String fileExt = FilenameUtils.getExtension(file.getName());
			String[] exts = extension.split(" +");
			for (String ext : exts) {
				if (ext.toLowerCase().equals(fileExt.toLowerCase())) {
					return true;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return isFile;
	}

}
