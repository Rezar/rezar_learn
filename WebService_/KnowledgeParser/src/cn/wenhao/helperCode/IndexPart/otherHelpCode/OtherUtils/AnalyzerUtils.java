package cn.wenhao.helperCode.IndexPart.otherHelpCode.OtherUtils;

import java.io.IOException;
import java.io.StringReader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

@SuppressWarnings("deprecation")
public class AnalyzerUtils {

	/**
	 * Call analysis
	 * 
	 * @param analyzer
	 * @param text
	 */
	public static void displayTokens(Analyzer analyzer, String text) {
		displayTokens(analyzer.tokenStream("content", new StringReader(text)));
	}

	public static void displayTokens(TokenStream stream) {
		TermAttribute term = stream.addAttribute(TermAttribute.class);
		try {
			while (stream.incrementToken()) {
				// Output parenthesized(带...括号的) vocabulary unit
				System.out.print("[" + term.term() + "] ");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Observe the vocabulary unit items, offset, type and location increments
	 * 
	 * @param analyzer
	 * @param text
	 * @throws IOException
	 */
	public static void displayTokensWithFullDetails(Analyzer analyzer,
			String text) throws IOException {
		System.out.println("analyzing... text : \"" + text
				+ "\",useing analyzer : \" "
				+ analyzer.getClass().getSimpleName() + "\"");
		TokenStream stream = analyzer.tokenStream("content", new StringReader(
				text));
		TermAttribute term = stream.addAttribute(TermAttribute.class);
		PositionIncrementAttribute posIncr = stream
				.addAttribute(PositionIncrementAttribute.class);
		OffsetAttribute offset = stream.addAttribute(OffsetAttribute.class);
		TypeAttribute type = stream.addAttribute(TypeAttribute.class);
		int position = 0;
		while (stream.incrementToken()) {
			// With previous vocabulary unit are [increment] units apart,default
			// is 1
			int increment = posIncr.getPositionIncrement();
			if (increment > 0) {
				position = position + increment;
				System.out.println();
				System.out.print(position + "\t( " + increment + ")" + ": ");
			}
			System.out.print("[" + term.term() + ":" + offset.startOffset()
					+ "-->" + offset.endOffset() + ":" + type.type() + "] ");
		}
		System.out.println();
	}

	/**
	 * 
	 * According to a an IndexSearcher objects, and Query object and the number
	 * of results to display,to display the search results
	 * 
	 * @param searcher
	 * @param query
	 * @param num
	 */
	public static void printSearch(IndexSearcher searcher, Query query, int num) {

		TopDocs tops;
		try {
			tops = searcher.search(query, num);
			for (ScoreDoc sd : tops.scoreDocs) {
				Document doc = searcher.doc(sd.doc);
				System.out.println(doc.get("content"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
