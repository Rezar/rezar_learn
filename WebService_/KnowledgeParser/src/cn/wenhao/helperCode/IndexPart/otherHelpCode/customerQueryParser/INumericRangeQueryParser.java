package cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser;

import org.apache.lucene.search.NumericRangeQuery;

public interface INumericRangeQueryParser {

	public NumericRangeQuery<? extends Number> generatorNumericRangQuery(
			String field, String start, String end, boolean inclusive);

}
