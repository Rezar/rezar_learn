package cn.wenhao.helperCode.IndexPart.otherHelpCode.OtherUtils;

import java.text.SimpleDateFormat;

public class Utils {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");

	public static long formatDate(long date) {
		String II = sdf.format(date);
		return Long.parseLong(II);
	}

}
