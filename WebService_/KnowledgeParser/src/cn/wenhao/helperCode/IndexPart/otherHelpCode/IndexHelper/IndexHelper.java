package cn.wenhao.helperCode.IndexPart.otherHelpCode.IndexHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.SearcherWarmer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import cn.wenhao.helperCode.IndexPart.otherHelpCode.IndexOperator.IIndexOperator;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.CustomParser;

/**
 * 根据一个对象和其对应的索引建立对象来构建一个文档添加进本系统的索引中去
 * 
 * @author Administrator
 * 
 */
public class IndexHelper {

	private File dirFile;
	private PerFieldAnalyzerWrapper analyzerProvider;
	private IndexWriter writer;
	private Directory dir;
	private boolean reset;

	public IndexHelper(File indexDirFile, PerFieldAnalyzerWrapper analyzers)
			throws IOException {
		this(indexDirFile, analyzers, false);
	}

	public IndexHelper(File indexDirFile, PerFieldAnalyzerWrapper analyzers,
			boolean reset) throws IOException {
		this.dirFile = indexDirFile;
		if (!this.dirFile.exists()) {
			throw new FileNotFoundException(this.dirFile.getAbsolutePath()
					+ " not Found !");
		}
		this.dir = FSDirectory.open(dirFile);
		this.analyzerProvider = analyzers;
		this.reset = reset;
		this.writer = getWriter(reset);
		/**
		 * 在每次reader重新打开的时候，会调用这个对象的方法
		 */
		SearcherWarmer warmer = new SearcherWarmer() {
			@Override
			public void warm(IndexSearcher arg0) throws IOException {
				System.out.println("reOpen !");
			}
		};
	}

	/**
	 * 通过一个待索引的对象并通过该对象对应的索引建立器进行相关域的索引
	 * 
	 * @param obj
	 * @param operator
	 * @throws IOException
	 */
	public <T> void indexObject(T obj, IIndexOperator<T> operator)
			throws IOException {
		// synchronized (this.nrtMgr) {
		// if (operator.ifIsIndexAlready(obj, this)) {
		// System.out.println("file is not change , need not to index !");
		// return;
		// }
		// }
		Document doc = operator.indexObject(obj);
		// synchronized (this.nrtMgr) {
		if (doc != null) {
			try {
				this.writer.addDocument(doc);
			} catch (Exception e) {
				System.out.println("AddDocuemnt is failure !!");
			} finally {
			}
		}
		// }
	}

	protected synchronized IndexWriter getWriter(boolean reset)
			throws IOException {
		if (this.writer == null) {
			this.writer = new IndexWriter(this.dir, new IndexWriterConfig(
					Version.LUCENE_35, this.analyzerProvider));
		}
		return this.writer;
	}

	public Analyzer getAnalyzer() {
		return this.analyzerProvider;
	}

	/**
	 * 获取一个查询解析对象
	 * 
	 * @return
	 */
	private CustomParser getCustomParser() {
		CustomParser parser = new CustomParser(Version.LUCENE_30, "content",
				this.analyzerProvider);
		return parser;
	}

	public void close() {
		try {
			this.writer.close();
		} catch (CorruptIndexException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
