package cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.impl;

import org.apache.lucene.search.NumericRangeQuery;

import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.INumericRangeQueryParser;

public class DoubleNumericRangeQuery implements INumericRangeQueryParser {

	@Override
	public NumericRangeQuery<? extends Number> generatorNumericRangQuery(
			String field, String start, String end, boolean inclusive) {
		try {
			Double start_ = Double.parseDouble(start);
			Double end_ = Double.parseDouble(end);

			return NumericRangeQuery.newDoubleRange(field, start_, end_,
					inclusive, inclusive);

		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		return null;
	}

}
