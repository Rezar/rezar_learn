package cn.wenhao.helperCode.IndexPart.otherHelpCode.FileIfIsExites;

import java.io.File;

import org.apache.lucene.store.Directory;

import cn.wenhao.helperCode.IndexPart.otherHelpCode.IFileIfIsExites.IFileIfIsExites;

/**
 * 判断某个文件是否已经进行索引
 * 
 * @author Administrator
 * 
 */
public class FileIfIsExites implements IFileIfIsExites {

	/* 索引进行完毕后保存的索引文件的目录 */
	@SuppressWarnings("unused")
	private Directory dir;

	/**
	 * 根据输入的一个文件，判断该文件是否已经进行索引
	 */
	@Override
	public boolean isExites(File file) {
		return false;
	}

}
