package cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.impl;

import org.apache.lucene.search.NumericRangeQuery;

import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.INumericRangeQueryParser;

public class FloatNumericRangeQuery implements INumericRangeQueryParser {

	@Override
	public NumericRangeQuery<? extends Number> generatorNumericRangQuery(
			String field, String start, String end, boolean inclusive) {
		try {
			Float start_ = Float.parseFloat(start);
			Float end_ = Float.parseFloat(end);

			return NumericRangeQuery.newFloatRange(field, start_, end_,
					inclusive, inclusive);

		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		return null;
	}
}
