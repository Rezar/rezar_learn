package cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser;

import java.util.HashMap;
import java.util.Map;

import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.impl.DoubleNumericRangeQuery;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.impl.FloatNumericRangeQuery;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.impl.IntNumericRangQuery;
import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.impl.LongNumericRangeQuery;

public class NumericRangeQueryHelper {

	private final Map<String, INumericRangeQueryParser> numericParser = new HashMap<String, INumericRangeQueryParser>();
	private boolean clear;

	public NumericRangeQueryHelper(boolean clear) {
		this.clear = clear;
	}

	public void addNumericParser(String fieldName, NumberType nt) {
		if (nt.equals(NumberType.INT)) {
			numericParser.put(fieldName, new IntNumericRangQuery());
		} else if (nt.equals(NumberType.FLOAT)) {
			numericParser.put(fieldName, new FloatNumericRangeQuery());
		} else if (nt.equals(NumberType.LONG)) {
			numericParser.put(fieldName, new LongNumericRangeQuery());
		} else if (nt.equals(NumberType.DOUBLE)) {
			numericParser.put(fieldName, new DoubleNumericRangeQuery());
		}
	}

	public Map<String, INumericRangeQueryParser> getNumericParser() {
		Map<String, INumericRangeQueryParser> ret = new HashMap<String, INumericRangeQueryParser>(
				this.numericParser);
		if (clear) {
			this.numericParser.clear();
		}
		return ret;
	}

}
