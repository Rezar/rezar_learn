package cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.impl;

import org.apache.lucene.search.NumericRangeQuery;

import cn.wenhao.helperCode.IndexPart.otherHelpCode.customerQueryParser.INumericRangeQueryParser;

public class LongNumericRangeQuery implements INumericRangeQueryParser {

	@Override
	public NumericRangeQuery<? extends Number> generatorNumericRangQuery(
			String field, String start, String end, boolean inclusive) {

		try {
			Long start_ = Long.parseLong(start);
			Long end_ = Long.parseLong(end);

			return NumericRangeQuery.newLongRange(field, start_, end_,
					inclusive, inclusive);

		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		return null;
	}

}
