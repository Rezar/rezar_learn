package cn.wenhao.helperCode.IndexPart.otherHelpCode.OtherUtils;

import java.io.File;

public class PublicValue {

	public static final String FILE_SEPARATOR = File.separator;

	public static final String SKIP_FILE_PATH = "SkipFile.properties";
	
	public static final String LINE_SEPARATOR = File.separator;

}
