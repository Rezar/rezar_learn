package cn.wenhao.helperCode.IndexPart.otherHelpCode.IndexOperator;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;

import el.Rezar.resourceIndex.IndexUtil;

public interface IIndexOperator<T> {

	/**
	 * 通过该方法来判断某个对象是否需要再次进行索引
	 * 
	 * @param obj
	 * @return
	 */
	public boolean ifIsIndexAlready(T obj, IndexUtil indexUtils_2);

	public Document indexObject(T obj);

	public Document updateObject(T obj,Term t);

	public Query deleteObject(T obj);

	/**
	 * 获取该对象进行索引的所有域
	 * 
	 * @return
	 */
	public List<String> getAllIndexField();
}
