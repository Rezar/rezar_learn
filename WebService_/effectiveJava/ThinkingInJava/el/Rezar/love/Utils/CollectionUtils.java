package el.Rezar.love.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import el.Rezar.love.泛型.GenericsParaminterface.Generator;

/**
 * 生成集合对象的工具类①
 * 
 * 
 * @author Administrator
 * 
 */
public class CollectionUtils {

	public static <K, V> Map<K, V> map() {
		return new HashMap<K, V>();
	}

	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> map(Class<?> mapClass) {
		try {
			return (Map<K, V>) mapClass.newInstance();
		} catch (Exception e) {
			return map();
		}
	}

	public static <V> List<V> list() {
		return new ArrayList<V>();
	}

	@SuppressWarnings("unchecked")
	public static <V> List<V> list(Class<?> listClass) {
		try {
			return (List<V>) listClass.newInstance();
		} catch (Exception e) {
			return list();
		}
	}

	public static <V> Set<V> set() {
		return new HashSet<V>();
	}

	@SuppressWarnings("unchecked")
	public static <V> Set<V> set(Class<?> setClass) {
		try {
			return (Set<V>) setClass.newInstance();
		} catch (Exception e) {
			return set();
		}
	}

	/**
	 * 类似于Arrays.asList()方法
	 * 
	 * @param args
	 * @return
	 */
	public static <T> List<T> makeList(T... args) {
		List<T> list = list();
		for (T arg : args) {
			list.add(arg);
		}
		return list;
	}

	/**
	 * 根据一个对象的生成器来填充一个集合
	 * 
	 * @param coll
	 * @param gen
	 * @param n
	 * @return
	 */
	public static <T> Collection<T> fill(Collection<T> coll, Generator<T> gen,
			int n) {
		for (int i = 0; i < n; i++) {
			coll.add(gen.next());
		}
		return coll;
	}

	/**
	 * 自动填充一个T类型的数组
	 * 
	 * @param a
	 * @param gen
	 * @return
	 */
	public static <T> T[] fill(T[] a, Generator<T> gen) {
		for (int i = 0; i < a.length; i++) {
			a[i] = gen.next();
		}
		return a;
	}

}
