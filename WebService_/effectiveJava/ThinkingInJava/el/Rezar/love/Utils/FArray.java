package el.Rezar.love.Utils;

import el.Rezar.love.泛型.GenericsParaminterface.Generator;

public class FArray {

	/**
	 * 自动填充一个T类型的数组
	 * 
	 * @param a
	 * @param gen
	 * @return
	 */
	public static <T> T[] fill(T[] a, Generator<T> gen) {

		for (int i = 0; i < a.length; i++) {
			a[i] = gen.next();
		}

		return a;

	}

}
