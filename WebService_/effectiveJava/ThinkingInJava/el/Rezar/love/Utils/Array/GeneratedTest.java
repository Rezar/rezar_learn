package el.Rezar.love.Utils.Array;

import java.util.Arrays;

import org.junit.Test;

import el.Rezar.love.Utils.CountingGenerator;
import el.Rezar.love.泛型.GenericsParaminterface.Generator;

public class GeneratedTest {

	@Test
	public void testArrayTArrayGeneratorOfT() {
		Integer[] arr1 = new Integer[20];
		// 这里必须是通过外部类来实例化内部类
		Generator<Integer> gen = new CountingGenerator.Integer();
		arr1 = Generated.array(arr1, gen);
		System.out.println(Arrays.toString(arr1));
	}

	@Test
	public void testArrayClassOfTGeneratorOfTInt() {

		Class<String> clazz = String.class;
		String[] arrStr = Generated.array(clazz,
				new CountingGenerator.String(), 20);
		System.out.println(Arrays.toString(arrStr));

	}

}
