package el.Rezar.love.Utils.Array;

import el.Rezar.love.泛型.GenericsParaminterface.Generator;

/**
 * 根据某个数组和该数组数据类型的生成器来填充这个数组<br/>
 * According to a array and that array data type of the generator to fill the
 * array
 * 
 * @author Administrator
 * 
 */
public class Generated {

	public static <T> T[] array(T[] a, Generator<T> gen) {

		return new CollectionData<T>(gen, a.length).toArray(a);

	}

	@SuppressWarnings("unchecked")
	public static <T> T[] array(Class<T> type, Generator<T> gen, int size) {
		T[] a = (T[]) java.lang.reflect.Array.newInstance(type, size);
		return array(a, gen);
	}

}
