package el.Rezar.love.Utils.Tuple;

public class ThreeTuple<S1, S2, S3> extends TwoTuple<S1, S2> {

	public final S3 thrid;

	public ThreeTuple(S1 first, S2 second, S3 thrid) {
		super(first, second);
		this.thrid = thrid;
	}

}
