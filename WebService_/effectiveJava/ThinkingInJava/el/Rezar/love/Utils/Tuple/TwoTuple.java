package el.Rezar.love.Utils.Tuple;

public class TwoTuple<S1, S2> {
	
	public final S1 first;
	public final S2 second;
	
	public TwoTuple(S1 first , S2 second){
		this.first = first;
		this.second = second;
	}

}
