package el.Rezar.love.����.GenericBound;

import java.awt.Color;

public class HoldItem<T> {

	T item;

	HoldItem(T item) {
		this.item = item;
	}

}

class Colored2<T extends HasColor> extends HoldItem<T> {

	Colored2(T item) {
		super(item);
	}

	public Color getColor() {
		return item.getColor();
	}

}

class ColorDimension2<T extends Dimension & HasColor> extends Colored2<T> {

	ColorDimension2(T item) {
		super(item);
	}

	int getX() {
		return item.x;
	}

	int getY() {
		return item.y;
	}

	int getZ() {
		return item.z;
	}

}

class Solid2<T extends Dimension & HasColor & Weight> extends
		ColorDimension2<T> {

	Solid2(T item) {
		super(item);
	}

	int weight() {
		return item.wight();
	}

}
