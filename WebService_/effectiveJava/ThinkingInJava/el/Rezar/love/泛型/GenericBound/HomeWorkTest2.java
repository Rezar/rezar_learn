package el.Rezar.love.泛型.GenericBound;

/**
 * 测试通过泛型限制边界来调用某个方法
 * 
 * @author Administrator
 * 
 */
public class HomeWorkTest2 {

	public static <T extends HasColor> void testCanGetColor(T obj) {
		obj.getColor();
	}

	public static <T extends Weight> void testCanGetWeight(T obj) {
		obj.wight();
	}

	public static void main(String[] args) {
		HomeWordTest hwt = new HomeWordTest();
		testCanGetColor(hwt);
		testCanGetWeight(hwt);
	}

}
