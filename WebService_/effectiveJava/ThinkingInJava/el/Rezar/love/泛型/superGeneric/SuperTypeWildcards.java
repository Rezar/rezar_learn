package el.Rezar.love.泛型.superGeneric;

import java.util.ArrayList;
import java.util.List;

/**
 * <? super Apple>:<br/>
 * 表示当前泛型的类型是Apple或者Apple的子类，定义了泛型参数类型的上界
 * 
 * @author Administrator
 * 
 */
public class SuperTypeWildcards {

	public static void writeTo(List<? super Apple> apples) {
		apples.add(new Apple());
		apples.add(new Jonathan());
	}

	public static void main(String[] args) {
		List<? super Apple> apples = new ArrayList<Apple>();
		apples = new ArrayList<Fruit>();
		writeTo(apples);
	}

}
