package el.Rezar.love.����;

import java.lang.reflect.Array;

public class GenericArrayWithTypeToken<T> {

	private T[] items;

	@SuppressWarnings("unchecked")
	public GenericArrayWithTypeToken(Class<T> clazz , int size) {
		items = (T[]) Array.newInstance(clazz, size);
	}
	
	public void put(T item , int index){
		this.items[index] = item;
	}
	
	public T get(int index){
		return this.items[index];
	}
	
	public T[] rep(){
		return this.items;
	}

}
