package el.Rezar.love.����.GenericsParaminterface;

import java.util.Iterator;
import java.util.Random;

public class CoffeeGenerator implements Generator<Coffee>, Iterable<Coffee> {

	private Class<?>[] types = { Latte.class, Mocha.class, Cappuccino.class };

	private static Random r = new Random(47);

	public CoffeeGenerator() {
	}

	private int size;

	public CoffeeGenerator(int size) {
		this.size = size;
	}
	

	@Override
	public Coffee next() {
		try {
			return (Coffee) types[r.nextInt(types.length)].newInstance();
		}  catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	class CoffeeIterator implements Iterator<Coffee>{
		int count = CoffeeGenerator.this.size;

		@Override
		public boolean hasNext() {
			return count > 0;
		}

		@Override
		public Coffee next() {
			count --;
			return CoffeeGenerator.this.next();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}

	@Override
	public Iterator<Coffee> iterator() {
		return new CoffeeIterator();
	}

}
