package el.Rezar.love.����.GenericsParaminterface;

import java.util.Iterator;

public class FibonacciIterable extends Fibonacci implements Iterable<Integer> {

	private int n;

	public FibonacciIterable(int n) {
		this.n = n;
	}

	@Override
	public Iterator<Integer> iterator() {
		return new Iterator<Integer>() {
			int count = n;

			@Override
			public boolean hasNext() {
				return count > 0;
			}

			@Override
			public Integer next() {
				count --;
				return FibonacciIterable.this.next();
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

}
