package el.Rezar.love.泛型.GenericsParaminterface;

import org.junit.Test;

/**
 * 该类负责生成Fibonacci数列
 * 
 * @author Administrator
 * 
 */
public class Fibonacci implements Generator<Integer> {

	private int count = 0;

	@Override
	public Integer next() {
		return fib(count++);
	}

	private Integer fib(int i) {
		if (i < 2) {
			return 1;
		}
		return fib(i - 2) + fib(i - 1);
	}

	@Test
	public void mainTest() {
		Fibonacci gen = new Fibonacci();
		for (int i = 0; i < 18; i++) {
			System.out.print(gen.next() + " ");
		}
	}

}
