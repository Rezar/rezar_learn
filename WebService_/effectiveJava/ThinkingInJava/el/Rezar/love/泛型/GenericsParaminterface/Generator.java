package el.Rezar.love.泛型.GenericsParaminterface;

/**
 * 对象生成器的抽象接口
 * 
 * @author Administrator
 * 
 * @param <T>
 */
public interface Generator<T> {

	T next();

}
