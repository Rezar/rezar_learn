package el.Rezar.love.泛型;

/**
 * 使用泛型实现的一种类似于栈的结构
 * 
 * @author Administrator
 * 
 * @param <T>
 */
public class LinkedStack<T> {

	private static class Node<U> {
		U item;
		Node<U> next;

		Node() {
			item = null;
			next = null;
		}

		Node(U item, Node<U> next) {
			this.item = item;
			this.next = next;
		}

		boolean end() {
			return item == null && next == null;
		}
	}

	private Node<T> top = new Node<T>();

	/**
	 * 入栈
	 * 
	 * @param item
	 */
	public void push(T item) {
		top = new Node<T>(item, top);
	}

	/**
	 * 出栈
	 * 
	 * @return
	 */
	public T pop() {
		T result = top.item;
		if (!top.end()) {
			top = top.next;
		}
		return result;
	}

}
