package el.Rezar.love.泛型;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 一个持有特定类型对象的列表，每次调用其上的select()方法时，它可以随机返回一个容器中的元素
 * 
 * @author Administrator
 * 
 * @param <T>
 */
public class RandomList<T> {

	private List<T> storage = new ArrayList<T>();
	private Random rand = new Random(47);

	public void add(T item) {
		this.storage.add(item);
	}

	public T select() {
		return this.storage.get(rand.nextInt(this.storage.size()));
	}

}
