package el.Rezar.love.����.reflection;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class SuperGenerics<T> {

	private Class<T> modelClazz;
	private String modelName;

	@SuppressWarnings("unchecked")
	public SuperGenerics() {
		ParameterizedType parameterizedType = (ParameterizedType) this
				.getClass().getGenericSuperclass();

		Type[] genericTypes = parameterizedType.getActualTypeArguments();
		this.modelClazz = (Class<T>) genericTypes[0];
		this.modelName = this.modelClazz.getSimpleName();
		System.out.println(modelName);
	}

}
