package el.Rezar.love.CharptOne;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

public class BufferToText {

	public static final int BSIZE = 1024;

	public static void main(String[] str) throws IOException {
		FileOutputStream out = new FileOutputStream("data2.txt");
		FileChannel fc = out.getChannel();
		fc.write(ByteBuffer.wrap("我们的爱".getBytes()));
		out.close();
		fc.close();

		FileInputStream in = new FileInputStream("data2.txt");
		fc = in.getChannel();
		ByteBuffer buffer = ByteBuffer.allocate(BSIZE);
		fc.read(buffer);
		buffer.flip();
		// doesn't work
		System.out.println(buffer.asCharBuffer());
		// decode using this system's default charset
		buffer.rewind();
		String encoding = System.getProperty("file.encoding");
		System.out.println("Decoded using " + encoding + ": "
				+ Charset.forName(encoding).decode(buffer));
		in.close();
		// or, we could encode with something that will print
		out = new FileOutputStream("data2.txt");
		fc = out.getChannel();
		fc.write(ByteBuffer.wrap("我们的爱2".getBytes("utf-16BE")));
		fc.close();
		in = new FileInputStream("data2.txt");
		fc = in.getChannel();
		buffer.clear();
		fc.read(buffer);
		buffer.flip();
		System.out.println(buffer.asCharBuffer());
		in.close();
	}

}
