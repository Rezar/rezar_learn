package el.Rezar.love.����;

import org.junit.Test;

public class GenericArrayWithTypeTokenTest {

	@Test
	public void testRep() {
		GenericArrayWithTypeToken<String> gawt = new GenericArrayWithTypeToken<String>(
				String.class, 20);
		for (int i = 0; i < 20; i++) {
			String item = "Test" + i;
			gawt.put(item, i);
		}

		String[] rep = gawt.rep();
		for (String str : rep) {
			System.out.println(str);
		}

	}

}
