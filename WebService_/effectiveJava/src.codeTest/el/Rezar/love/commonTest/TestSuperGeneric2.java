package el.Rezar.love.commonTest;

import org.junit.Test;

import el.Rezar.love.泛型.reflection.TestSuperGeneric;

public class TestSuperGeneric2 {

	/**
	 * 如果该测试方法是放在一个对象中的，在调用这个测试方法的时候，会先进行当前对象的初始化
	 */
	@Test
	public void test() {
		new TestSuperGeneric();
	}

}
