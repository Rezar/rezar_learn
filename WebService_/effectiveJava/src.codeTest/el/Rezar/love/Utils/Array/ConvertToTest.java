package el.Rezar.love.Utils.Array;

import java.lang.reflect.Array;
import java.util.Arrays;

import org.junit.Test;

import el.Rezar.love.Utils.FArray;
import el.Rezar.love.Utils.RandomGenerator;

public class ConvertToTest {

	@Test
	public void testPrimitive() {
		Boolean[] bools = new Boolean[4];
		FArray.fill(bools, new RandomGenerator.Boolean());
		boolean[] bools_ = ConvertTo.primitive(bools);
		System.out.println(Arrays.toString(bools_));
	}

	@Test
	public void testPrimitive_1() {
		Integer[] ints = new Integer[4];
		FArray.fill(ints, new RandomGenerator.Integer());
		System.out.println(Arrays.toString(ints));
		int[] ints_ = ConvertTo.primitive(ints);
		System.out.println(Arrays.toString(ints_));
	}

	@Test
	public void testArray() {
		Object obj = Array.newInstance(Integer.class, 20);
		// [[Ljava.lang.Integer;@99353f
		System.out.println(obj);
		obj = Array.newInstance(int.class, 20);
		// [I@14c194d
		System.out.println(obj);
	}

	@Test
	public void testPrimitive_2() {
		int size = 10;
		int[] retValue = ConvertTo.primitive(Generated.array(Integer.class,
				new RandomGenerator.Integer(), size));
		System.out.println(Arrays.toString(retValue));
	}

}
