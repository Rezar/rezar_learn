package cn.wx.domain;

import java.util.Date;

/**
 * 此处为求职者关注的企业动态或职位动态信息提醒
 * 
 * @author Administrator
 * 
 */
public class AttentionRemindMessage {

	private String id;
	private String title;
	private String content;
	private Date date;
	/**
	 * 查看状态 <br/>
	 * 0:not read <br/>
	 * 1:read already
	 * 
	 * */
	private int state;
	private User jobSeeker;

	public User getJobSeeker() {
		return jobSeeker;
	}

	public void setJobSeeker(User jobSeeker) {
		this.jobSeeker = jobSeeker;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

}
