package cn.wx.domain;

import java.io.File;
import java.util.Date;

/**
 * 此处为求职者接收到企业发送的邀约消息
 * 
 * @author Administrator
 * 
 */
public class JobInvalitionMessage {

	private String id;
	private String theme;
	private Date date;
	private User sendUser;
	private User receiveUser;
	private String content;
	private File[] attachment;
	/**
	 * 查看状态 <br/>
	 * 0:not read <br/>
	 * 1:read already
	 * 
	 * */
	private int state;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getSendUser() {
		return sendUser;
	}

	public void setSendUser(User sendUser) {
		this.sendUser = sendUser;
	}

	public User getReceiveUser() {
		return receiveUser;
	}

	public void setReceiveUser(User receiveUser) {
		this.receiveUser = receiveUser;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public File[] getAttachment() {
		return attachment;
	}

	public void setAttachment(File[] attachment) {
		this.attachment = attachment;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

}
