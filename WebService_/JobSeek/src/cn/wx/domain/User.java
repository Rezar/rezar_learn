package cn.wx.domain;

/**
 * the super class of JobSeeker , Recruiter
 * 
 * @author Administrator
 * 
 */
public class User {

	private String id;
	/*
	 * if is jobSeeker , name is mean it's name,and if user is a company , name
	 * is mean the company's name
	 */
	private String name;
	private String phone;
	private String date;
	/**
	 * JobSeeker's state<br/>
	 * 0:not validation<br/>
	 * 1:validation OK <br/>
	 */
	private int state;
	/*
	 * if user is a jobSeeker , user_type is Q,and if user identify is a company ,
	 * user_type is Z
	 */
	private String user_type;

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
