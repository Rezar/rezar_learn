package cn.wx.com.ServiceInterfaces;

/**
 * 求职者的手机验证码服务的具体实现
 * 
 * @author Administrator
 * 
 */
public class JobSeekerPhoneValidationCodeServiceImpl implements
		IPhoneValidationCodeService {

	@Override
	public String getPhoneValidationCode() {
		return "这是求职者的手机验证码";
	}

}
