package cn.wx.com.ServiceInterfaces;

/**
 * 生成手机验证码的服务的提供者接口
 * 
 * @author Administrator
 * 
 */
public interface PhoneValidationCodeServiceProviderInterface {

	public IPhoneValidationCodeService getService();

}
