package cn.wx.com.ServiceInterfaces.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import cn.wx.config.CommonFieldValue;
import cn.wx.utils.CollectionUtils;

/**
 * 用户注册ID生成的工具类
 * 
 * @author Administrator
 * 
 */
public class CodeHelpUtil {
	private static Object qObject = new Object();
	private static Object zObject = new Object();

	static {
		Thread refreshTaskThread = new Thread(new RefreshTask());
		refreshTaskThread.setDaemon(true);
		refreshTaskThread.start();
	}

	private static SimpleDateFormat sd = new SimpleDateFormat(
			CommonFieldValue.DateType);

	/**
	 * 用于保存一定范围内的数值
	 */
	private static Map<String, Integer> numbers = CollectionUtils.newInstance();

	/**
	 * 返回当前年的年月日字符串，如： 2014-12-08
	 * 
	 * @return
	 */
	private static String generateCurrentDateString() {
		Date date = new Date();
		return sd.format(date);
	}

	/**
	 * 生成一个范围中的数字字符
	 * 
	 * @return
	 */
	private static String generateNumberForQ(String dateStr) {
		String str = "";
		synchronized (qObject) {
			Integer nums = numbers.get(dateStr);
			if (nums == null) {
				nums = Integer.valueOf(0);
				numbers.put(dateStr, nums);
			}
			Integer num = numbers.get(dateStr);
			num = num + 1;
			if (num > 9999) {
				throw new RuntimeException(
						"can not register more with id is bigger than 9999");
			}
			numbers.put(dateStr, num);
			str = fillNum(num);
		}
		return dateStr + str;
	}

	/**
	 * 生成一个范围中的数字字符
	 * 
	 * @return
	 */
	private static String generateNumberForZ(String dateStr) {
		String str = "";
		synchronized (zObject) {
			Integer nums = numbers.get(dateStr);
			if (nums == null) {
				nums = Integer.valueOf(0);
				numbers.put(dateStr, nums);
			}
			Integer num = numbers.get(dateStr);
			num = num + 1;
			if (num > 9999) {
				throw new RuntimeException(
						"can not register more with id is bigger than 9999");
			}
			numbers.put(dateStr, num);
			str = fillNum(num);
		}
		return dateStr + str;
	}

	public static String generatePhoneValidationCode(String prefix) {
		if (prefix.startsWith("Q"))
			return generateNumberForQ(prefix + generateCurrentDateString());
		else {
			return generateNumberForZ(prefix + generateCurrentDateString());
		}
	}

	private static String fillNum(Integer num) {
		StringBuilder sb = new StringBuilder();
		int length = num.toString().length();
		for (int i = length; i < 4; i++) {
			sb.append("0");
		}
		sb.append(num);
		return sb.toString();
	}

	private static class RefreshTask implements Runnable {

		@Override
		public void run() {
			try {
				/**
				 * 获取从当前时间开始到下一天的开始所经历的时间
				 */
				long sleepTime = getSleepTime();
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
			numbers.clear();
		}

		/**
		 * 获取当前时间到达下一天开始所需要经历的时间,因为每一天的ID不同，所以在一天过去之后，可以<br/>
		 * 将之前一天的ID数据全部清除
		 * 
		 * @return
		 */
		private long getSleepTime() {
			Date date = new Date();
			Calendar cd = Calendar.getInstance();
			cd.setTime(date);
			int hour = cd.get(Calendar.HOUR_OF_DAY);
			int minute = cd.get(Calendar.MINUTE);
			int second = cd.get(Calendar.SECOND);
			System.out.println("hour is : " + hour + ",minute is : " + minute
					+ ",second is : " + second);
			long time = ((23 - hour) * 3600 + (59 - minute) * 60 + (59 - second)) * 1000;
			return time;
		}

	}

}
