package cn.wx.com.ServiceInterfaces.util;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;

public class CodeHelpUtilTest {

	@Test
	public void testGeneratePhoneValidationCode() {
		for (int i = 0; i < 100; i++) {
			String code = CodeHelpUtil.generatePhoneValidationCode("Q");
			System.out.println(code);
		}

	}

	@Test
	public void test2() {
		Date date = new Date();
		Calendar cd = Calendar.getInstance();
		cd.setTime(date);
		int hour = cd.get(Calendar.HOUR_OF_DAY);
		int minute = cd.get(Calendar.MINUTE);
		int second = cd.get(Calendar.SECOND);
		System.out.println("hour is : " + hour + ",minute is : " + minute
				+ ",second is : " + second);
		long time = ((23 - hour) * 3600 + (59 - minute) * 60 + (59 - second)) * 1000;
		System.out.println(time);
	}

	public static void main(String[] args) {
		Runnable[] tasks = new Runnable[4];
		for (int i = 0; i < tasks.length; i++) {
			if (i % 2 == 0)
				tasks[i] = new register("Q");
			else
				tasks[i] = new register("Z");
		}

		ExecutorService executor = Executors.newCachedThreadPool();
		for (Runnable task : tasks) {
			executor.execute(task);
		}
		executor.shutdown();
	}
}

class register implements Runnable {

	private String prefix;

	public register(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public void run() {
		for (int i = 0; i < 1000; i++) {
			String code = CodeHelpUtil.generatePhoneValidationCode(this.prefix);
			System.out.println(code);
		}
	}

}
