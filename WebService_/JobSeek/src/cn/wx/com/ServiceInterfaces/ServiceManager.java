package cn.wx.com.ServiceInterfaces;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 服务提供者注册类
 * 
 * @author Administrator
 * 
 */
public class ServiceManager {

	private static final String DEFAULT_PROVIDER_NAME = "<def>";

	private ServiceManager() {
	}

	/* Maps service names to services */
	private static final Map<String, PhoneValidationCodeServiceProviderInterface> providers = new ConcurrentHashMap<String, PhoneValidationCodeServiceProviderInterface>();

	/**
	 * register a service Provider with name and subwayproviderinterface
	 * 
	 * @param name
	 * @param p
	 */
	public static void registerProvider(String name, PhoneValidationCodeServiceProviderInterface p) {
		providers.put(name, p);
	}

	/**
	 * register a default provider
	 * 
	 * @param p
	 */
	public static void registerDefaultProvider(PhoneValidationCodeServiceProviderInterface p) {
		providers.put(DEFAULT_PROVIDER_NAME, p);
	}

	/**
	 * access the service with default service'name
	 * 
	 * @return
	 */
	public static IPhoneValidationCodeService newInstance() {
		return newInstance(DEFAULT_PROVIDER_NAME);
	}

	/**
	 * according to a service's name , return the mapping service
	 */
	public static IPhoneValidationCodeService newInstance(String name) {
		PhoneValidationCodeServiceProviderInterface p = providers.get(name);
		if (p == null) {
			throw new IllegalArgumentException(name
					+ " 'services was not register !");
		}
		return p.getService();
	}

}
