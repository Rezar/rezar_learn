package cn.wx.com.ServiceInterfaces;

/**
 * 求职者注册的时候获取到手机验证码的服务接口
 * 
 * @author Administrator
 * 
 */
public interface IPhoneValidationCodeService {

	/**
	 * 生成用户注册的时候用来校验的手机验证码，特定的用户根据不同的实现类来完成
	 * 
	 */
	public String getPhoneValidationCode();

}
