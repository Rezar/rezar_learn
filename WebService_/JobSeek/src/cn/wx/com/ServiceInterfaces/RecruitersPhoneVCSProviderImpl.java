package cn.wx.com.ServiceInterfaces;

import cn.wx.config.CommonFieldValue;

/**
 * 招聘者手机验证码的生成服务的具体实现
 * 
 * @author Administrator
 * 
 */
public class RecruitersPhoneVCSProviderImpl implements
		PhoneValidationCodeServiceProviderInterface {

	static {
		ServiceManager.registerProvider(CommonFieldValue.RECRUILTERS,
				new RecruitersPhoneVCSProviderImpl());
	}

	@Override
	public IPhoneValidationCodeService getService() {
		return new RecruitersPhoneValidationCodeServiceImpl();
	}

}
