package cn.wx.Services;

import java.util.List;

import cn.wx.domain.ApplyForFeedBackMessage;

public interface IApplyForFeedBackMessageService {

	/**
	 * save a applyForFeedBackMessage
	 * 
	 * @param message
	 * @return
	 */
	public boolean saveAFFBM(ApplyForFeedBackMessage message);

	/**
	 * 根据主键ID查找出一个相对应的Message
	 * 
	 * @param mId
	 * @return
	 */
	public ApplyForFeedBackMessage findAFFBM(String mId);

	/**
	 * 查找出所有的message
	 * 
	 * @return
	 */
	public List<ApplyForFeedBackMessage> findAllFFBM();

}
