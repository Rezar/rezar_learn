package cn.wx.Services;

import cn.wx.domain.User;

/**
 * user's services
 * 
 * @author Administrator
 * 
 */
public interface IUserService {

	/**
	 * 
	 * @param clientPVC
	 *            :jobSeeker's register form's phone validation code
	 * @param sessionPVC
	 *            :session's register phone validation code
	 * @return whether register validation is ok
	 */
	public boolean registerValidation(String clientPVC, String sessionPVC);

	/**
	 * register a jobSeeker
	 */
	public boolean registerJobSeeker(User jobSeeker);

	/**
	 * find a user by userId
	 * @param userId
	 * @return
	 */
	public User findUser(String userId);

}
