package cn.wx.config;

/**
 * 系统中用到了一些属性的常量
 * 
 * @author Administrator
 * 
 */
public class CommonFieldValue {

	/* 求职者 */
	public static final String JOBSEEKER = "JobSeeker";
	/* 招聘者 */
	public static final String RECRUILTERS = "Recruiters";
	public static final String DateType = "yyyyMMdd";

}
