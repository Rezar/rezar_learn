package cn.wenhao.com.mybatis.model;

import java.util.List;
import java.util.Map;

public interface IUser {

	public User selectUserByID(int id);

	public List<User> selectUsers(String userName);

	public void addUser(User user);

	public void deleteUser(int id);

	public List<Article> getUserArticles(int id);

	public List<User> getUsers(List<Integer> ids);

	public List<User> getUsers2(int[] ids);

	public List<Article> getArticles(Map<String, Object> params);

}
