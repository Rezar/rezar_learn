package el.Rezar.love.IOC.handler;

import java.io.File;
import java.util.Map;

import org.junit.Test;

public class XmlHandlerTest {

	@Test
	public void testConvert() {
		String resource = File.separator + "bean.xml";
		Map<String, Object> initMap = new XmlHandler().convert(resource);
		for (Map.Entry<String, Object> entry : initMap.entrySet()) {
			System.out.println("Bean Name is : " + entry.getKey() + ";--"
					+ entry.getValue());
		}
	}

}
