package el.Rezar.love.test;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import el.Rezar.love.IOC.bean.ZhangSan;
import el.Rezar.love.test.classTest1.Dao;
import el.Rezar.love.test.classTest1.UserDaoImpl;

public class DeclaredTest {

	@Test
	public void test() {
		List<String> list = new ArrayList<String>();
		Class<?> clazz = list.getClass().getDeclaringClass();
		System.out.println(clazz.getName());
		System.out.println(list.getClass().getName());

	}

	@Test
	public void test2() {
		ZhangSan zs = new ZhangSan();
		Class<?> clazz = zs.getClass();
		Class<?>[] clazzs = clazz.getClasses();
		for (Class<?> clazz_ : clazzs) {
			System.out.println(clazz_.getName());
		}
	}

	@Test
	public void test3() {

		Dao<String> udi = new UserDaoImpl();
		Type genType = udi.getClass().getGenericSuperclass();
		System.out.println(genType);
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		Class<?> type = (Class<?>) params[0];
		System.out.println(type.getName());

	}

}
