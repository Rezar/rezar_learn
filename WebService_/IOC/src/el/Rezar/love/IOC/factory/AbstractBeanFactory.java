package el.Rezar.love.IOC.factory;

import el.Rezar.love.IOC.date.IDataContext;
import el.Rezar.love.IOC.entity.BeanEntity;
import el.Rezar.love.IOC.handler.HandlerDecorator;

/**
 * 使用模板设计模式
 * 
 * @author Administrator
 * 
 */
public abstract class AbstractBeanFactory implements IBeanFactory {

	protected HandlerDecorator handlerDecorator;
	protected IDataContext beanDefinitionContext;
	protected IDataContext beanCacheContext;

	/* 定义服务定义信息的存储服务对象 */
	public abstract void setBeanDefinitionContext(String contextClass,
			String resource);

	/* 定义缓存数据存储服务对象的类 */
	public abstract void setBeanCacheContext(String contextClass);

	/* 定义元数据转换器 （元数据到服务描述信息） */
	public abstract void setHandler(String defaultHandler, String handlerName);

	// 模板方法
	// 获取服务提供者实例
	@Override
	public Object getBean(String name) {

		if (this.containsBeanCache(name)) {
			return this.beanCacheContext.get(name);
		}

		BeanEntity be = this.getBeanEntity(name);
//		if ("SINGLETON".equals(be.getScope())) {
//			Object bean = be.getValue();
//			if (bean == null) {
//				bean = this.createBean(be);
//				be.setValue(bean);
//			}
//			return bean;
//		}

		Object bean = this.createBean(be);
//		if (!"SINGLETON".equals(be.getScope())) {
//			this.regiterBeanCache(name, bean);
//		}
		this.regiterBeanCache(name, bean);

		return bean;
	}

	@Override
	public void registerBeanDefinition(String resource, String cacheContext,
			String definitionContext, String defaultHandler,
			String customHandler) {
		this.setHandler(defaultHandler, customHandler);
		this.setBeanCacheContext(cacheContext);
		this.setBeanDefinitionContext(definitionContext, resource);
	}

	/* 获取某个Bean的描述信息 */
	public abstract BeanEntity getBeanEntity(String name);

	/* 判断缓存中是否包含某个bean的实例 */
	public abstract boolean containsBeanCache(String name);

	/* 从缓存中获取某个Bean的实例 */
	public abstract Object getBeanCache(String name);

	/* 创建某个Bean的实例 */
	public abstract Object createBean(BeanEntity beanEntity);

	/* 向缓存中注册某个Bean的实例 */
	public abstract void regiterBeanCache(String name, Object beanF);

	/* 获取所有的服务描述信息 */
	public abstract IDataContext getBeanDefinitionContext();

	/* 获取所有的服务实例缓存信息 */
	public abstract IDataContext getBeanCacheContext();

}
