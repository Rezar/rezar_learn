package el.Rezar.love.IOC.factory;

import el.Rezar.love.IOC.entity.BeanEntity;
import el.Rezar.love.IOC.entity.FieldEntity;
import el.Rezar.love.IOC.utils.BBeanUtil;
import el.Rezar.love.IOC.utils.ReflectionUtil;

public class MyBeanFactory extends DefaultBeanFactory {

	/**
	 * 使用自定义的创建Bean的方法
	 */
	@Override
	public Object createBean(BeanEntity beanEntity) {

		// 根据一个BeanEntity来构建一个Bean对象
		// 根据Bean的Type获取到该Bean的一个实例对象
		Object obj = ReflectionUtil.getInstance(beanEntity.getType());
		// 对该Bean对象进行属性的填充
		try {
			if (beanEntity.getFieldEntityList() != null) {
				for (FieldEntity fe : beanEntity.getFieldEntityList()) {
					String ref = fe.getRef();
					String fieldName = fe.getName();
					String value = (String) fe.getValue();
					Object fieldValue = null;
					if (value != null && !value.equals("")) {
						fieldValue = BBeanUtil.change(value,
								BBeanUtil.getFieldType(obj, fieldName));
					} else {
						fieldValue = this.getBean(ref);
					}
					BBeanUtil.setFieldValue(fieldName, obj, fieldValue);
				}
			}
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return super.createBean(beanEntity);
	}

}
