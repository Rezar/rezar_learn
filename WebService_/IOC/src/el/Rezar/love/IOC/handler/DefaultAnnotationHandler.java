package el.Rezar.love.IOC.handler;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import el.Rezar.love.IOC.annotation.Bean;
import el.Rezar.love.IOC.annotation.Param;
import el.Rezar.love.IOC.annotation.Service;
import el.Rezar.love.IOC.entity.BeanEntity;
import el.Rezar.love.IOC.entity.ParamEntity;
import el.Rezar.love.IOC.entity.ServiceEntity;

/**
 * 对注解信息进行解析生成元数据定义信息
 * 
 * @author Administrator
 * 
 */
public class DefaultAnnotationHandler extends HandlerDecorator {

	@Override
	public Map<String, Object> convert(String resource) {

		Map<String, Object> map = super.convert(resource);

		Map<String, Object> beanMap = new HashMap<String, Object>();

		String packageName = resource.replace(".", "/");
		System.out.println(packageName);
		Enumeration<URL> dirs = null;

		try {
			dirs = Thread.currentThread().getContextClassLoader()
					.getResources(packageName);
			while (dirs.hasMoreElements()) {
				URL url = dirs.nextElement();
				System.out.println("url File is : " + url.getFile());
				File file_ = new File(url.getFile());
				File[] files = file_.listFiles();
				for (File file : files) {
					System.out.println(file.getName());
					String name = file.getName();
					String className = name.substring(0, name.length() - 6);
					className = resource + "." + className;
					Object object = Class.forName(className).newInstance();
					Class<?> beanClass = object.getClass();
					boolean flag = beanClass.isAnnotationPresent(Bean.class);
					if (flag) {
						Bean bean = beanClass.getAnnotation(Bean.class);
						BeanEntity be = new BeanEntity();

						Method[] methods = beanClass.getDeclaredMethods();
						List<ServiceEntity> serviceEntityList = new ArrayList<ServiceEntity>();
						for (Method method : methods) {
							if (!method.isAnnotationPresent(Service.class)) {
								continue;
							}
							Service service = method
									.getAnnotation(Service.class);
							String methodName = method.getName();
							String serviceName = service.name();
							Class<?>[] params = method.getParameterTypes();
							Annotation[][] annts = method
									.getParameterAnnotations();
							List<ParamEntity> paramEntitys = new ArrayList<ParamEntity>();
							for (int i = 0; i < annts.length; i++) {
								for (int j = 0; j < annts[i].length; j++) {
									Annotation param = (Param) annts[i][j];
									if (param instanceof Param) {
										String paramName = ((Param) param)
												.name();
										paramEntitys
												.add(new ParamEntity(paramName,
														params[j].getName()));
									}
								}
							}
							serviceEntityList.add(new ServiceEntity(
									serviceName, methodName, paramEntitys));
						}
						be.setName(bean.name());
						be.setServiceEntityList(serviceEntityList);
						be.setType(className);
						beanMap.put(be.getName(), be);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (map != null && map.size() > 0) {
			beanMap.putAll(map);
		}

		return beanMap;
	}
}
