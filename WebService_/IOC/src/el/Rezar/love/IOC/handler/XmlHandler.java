package el.Rezar.love.IOC.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Element;

import el.Rezar.love.IOC.entity.BeanEntity;
import el.Rezar.love.IOC.entity.FieldEntity;
import el.Rezar.love.IOC.entity.ParamEntity;
import el.Rezar.love.IOC.entity.ServiceEntity;
import el.Rezar.love.IOC.utils.BBeanUtil;
import el.Rezar.love.IOC.utils.FilePathOridentedUtil;
import el.Rezar.love.IOC.utils.XmlManager;

/**
 * @Date : 2014/6/23
 * @Description :元数据处理器装饰对象的具体实现（处理XML）
 */
public class XmlHandler extends HandlerDecorator {

	@Override
	public Map<String, Object> convert(String resource) {

		// 这是一个XML的handler
		// 具体实现
		// TODO:XML元数据处理器
		Map<String, Object> map = super.convert(resource);

		Map<String, Object> beanMap = new HashMap<String, Object>();

		// 解析XML文件中的Bean节点
		String filePath = FilePathOridentedUtil.getFilePath(resource);
		System.out.println(filePath);

		try {
			Element root = XmlManager.parse(filePath).getDocumentElement();
			// 解析出每一个bean节点的数据
			List<Element> beans = XmlManager.getChildElements(root, "bean");
			for (Element ele : beans) {
				BeanEntity be = new BeanEntity();
				Map<String, String> attrValues = XmlManager
						.getElementAttributes(ele);
				BBeanUtil.popluteFields(be, attrValues);
				// 获取该Bean的所有属性节点
				List<Element> props = XmlManager.getChildElements(ele,
						"property");
				if (props != null && props.size() != 0) {
					Set<FieldEntity> fieldEntitys = solveProperty(props);
					be.setFieldEntityList(fieldEntitys);
				}
				// 获取该Bean的所有服务的节点
				List<Element> services = XmlManager.getChildElements(ele,
						"service");
				if (services != null && services.size() != 0) {
					// 获取到该服务的所有的参数的节点
					List<ServiceEntity> ServiceEntitys = solveServices(services);
					be.setServiceEntityList(ServiceEntitys);
				}
				beanMap.put(be.getName(), be);
			}
			root = null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		if (map != null && map.size() != 0) {
			beanMap.putAll(map);
		}

		return beanMap;
	}

	/**
	 * 处理该bean对应的所有的Services
	 * 
	 * @param services
	 * @return
	 */
	private List<ServiceEntity> solveServices(List<Element> services) {

		List<ServiceEntity> ret = new ArrayList<ServiceEntity>();
		for (Element ele : services) {
			Map<String, String> values = XmlManager.getElementAttributes(ele);
			ServiceEntity se = new ServiceEntity();
			BBeanUtil.popluteFields(se, values);
			List<Element> paramElements = XmlManager.getChildElements(ele,
					"param");
			List<ParamEntity> paramEntitys = solveParamEntity(paramElements);
			se.setParamEntityList(paramEntitys);
			ret.add(se);
		}
		return ret;
	}

	/**
	 * 对Parameter节点进行处理
	 * 
	 * @param paramElements
	 * 
	 * @return
	 */
	private List<ParamEntity> solveParamEntity(List<Element> paramElements) {
		List<ParamEntity> ret = new ArrayList<ParamEntity>();
		for (Element ele : paramElements) {
			Map<String, String> values = XmlManager.getElementAttributes(ele);
			ParamEntity pe = new ParamEntity();
			BBeanUtil.popluteFields(pe, values);
			ret.add(pe);
		}
		return null;
	}

	/**
	 * 对Property节点进行处理
	 * 
	 * @param props
	 * @return
	 */
	private Set<FieldEntity> solveProperty(List<Element> props) {
		Set<FieldEntity> ret = new HashSet<FieldEntity>();
		for (Element ele : props) {
			Map<String, String> values = XmlManager.getElementAttributes(ele);
			FieldEntity fe = new FieldEntity();
			BBeanUtil.popluteFields(fe, values);
			ret.add(fe);
		}
		return ret;
	}
}
