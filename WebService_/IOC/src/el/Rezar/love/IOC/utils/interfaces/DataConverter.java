package el.Rezar.love.IOC.utils.interfaces;

public interface DataConverter<T> {

	public T convert(Object from);

}
