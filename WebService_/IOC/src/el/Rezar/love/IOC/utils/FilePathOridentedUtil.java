package el.Rezar.love.IOC.utils;

import java.io.File;
import java.net.URISyntaxException;

/**
 * The tools just used to retrieve the file directory file path
 * 
 * @author Rezar
 * 
 */
public class FilePathOridentedUtil {

	/**
	 * get file's path
	 * 
	 * @return
	 */
	public static String getFilePath(String fileNameOrPath) {

		String webPropertyReaderPath = "";
		// boolean isStartWith = false;
		try {
			if (fileNameOrPath.startsWith(File.separatorChar + "")) {
				// isStartWith = true;
				webPropertyReaderPath = FilePathOridentedUtil.class
						.getResource("/").toURI().getPath();
			} else {
				webPropertyReaderPath = FilePathOridentedUtil.class
						.getResource("").toURI().getPath();
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
		String filepath = null; /*
								 * webPropertyReaderPath + (isStartWith ? "" :
								 * File.separator) + fileNameOrPath;
								 */
		File fileParent = new File(webPropertyReaderPath, fileNameOrPath);
		if (fileParent.exists()) {
			filepath = fileParent.getAbsolutePath();
		} else {
			filepath = null;
		}
		return filepath;

	}
}
