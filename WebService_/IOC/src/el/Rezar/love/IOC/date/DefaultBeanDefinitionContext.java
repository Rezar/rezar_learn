package el.Rezar.love.IOC.date;

import java.util.Map;

/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/6/22
 * @Description :默认的服务描述信息存储区
 */
public class DefaultBeanDefinitionContext implements IDataContext{

    private  Map<String,Object> beanDefinitionMap=null;

    public DefaultBeanDefinitionContext(){}

    @Override
    public void initData(Map<String, Object> map) {
        beanDefinitionMap=map;
    }

    @Override
    public void set(String name, Object obj) {
        beanDefinitionMap.put(name,obj);
    }

    @Override
    public Object get(String name) {
        return beanDefinitionMap.get(name);
    }

    @Override
    public Map<String, Object> getAll() {
        return beanDefinitionMap;
    }
}
