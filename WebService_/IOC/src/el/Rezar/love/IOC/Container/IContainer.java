package el.Rezar.love.IOC.Container;

import el.Rezar.love.IOC.date.IDataContext;
import el.Rezar.love.IOC.entity.BeanEntity;



/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/6/22
 * @Description :容器的核心接口
 */

public interface IContainer{

    //获取容器的某个服务提供者实例
    public Object getBean(String name);

    //根据服务提供者名称，服务名称，参数来获取容器提供的服务
    //由三者确定一个唯一的服务。
    public Object getService(String beanName,String serviceName,Object... args);

    //获取容器所有服务描述信息
    public IDataContext getBeanDefinitionContext();

    //获取容器中某个服务提供者的描述信息
    public BeanEntity getBeanDefinition(String name);

    //获取容器中所有服务提供者实例的缓存
    public IDataContext getBeanCacheContext();

    //热加载新的服务提供者
    public void initContainerService(String resource);

}
