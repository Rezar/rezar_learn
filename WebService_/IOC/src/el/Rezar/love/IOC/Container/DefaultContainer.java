package el.Rezar.love.IOC.Container;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import el.Rezar.love.IOC.date.IDataContext;
import el.Rezar.love.IOC.entity.BeanEntity;
import el.Rezar.love.IOC.entity.ServiceEntity;
import el.Rezar.love.IOC.factory.AbstractBeanFactory;
import el.Rezar.love.IOC.utils.ReflectionUtil;

public class DefaultContainer implements IContainer {
	/* 在该Container中包含一个Factory对象，用于对象的创建和管理 */
	private AbstractBeanFactory factory;

	public DefaultContainer(Map<String, Object> map) {
		System.out.println("----------开始初始化容器---------");
		// 这个默认工厂的读取应该来自配置文件
		// 如果无自定义信息，自动加载默认的工厂实现
		System.out.println("----------开始初始化容器内工厂---------");
		factory = (AbstractBeanFactory) ReflectionUtil
				.getInstance(map.get("factory") == null ? "el.Rezar.love.IOC.factory.DefaultBeanFactory"
						: map.get("factory").toString());
		factory.registerBeanDefinition(
				map.get("resource") == null ? "com.tgb.bean" : map.get(
						"resource").toString(),
				map.get("cacheContext") == null ? "el.Rezar.love.IOC.date.DefaultBeanCacheContext"
						: map.get("cacheContext").toString(),
				map.get("definitionContext") == null ? "el.Rezar.love.IOC.date.DefaultBeanDefinitionContext"
						: map.get("definitionContext").toString(),
				map.get("defaultHandler") == null ? "el.Rezar.love.IOC.handler.DefaultAnnotationHandler"
						: map.get("defaultHandler").toString(),
				map.get("customHandler") == null ? null : map.get(
						"customHandler").toString());
		System.out.println("----------初始化容器内工厂成功！---------");
		System.out.println("----------初始化容器成功！---------");
	}

	@Override
	public Object getBean(String name) {
		return this.factory.getBean(name);
	}

	@Override
	public Object getService(String beanName, String serviceName,
			Object... args) {
		BeanEntity be = this.factory.getBeanEntity(beanName);
		List<ServiceEntity> services = be.getServiceEntityList();
		String methodName = null;
		for (ServiceEntity se : services) {
			if (serviceName.equals(se.getName())) {
				methodName = se.getValue();
			}
		}
		Object bean = this.factory.getBean(beanName);
		Method method = ReflectionUtil.getMethod(bean, methodName);
		try {
			return method.invoke(bean, args);
		} catch (IllegalArgumentException e) {

			e.printStackTrace();
		} catch (IllegalAccessException e) {

			e.printStackTrace();
		} catch (InvocationTargetException e) {

			e.printStackTrace();
		}
		return null;
	}

	@Override
	public IDataContext getBeanDefinitionContext() {
		return this.factory.getBeanDefinitionContext();
	}

	@Override
	public BeanEntity getBeanDefinition(String name) {
		return this.factory.getBeanEntity(name);
	}

	@Override
	public IDataContext getBeanCacheContext() {
		return this.factory.getBeanCacheContext();
	}

	/**
	 * 实现热加载
	 */
	@Override
	public void initContainerService(String resource) {

	}

}
