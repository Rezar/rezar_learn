package el.Rezar.love.IOC.bean;

import java.util.Date;

import el.Rezar.love.IOC.annotation.Bean;
import el.Rezar.love.IOC.annotation.Param;
import el.Rezar.love.IOC.annotation.Service;

public class Rezar {

	private String name;
	private int age;
	@Bean(name = "person")
	private Person person;

	private Date date;

	@Service(name = "test")
	public void test(@Param(name = "name") String name, int age) {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Rezar [name=" + name + ", age=" + age + ", person=" + person
				+ ", date=" + date + "]";
	}

}
