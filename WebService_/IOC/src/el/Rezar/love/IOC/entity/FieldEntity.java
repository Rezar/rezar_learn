package el.Rezar.love.IOC.entity;

public class FieldEntity {

	/* 该属性的名称 */
	private String name;
	/* 该属性引用的对象的名称 */
	private String ref;
	/* 该属性对应的属性值 */
	private Object value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "FieldEntity [name=" + name + ", ref=" + ref + ", value="
				+ value + "]";
	}
}
