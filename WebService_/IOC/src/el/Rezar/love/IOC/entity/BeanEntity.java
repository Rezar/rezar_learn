package el.Rezar.love.IOC.entity;

import java.util.List;
import java.util.Set;

/**
 * @Description :服务提供者描述信息载体，其数据结构如下：
 * 
 *              BeanEntity
 * 
 *              name type List
 * @bean注解名 Bean类型 ServicEntity的arrayList ServiceEntity……
 * 
 *          name value List
 * @Service注解名 方法名 ParamEntity的ArrayList ParamEntity…… name value
 * @param注解名 参数类型
 */

public class BeanEntity {
	private String name; // 服务提供者名
	private String type; // 服务提供者实例的类型
	private Object value; // 服务提供者实例
	private String scope; // 服务提供者作用的范围
	private Set<FieldEntity> fieldEntityList; // 服务提供者的属性
	private List<ServiceEntity> serviceEntityList; // 服务提供者提供的所有服务

	public List<ServiceEntity> getServiceEntityList() {
		return serviceEntityList;
	}

	public void setServiceEntityList(List<ServiceEntity> serviceEntityList) {
		this.serviceEntityList = serviceEntityList;
	}

	public BeanEntity() {
	}

	public BeanEntity(String name, String type) {
		this.name = name;
		this.type = type;
	}

	public BeanEntity(String name, String type,
			List<ServiceEntity> serviceEntityList) {
		this.name = name;
		this.type = type;
		this.serviceEntityList = serviceEntityList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Set<FieldEntity> getFieldEntityList() {
		return fieldEntityList;
	}

	public void setFieldEntityList(Set<FieldEntity> fieldEntityList) {
		this.fieldEntityList = fieldEntityList;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	@Override
	public String toString() {
		return "BeanEntity [name=" + name + ", type=" + type + ", value="
				+ value + ", scope=" + scope + ", fieldEntityList="
				+ fieldEntityList + ", serviceEntityList=" + serviceEntityList
				+ "]";
	}
}
