package el.Rezar.love.IOC;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import el.Rezar.love.IOC.Container.DefaultContainer;
import el.Rezar.love.IOC.Container.IContainer;
import el.Rezar.love.IOC.bean.Person;
import el.Rezar.love.IOC.date.IDataContext;
import el.Rezar.love.IOC.entity.BeanEntity;
import el.Rezar.love.IOC.handler.DefaultAnnotationHandler;
import el.Rezar.love.IOC.utils.ReflectionUtil;

/**
 * @Author : JiaLin
 * @Email : shan9liang@163.com
 * @Date : 2014/6/22
 * @Description :容器的单元测试类
 */
public class TestContainer {

	private IContainer container;

	@Before
	public void init() {
		Map<String, Object> map = new HashMap<String, Object>();
		// 可以自定义初始化参数，如果不自定义，则使用默认实现。
		map.put("factory", "el.Rezar.love.IOC.factory.MyBeanFactory");
		map.put("resource", File.separator + "bean.xml");
		// map.put("cacheContext", "com.tgb.data.DefaultBeanCacheContext");
		// map.put("definitionContext",
		// "el.Rezar.love.IOC.date.DefaultBeanDefinitionContext");
		map.put("defaultHandler", "el.Rezar.love.IOC.handler.XmlHandler");
		// map.put("customHandler", "com.tgb.handler.XmlHandler");

		container = new DefaultContainer(map);
	}

	@Test
	public void testGetBean() {
		// 通过Filter拦截到要调用的类，获取该类的注解，然后通过容器获得该类的实例。
		Person person = (Person) container.getBean("person");
		Object result = ReflectionUtil.invoke(person,
				ReflectionUtil.getMethods(person)[0], "China");
		System.out.println(result);
	}

	@Test
	public void testGetBeanDefinition() {
		BeanEntity beanEntity = container.getBeanDefinition("person");
		assertNotNull(beanEntity);
		assertEquals("person", beanEntity.getName());
	}

	@Test
	// 主测试方法
	public void testService() {
		Object result = container.getService("person", "ToSomeWhere", "China");
		assertEquals("Drive to China", result);
	}

	@Test
	// 主测试方法
	public void testService1() {
		Object result = container
				.getService("zhangsan", "ToSomeWhere", "China");
		assertEquals("Drive to China", result);

	}

	@Test
	public void testGetObjectsInPackage() {
		Map<String, Object> map = new DefaultAnnotationHandler()
				.convert("com.tgb.bean");
		assertTrue(map.size() == 2);
		assertNotNull(map.get("person"));
		assertNotNull(map.get("car"));
	}

	@Test
	public void testGetBeanDefinitionContext() {
		IDataContext beanDefinitionContext = container
				.getBeanDefinitionContext();
		Map<String, Object> map = beanDefinitionContext.getAll();
		assertTrue(map.size() == 2);
		assertNotNull(map.get("person"));
		assertNotNull(map.get("car"));
	}

	@Test
	public void testGetBeanCacheContext() {
		Object result = container.getService("person", "ToSomeWhere", "China");
		// IDataContext beanCacheContext = container.getBeanCacheContext();
		// Map<String, Object> map = beanCacheContext.getAll();
		// assertTrue(map.size() == 2);
		// assertNotNull(map.get("person"));
		// assertNotNull(map.get("car"));
		System.out.println(result);
	}

}
