package el.Rezar.love.serviceProviderFrameWork;

/**
 * 服务提供者实现类
 * 
 * @author Administrator
 * 
 */
public class SubwayProviderImpl implements SubwayProviderInterface {

	static {
		ServiceManager.registerProvider("OneCard", new SubwayProviderImpl());
	}

	@Override
	public SubWayInterface getService() {
		return new SubWayImpl();
	}

}
