package el.Rezar.love.serviceProviderFrameWork;

/**
 * 一卡通地铁进出服务实现
 * 
 * @author Administrator
 * 
 */
public class SubWayImpl implements SubWayInterface {

	@Override
	public boolean in() {
		System.out.println("通过一卡通进入地铁!");
		/**
		 * 对一卡通进行相关的处理,判断是否能够进行地铁站
		 */
		return false;
	}

	@Override
	public boolean out() {
		System.out.println("通过一卡通离开地铁!");
		/**
		 * 对一卡通进行相关的处理，判断是否放行
		 */
		return false;
	}

}
