package el.rezar.love.ProxyPattern.��������;

public class PersonBeanImpl implements PersonBean {

	private String name;
	private String gender;
	private String interests;
	private int hotOrNotRating;
	private int ratingCount = 0;

	public PersonBeanImpl() {
		super();
	}

	public PersonBeanImpl(String name, String gender, String interests,
			int hotOrNotRating) {
		this.name = name;
		this.gender = gender;
		this.interests = interests;
		this.hotOrNotRating = hotOrNotRating;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public int getHotOrNotRating() {
		if (this.ratingCount == 0) {
			return 0;
		}
		return this.hotOrNotRating / this.ratingCount;
	}

	public void setHotOrNotRating(int hotOrNotRating) {
		this.hotOrNotRating += hotOrNotRating;
	}

	@Override
	public String toString() {
		return "PersonBeanImpl [name=" + name + ", gender=" + gender
				+ ", interests=" + interests + ", hotOrNotRating="
				+ hotOrNotRating + "]";
	}
}
