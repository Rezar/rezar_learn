package cn.wenhao.testPrint;

public class PrintOther {

	/**
	 * 打印图形的步骤：<br/>
	 * A:首先确定图形的样式，分析图形由什么构成 <br/>
	 * B:要明确控制台输出的情形是，当前没有换行的话，光标是之前最后打印字符的位置，所以：<br/>
	 * 在控制台上的某点处打印一个字符，需要在之前打印出N个字符之后再输出改字符
	 */

	/**
	 * @param spaceCount
	 *            ：每次需要打印的空格的个数
	 * @param spaceNum
	 *            ：在输出字符之前需要答应多少个空格
	 */
	public static void printSpace(int spaceCount, int spaceNum, String ch) {
		String temp = getSpaceStr(spaceCount, ch);
		for (int i = 0; i < spaceNum; i++) {
			System.out.print(temp);
		}
	}

	/**
	 * 相对于之前打印的空格后再进行输出字符
	 * 
	 * @param currentLine
	 * @return
	 */
	public static int[] countEachLineIndex(int currentLine, int totalLine,
			int num) {
		int[] eachLineIndex = new int[num];
		eachLineIndex[0] = 0;
		for (int i = 1; i < num; i++) {
			eachLineIndex[i] = (currentLine * 2);
		}
		return eachLineIndex;
	}

	private static String getSpaceStr(int spaceCount, String ch) {
		String temp = "";
		for (int i = 0; i < spaceCount; i++) {
			temp += ch;
		}
		return temp;
	}

	public static void printCh(int index, String ch) {
		for (int i = 0; i <= index; i++) {
			System.out.print(ch);
		}
	}

	public static void main(String[] args) {
		int totalLine = 10;
		String ch = "*";
		for (int i = 0; i < totalLine; i++) {
			printSpace(1, totalLine - i, ch);
			int[] index = countEachLineIndex(i, totalLine, 2);
			
		}
	}

}
