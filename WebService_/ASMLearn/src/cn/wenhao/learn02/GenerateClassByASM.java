package cn.wenhao.learn02;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

public class GenerateClassByASM {

	public void generateClass() {

		ClassWriter cw = new ClassWriter(0);

		cw.visit(Opcodes.V1_6, Opcodes.ACC_PUBLIC, "cn/wenhao/learn02/MyClass",
				null, Type.getInternalName(Object.class), null);
		cw.visitSource("MyClass.java", null);

		cw.visitField(Opcodes.ACC_PRIVATE, "name",
				Type.getDescriptor(String.class), null, "womendeai");

		cw.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null)
				.visitCode();// 定义构造方法

		String getMethodDesc = "()" + Type.getDescriptor(String.class);
		cw.visitMethod(Opcodes.ACC_PUBLIC, "getName", getMethodDesc, null, null)
				.visitCode();

		String setMethodDesc = "(" + Type.getDescriptor(String.class) + ")V";
		cw.visitMethod(Opcodes.ACC_PUBLIC, "setName", setMethodDesc, null, null)
				.visitCode();

		cw.visitEnd();

	}

	class MyClassLoader extends ClassLoader {
		public Class<?> defineClassFromClassFile(String className,
				byte[] classFile) throws ClassFormatError {
			return defineClass(className, classFile, 0, classFile.length);
		}
	}

	public static void main(String[] args) {
		new GenerateClassByASM().generateClass();
	}

}
