package cn.wenhao.learn01;

import org.objectweb.asm.Type;

public class InternalNameTransform {

	public static void main(String[] args) {
		System.out.println(Type.getInternalName(String.class));
		System.out.println(Type.getInternalName(Integer.class));
		System.out.println(Type.getInternalName(InternalNameTransform.class));
	}

}
