package cn.wenhao.springIdol;

public class Sonnet29 implements Poem {

	private final static String[] poem = { "只可惜时光之里山南水北", "只可惜你我之间人来往去" };

	public Sonnet29() {
	}

	@Override
	public void recite() {
		for (String str : poem) {
			System.out.println(str);
		}
	}

}
