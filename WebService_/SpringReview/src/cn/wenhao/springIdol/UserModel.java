package cn.wenhao.springIdol;

import org.springframework.beans.factory.annotation.Autowired;

public class UserModel {

	@Autowired
	private Address address;

	public UserModel() {

	}

	public UserModel(Address address) {
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "UserModel [address=" + address + "]";
	}

}
