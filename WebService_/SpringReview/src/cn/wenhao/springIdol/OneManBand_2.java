package cn.wenhao.springIdol;

import java.util.Map;

public class OneManBand_2 implements Performer {

	private Map<String, Instrument> instruments;

	private Map<Integer, Double> showMoney;

	private String nullValue = "1234";

	public void setNullValue(String nullValue) {
		this.nullValue = nullValue;
	}

	public void setInstruments(Map<String, Instrument> instruments) {
		this.instruments = instruments;
	}

	public void setShowMoney(Map<Integer, Double> showMoney) {
		this.showMoney = showMoney;
	}

	@Override
	public void preform() {
		for (String key : this.instruments.keySet()) {
			System.out.print(key + " : ");
			Instrument is = this.instruments.get(key);
			is.play();
		}

		for (Integer id : this.showMoney.keySet()) {
			System.out.println(id + " is need : " + this.showMoney.get(id));
		}

		System.out.println((this.nullValue == null) + "===" + this.nullValue);

	}

}
