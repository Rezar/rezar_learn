package cn.wenhao.springIdol;

public class PoeticJuggler extends Juggler {

	private Poem poem;

	public PoeticJuggler(Poem poem) {
		super();
		this.poem = poem;
	}

	public PoeticJuggler(int beanBags, Poem poem) {
		super(beanBags);
		this.poem = poem;
	}

	@Override
	public void preform() {
		super.preform();
		System.out.println("while reciting ... ");
		this.poem.recite();
	}

}
