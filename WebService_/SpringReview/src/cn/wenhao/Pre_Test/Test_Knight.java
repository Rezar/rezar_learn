package cn.wenhao.Pre_Test;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test_Knight {

	@Test
	public void test_knight() throws Exception {
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		Knight knight = (Knight) ctx.getBean("knight");
		knight.embarkOnQuest();
	}

}
