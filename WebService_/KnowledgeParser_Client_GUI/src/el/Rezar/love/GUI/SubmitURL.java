package el.Rezar.love.GUI;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

import javax.swing.JOptionPane;

/**
 * 向服务器发送资源所在的URL
 * 
 * @author Administrator
 * 
 */
public class SubmitURL {

	private static final String PROPERTIES_FILE_NAME = "Configuration.properties";
	private static String serviceUrl;
	private static String encoding;

	static {
		File file = new File(".");
		File propFile = new File(file, PROPERTIES_FILE_NAME);
		System.out.println(propFile.getAbsolutePath());

		Properties prop = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(propFile);
			prop.load(fis);
			serviceUrl = prop.getProperty("serviceUrl");
			encoding = prop.getProperty("encoding");
			prop = null;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"not find Configuration.properties in current file path:"
							+ file.getAbsolutePath());
			throw new Error(e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void submitURL(String urlStr) {
		System.out.println(serviceUrl);
		boolean isOk = check(urlStr);
		if (isOk) {
			try {
				URL url = new URL(serviceUrl);
				// 打开连接
				HttpURLConnection uc = (HttpURLConnection) url.openConnection();
				uc.setRequestMethod("POST");
				uc.setDoOutput(true);

				OutputStream out = uc.getOutputStream();
				OutputStream buffered = new BufferedOutputStream(out);
				OutputStreamWriter outWriter = new OutputStreamWriter(buffered,
						encoding);
				outWriter.write("url=" + urlStr + "");
				outWriter.flush();
				outWriter.close();
				out.close();
				uc.getResponseCode(); // 响应代码 200表示成功

			} catch (Exception e) {
				throw new RuntimeException("url : " + serviceUrl
						+ " is not validate !");
			}
		}
	}

	/**
	 * 这个正则表达式不正确，所以直接返回了true
	 * 
	 * @param urlStr
	 * @return
	 */
	private static boolean check(String urlStr) {
		// String regex =
		// "^(http://|https://)?((?:[A-Za-z0-9]+-[A-Za-z0-9]+|[A-Za-z0-9]+)\\.)+([A-Za-z]+)[/\\?\\:]?.*$";
		// boolean isOk = Pattern.matches(regex, urlStr);
		// System.out.println(isOk);
		return true;
	}
	// public static void main(String[] args) {
	// submitURL("http://blog.sina.com.cn/s/blog_6f505d710102uzwu.html");
	// }

}
