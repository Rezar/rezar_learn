package el.Rezar.love.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * 进行URL资源输入的GUI页面
 * 
 * @author Administrator
 * 
 */
public class URLInput {
	private JFrame frame;

	public URLInput() {
		frame = new JFrame("URL INPUT");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600, 85);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setLayout(new BorderLayout());
		frame.setResizable(false);

		JLabel jl = new JLabel("Resource URL");
		frame.add(BorderLayout.WEST, jl);

		JTextField jf1 = new JTextField(350);
		jf1.setForeground(new Color(108, 2, 10));
		Font f = new Font("Consolas",Font.BOLD,12);
		jf1.setFont(f);
		frame.add(BorderLayout.CENTER, jf1);

		JButton jb1 = new JButton("提交");
		ActionListener al = new SubmitActionListener(jf1);
		jb1.addActionListener(al);
		frame.add(BorderLayout.SOUTH, jb1);

	}

	public static void main(String[] args) {
		new URLInput();
	}

}
