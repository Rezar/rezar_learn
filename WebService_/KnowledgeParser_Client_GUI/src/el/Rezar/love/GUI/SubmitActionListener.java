package el.Rezar.love.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

public class SubmitActionListener implements ActionListener {

	private JTextField jf1;

	public SubmitActionListener(JTextField jf1) {
		this.jf1 = jf1;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		String url = this.jf1.getText().trim();
		this.jf1.setText("");
		SubmitURL.submitURL(url);
	}

}
