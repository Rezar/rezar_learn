package el.Rezar.love.GUI;

import java.util.regex.Pattern;

import org.junit.Test;

public class SubmitURLTest {

	@Test
	public void testSubmitURL() {
		CharSequence urlStr = "http://localhost/xyzefg/index.jsp";
		String regex = "^(http|https)://([\\w-]+\\.)+[\\w-]+(/[\\w-./?%&=]*)?$";
		boolean isOk = Pattern.matches(regex, urlStr);
		System.out.println(isOk);
	}

}
