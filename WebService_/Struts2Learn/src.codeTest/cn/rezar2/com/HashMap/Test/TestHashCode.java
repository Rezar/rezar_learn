package cn.rezar2.com.HashMap.Test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import cn.Rezar.myXWork.myReferenceMap.MyReferenceMap;

import com.opensymphony.xwork2.inject.util.ReferenceMap;
import com.opensymphony.xwork2.inject.util.ReferenceType;

public class TestHashCode {

	@Test
	public void test1() throws InterruptedException {
		String str1 = new String("abc");
		String str2 = new String("abc");
		int hashCode1 = System.identityHashCode(str1);
		int hashCode2 = System.identityHashCode(str2);
		System.out.println(hashCode1 + ":" + hashCode2);
		System.out.println(hashCode1 == hashCode2);

		MyReferenceMap<String, double[]> mrm = new MyReferenceMap<String, double[]>(
				cn.Rezar.myXWork.myReferenceMap.ReferenceType.WEAK,
				cn.Rezar.myXWork.myReferenceMap.ReferenceType.WEAK);

		ReferenceMap<String, double[]> rm = new ReferenceMap<String, double[]>(
				ReferenceType.WEAK, ReferenceType.WEAK);
		double[] ds = new double[10000000 * 2 + 10000];
		rm.put(str1, ds);
		double[] ret = rm.get(str1);

		mrm.put(str1, ds);

		System.out.println(ret);
		System.gc();
		System.gc();
		System.gc();
		System.gc();
		System.gc();
		System.gc();
		System.gc();
		Thread.sleep(8000);
	}

	@Test
	public void test2() {
		// ReferenceMap<String,String> rm = new
		// ReferenceMap<String,String>(ReferenceType.SOFT,
		// ReferenceType.SOFT);

		String str1 = new String("abc");
		String str2 = new String("abc");
		// rm.put(str1, "123");
		Map<String, String> map = new HashMap<String, String>();
		map.put(str1, "abc");
		String ret = map.get(str2);
		System.out.println(ret);

	}

}
