package cn.wenhao.action;

import cn.wenhao.Utils.ObjectCheck.ObjectUtils;
import cn.wenhao.domain.User;

import com.opensymphony.xwork2.ActionSupport;

public class RegisterAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6673193930693593260L;

	private User user = new User();

	public String register() {
		user.setId(12);
		user.setUserName("Rezar");
		user.setUserAddress("beijngshangdi");
		try {
			ObjectUtils.isValidate(user);
		} catch (Exception e) {
			return "failure";
		}
		System.out.println("Is OK !");
		return "registerSuccess";
	}

	@Override
	public String execute() throws Exception {
		return this.register();
	}

}
