package cn.wenhao.Utils.interfaces;

import java.lang.reflect.Field;

import cn.wenhao.Utils.annotation.validate.AValidate;

public interface ValidateForAnnotation {

	public <T> boolean validate(Field field, T obj,AValidate avalidate);

}
