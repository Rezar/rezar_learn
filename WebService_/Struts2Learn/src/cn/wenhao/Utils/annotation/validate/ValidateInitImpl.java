package cn.wenhao.Utils.annotation.validate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import cn.wenhao.Utils.interfaces.ValidateForAnnotation;
import cn.wenhao.Utils.propertiesUtils.PropertyReader;

public class ValidateInitImpl implements ValidateInit {

	@Override
	public void generateValidate(ArrayList<AValidate> validates) {
		// 获取指定路径下的Properties文件
		Properties prop = PropertyReader
				.getProperties("/config/validate.properties");
		// 获取src路径下的Properties文件，如果有的话
		Properties prop_src = PropertyReader
				.getProperties("/validate.properties");
		Collection<Object> validates_ = null;
		if (prop != null) {
			validates_ = prop.values();
		}
		if (prop != null && prop_src != null) {
			Collection<Object> validates2 = prop_src.values();
			validates_.addAll(validates2);
		}
		for (Object validate : validates_) {
			String classpath = (String) validate;
			Class<ValidateForAnnotation> validateClass = null;
			try {
				validateClass = (Class<ValidateForAnnotation>) Class
						.forName(classpath);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			try {
				ValidateForAnnotation vfa = validateClass.newInstance();
				AValidate ava = new AValidate(vfa);
				validates.add(ava);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

}
