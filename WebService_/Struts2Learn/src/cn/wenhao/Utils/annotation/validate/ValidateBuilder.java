package cn.wenhao.Utils.annotation.validate;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class ValidateBuilder {

	private final ArrayList<AValidate> validates;
	private ValidateInit vi;

	private static final class SingletHodler {
		private static final ValidateBuilder vb = new ValidateBuilder();
	};

	public static ValidateBuilder getInstanceof() {
		return SingletHodler.vb;
	}

	private ValidateBuilder() {
		validates = new ArrayList<AValidate>();
		this.vi = new ValidateInitImpl();
		this.vi.generateValidate(this.validates);

		for (int i = 0; i <= this.validates.size() - 1; i++) {
			if ((i + 1) < this.validates.size())
				this.validates.get(i).setAvalidate(this.validates.get(i + 1));
		}
	}

	public <T> void beginValidate(Field field, T obj) {
		this.validates.get(0).doValidate(field, obj);
	}

}
