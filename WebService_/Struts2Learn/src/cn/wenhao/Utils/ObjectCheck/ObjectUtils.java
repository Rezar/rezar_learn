package cn.wenhao.Utils.ObjectCheck;

import java.lang.reflect.Field;

import cn.wenhao.Utils.annotation.validate.ValidateBuilder;
import cn.wenhao.Utils.interfaces.IDomainValidate;

public class ObjectUtils {

	/**
	 * 通过注解判断某个对象中的某些属性是否为空或者(长度为零，字符串的情况下)
	 * 
	 * @param obj
	 * @return
	 */
	public static final <T> boolean isValidate(T obj) {
		boolean isVaildate = true;
		if (obj instanceof IDomainValidate) {
			return ((IDomainValidate) obj).validate();
		}

		Class<?> clazz = obj.getClass();
		ValidateBuilder vb = ValidateBuilder.getInstanceof();
		try {
			Field[] fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				try {
					vb.beginValidate(field, obj);
				} catch (Exception e) {
					isVaildate = false;
					throw e;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("can not validate the obj,due to : " + e);
		}

		return isVaildate;
	}

}
