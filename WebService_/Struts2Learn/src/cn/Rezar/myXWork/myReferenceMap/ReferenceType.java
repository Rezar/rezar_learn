package cn.Rezar.myXWork.myReferenceMap;

/**
 * Reference type. Used to specify what type of reference to keep to a referent.
 * 
 * @author Administrator
 * 
 */
public enum ReferenceType {

	/**
	 * Prevents reference from being reclaimed by the garbage collector
	 */
	STRONG,

	/**
	 * Referent reclaimed in an LRU fashion when the VM runs low on memory and
	 * no strong references exist
	 */
	SOFT,

	/**
	 * Referent reclaimed when no strong or soft reference exist
	 */
	WEAK,

	/**
	 * Similar to weak references except the garbage collector doesn't actually
	 * reclaim the referent. More flexible alternative to finalization.
	 * 
	 * @see java.lang.ref.PhantomReference
	 */
	PHANTOM;

}
