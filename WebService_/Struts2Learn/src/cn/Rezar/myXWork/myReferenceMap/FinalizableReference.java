package cn.Rezar.myXWork.myReferenceMap;

public interface FinalizableReference {

	/**
	 * Invoked on a background thread after the referent has been garbage
	 * collected.
	 */
	void finalizeReferent();

}
