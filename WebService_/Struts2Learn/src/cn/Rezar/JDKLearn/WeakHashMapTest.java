package cn.Rezar.JDKLearn;

import java.util.WeakHashMap;

public class WeakHashMapTest {

	/**
	 * 显示的结果表示，垃圾回收器只会回收那些仅仅持有弱引用的Key，id能被3整除的key持有强引用，所以不会被回收
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		final int size = 1000;

		Key[] keys = new Key[size];
		WeakHashMap<Key, Value> whm = new WeakHashMap<Key, Value>();
		for (int i = 0; i < size; i++) {
			Key key = new Key(Integer.toString(i));
			Value value = new Value(Integer.toString(i));
			if (i % 3 == 0) {
				keys[i] = key;
			}
			whm.put(key, value);
		}

		// 催促GC进行垃圾回收
		System.gc();
		Thread.sleep(8000);

	}

}

class Key {
	String id;

	public Key(String id) {
		this.id = id;
	}

	public String toString() {
		return this.id;
	}

	public int hashCode() {
		return this.id.hashCode();
	}

	public boolean equals(Object o) {
		return (o instanceof Key) && (((Key) o).id.equals(id));
	}

	public void finalize() {
		System.out.println("Finalize key'id : " + this.id);
	}
}

class Value {
	String id;

	public Value(String id) {
		this.id = id;
	}

	public String toString() {
		return this.id;
	}

	public void finalize() {
		System.out.println("Finalize value'id : " + this.id);
	}
}
