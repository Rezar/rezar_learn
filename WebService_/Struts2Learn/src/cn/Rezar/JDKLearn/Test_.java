package cn.Rezar.JDKLearn;

import java.lang.ref.SoftReference;

public class Test_ {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("��ʼ");

		A a = new A();

		SoftReference<A> sr = new SoftReference<A>(a);
		a = null;
		Thread.sleep(10000);
		if (sr != null) {
			a = sr.get();
			System.out.println(a);
		} else {
			a = new A();
			sr = new SoftReference<A>(a);
		}

		System.out.println("����");
	}

}

class A {
	int[] a;

	public A() {
		a = new int[(int) (10000000 * 0.9)];
	}

	@Override
	public String toString() {
		return "A []";
	}

	@Override
	protected void finalize() throws Throwable {
		System.out.println("a is finalize !");
	}

}
