package cn.wenhao.MyClassLoader;

import com.sun.org.apache.bcel.internal.util.ClassLoader;

/**
 * 热部署一个Class对象的基本实现逻辑为：<br/>
 * A:因为JVM判断一个字节码对象是否已经被加载进内存的判断依据有：<br/>
 * a:该类的权限定名是否一致<br/>
 * b:加载该字节码对象的类加载器是否一样<br/>
 * 所以此时进行热部署的方案是，创建多个不同的自定义的类加载器，在发现字节码对象对应的.class文件发生改变的时候，<br/>
 * 利用新的自定义类加载器加载该类。<br/>
 * 此时的问题是：使用不同的ClassLoader实例加载同一个类，会不会导致JVM的PermGen区无限增大？<br/>
 * 得益于JVM的垃圾回收机制，当一个对象不再被其它的对象进行引用的时候，该对象会被垃圾回收机制回收，<br/>
 * 此时，在声明了一个ClassLoader变量后，通过在热部署的时候赋予它不同的自定义类加载器，来加载新的Class对象
 * 
 * @author Administrator
 * 
 */
public class ReloadClassLoader extends ClassLoader {

	
	
}
