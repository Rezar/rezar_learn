package cn.wenhao.MyClassLoader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.sun.org.apache.bcel.internal.util.ClassLoader;

@SuppressWarnings("restriction")
public class PathClassLoader extends ClassLoader {

	private String packageName;

	private String classPath;

	public PathClassLoader(String classPath, String packageName) {
		this.classPath = classPath;
		this.packageName = packageName;
	}
	
	

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		if (name.startsWith(this.packageName)) {
			byte[] classDate = getDate(name);
			if (classDate == null) {
				throw new ClassNotFoundException(name + " not found in "
						+ this.classPath);
			} else {
				return super.defineClass(classDate, 0, classDate.length);
			}
		} else {
			return super.loadClass(name);
		}
	}

	private byte[] getDate(String className) {
		String path = this.classPath + File.separator
				+ className.replaceAll(".", File.separator) + ".class";
		System.out.println(path);
		InputStream in = null;
		try {
			in = new FileInputStream(new File(path));
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buffer = new byte[2048];
			int len = 0;
			while ((len = in.read(buffer)) != -1) {
				baos.write(buffer, 0, len);
			}
			return baos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return null;
	}
}
