package cn.wenhao.MyClassLoader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import com.sun.org.apache.bcel.internal.util.ClassLoader;

public class NetClassLoader extends ClassLoader {

	private String classPath;
	private String packageName = "";

	public NetClassLoader(String classPath, String packageName) {
		this.classPath = classPath;
		this.packageName = packageName;
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {

		Class<?> aClass = findLoadedClass(name);
		if (aClass != null) {
			return aClass;
		}

		if (name.startsWith(packageName)) {
			byte[] classData = getData(name);
			if (classData == null) {
				throw new ClassNotFoundException("can not found : " + name);
			} else {
				return super.defineClass(classData, 0, classData.length);
			}
		} else {
			return super.findClass(name);
		}
	}

	private byte[] getData(String name) {

		String path = this.classPath + File.separatorChar
				+ name.replace('.', File.separatorChar) + ".class";

		InputStream in = null;
		try {
			URL url = new URL(path);
			in = url.openStream();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			int len = 0;
			byte[] buffer = new byte[2048];
			while ((len = in.read(buffer)) != -1) {
				baos.write(buffer, 0, len);
			}
			return this.decode(baos.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public byte[] decode(byte[] src) {
		byte[] decode = null;
		// solve the byte[] source
		return decode;
	}

}
