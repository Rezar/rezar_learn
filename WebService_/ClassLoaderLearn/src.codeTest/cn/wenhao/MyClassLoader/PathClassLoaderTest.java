package cn.wenhao.MyClassLoader;

import org.junit.Test;

public class PathClassLoaderTest {

	@Test
	public void test() throws ClassNotFoundException {
		PathClassLoader loader = new PathClassLoader("c:\\classLoader",
				"cn.wenhao.MyClassLoader");
		String name = "cn.wenhao.MyClassLoader.PathClassLoader";
		Class<?> retClass = loader.loadClass(name);
		System.out.println(retClass.getName());
	}

}
