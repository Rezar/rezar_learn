在使用该工具模板时，要注意的有：
	首先，修改WebWoek的包名为自己的项目名，如果不进行修改，则可以使用默认的WebWork包名
	若修改了，注意，在
	一：	-----config/applicationContext.xml中，对
		
	《	<bean id="sessionFactory"
		class="org.springframework.orm.hibernate3.LocalSessionFactoryBean">
		<property name="dataSource" ref="dataSource" />

		<!-- 指定hibernate自身的属性 -->
		<property name="hibernateProperties">
			<props>
				<prop key="hibernate.dialect">${hibernate.dialect}</prop>
				<prop key="hibernate.show_sql">${hibernate.show_sql}</prop>
				<prop key="hibernate.hbm2ddl.auto">${hibernate.hbm2ddl.auto}</prop>
				<prop key="hibernate.jdbc.batch_size">${hibernate.jdbc.batch_size}</prop>
				<prop key="hibernate.format_sql">${hibernate.format_sql}</prop>
			</props>
		</property>
		<property name="mappingDirectoryLocations">
			<list>
				<value>classpath:cn/wenhao/？？？？？？/example/model</value>
			</list>
		</property>

	</bean>
	
	》
	的五个问号的地方同样进行修改，修改为自己定义的包名
	
	
	二： ------config/applicationContext.xml中，对
	《
	<import
		resource="classpath:cn/wenhao/WebWork？/sample/actionFild/action_beans.xml" />
		
		》
	中，对WebWork？进行修改，修改为自定义的包名
	
二：config/classSavePath.properties
	中的相关属性时默认的，不需要进行修改
	
三：在WebWork/example/model
	包下编写实体对象，进行配置注解的配置
	然后运行：
	cn/wenhao/framework/codeUtils/CodeCreateOperation 的测试类，该测试类中包含有两个方法
		一个是强制更新所有的Model对象
		另一个只会更新在Model类上加了JustThisClass注解的Model对象类
		
		/**
		 * 更新指定路径下的所有的Model类的信息
		 */
		@Test
		public void updateAll() {
			try {
				PropertiesFileUtils.setFileFirst();
				AutoCreate.autoCreate(true);
			} catch (IOException e) {
				System.out.println("无法更新配置文件：" + CUtils.PROP_FILE_NAME);
				e.printStackTrace();
			}
		}
		
		
		/**
		 * 更新指定路径下的有些Model类的信息，此类应该是添加上了JustThisClass的注解
		 */
		@Test
		public void updateSome() {
			try {
				PropertiesFileUtils.setFileFirst();
				AutoCreate.autoCreate(false);
			} catch (IOException e) {
				System.out.println("无法更新配置文件：" + CUtils.PROP_FILE_NAME);
				e.printStackTrace();
			}
		}
		
		另外，由于在AbstractModel类中添加了一个Test方法，该方法
		@Test
		public void autoCreateCurrentModel() {
			cn.wenhao.framework.codeUtils.codeMakerUtils.AutoCreate
					.autoCurrentModel(this);
		}
		
		主要用于在修改了Model雷猴，直接可以将其当做一个测试类运行，此时，该运行的结果是只更新当前的Model类的所有需要进行更新的
		类和配置文件
	
	
	类，程序会自动生成相关实体类的配置文件和QueryModel，Dao，DaoImpl，Ebi(ServiceInterface),Ebo(ServiceImpl)
		并且会自动在src目录下的
		daoimpl_beans.xml映射文件中配置Dao的相关信息，提供给Spring管理
		ebo_beans.xml映射文件中配置Ebi的相关信息，提供给spring管理
	
		
		
		
		