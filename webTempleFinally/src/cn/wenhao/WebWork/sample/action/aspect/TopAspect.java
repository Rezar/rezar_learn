package cn.wenhao.WebWork.sample.action.aspect;

import java.util.Map;

import org.apache.struts2.interceptor.ApplicationAware;
import org.aspectj.lang.annotation.Aspect;


@Aspect
public class TopAspect implements ApplicationAware{
	
	private Map<String,Object> applicationMap;

	
	public void topBefore(){
		System.out.println("启动");
	}
	
	@Override
	public void setApplication(Map<String, Object> arg0) {
		this.applicationMap = arg0;
	}
	
	

}
