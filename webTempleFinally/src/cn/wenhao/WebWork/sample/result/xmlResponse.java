package cn.wenhao.WebWork.sample.result;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.Result;

public class xmlResponse implements Result {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3755127587740756670L;

	@Override
	public void execute(ActionInvocation arg0) throws Exception {
		HttpServletResponse response = ServletActionContext.getResponse();
		HttpServletRequest request = ServletActionContext.getRequest();
		response.setContentType("application/xml;charset=utf-8");
		String jsonResult = (String) request.getAttribute("result");
		response.getWriter().write(jsonResult);
	}

}
