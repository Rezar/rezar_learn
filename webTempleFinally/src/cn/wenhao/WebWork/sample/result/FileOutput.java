package cn.wenhao.WebWork.sample.result;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.Result;

public class FileOutput implements Result {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2534439767177310686L;

	@Override
	public void execute(ActionInvocation arg0) throws Exception {
		HttpServletResponse response = ServletActionContext.getResponse();
		File file = (File) ServletActionContext.getRequest().getAttribute("file");
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
		int read = 0;
		OutputStream out = response.getOutputStream();
		while((read = bis.read()) != -1){
			out.write(read);
		}
		bis.close();
		out.flush();
		out.close();
	}

}
