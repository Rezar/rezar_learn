package cn.wenhao.WebWork.example.pageFormModel;

public class RegisterFormModel {
	
	private String loginName;
	private String loginPassword;
	private String loginPasswordRep;
	private String email;
	private String verifyCode;
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLoginPassword() {
		return loginPassword;
	}
	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}
	public String getLoginPasswordRep() {
		return loginPasswordRep;
	}
	public void setLoginPasswordRep(String loginPasswordRep) {
		this.loginPasswordRep = loginPasswordRep;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getVerifyCode() {
		return verifyCode;
	}
	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}
	@Override
	public String toString() {
		return "RegisterFormModel [loginName=" + loginName + ", loginPassword="
				+ loginPassword + ", loginPasswordRep=" + loginPasswordRep
				+ ", email=" + email + ", verifyCode=" + verifyCode + "]";
	}
	
	public void validate() {
		
	}

}
