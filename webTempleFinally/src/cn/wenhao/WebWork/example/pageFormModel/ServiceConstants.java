package cn.wenhao.WebWork.example.pageFormModel;

import java.lang.reflect.Field;
import java.util.Properties;

public class ServiceConstants {

	public Integer PAGE_SIZE = 10;
	public Integer ORDER_PAGE_SIZE= 8;

	private static ServiceConstants SERVICE_CONSTANTS;

	private ServiceConstants() {
	}

	public static ServiceConstants getSC() {
		init();
		return SERVICE_CONSTANTS;
	}

	private static void init() {
		if (SERVICE_CONSTANTS == null) {
			SERVICE_CONSTANTS = new ServiceConstants();
			Properties prop = new Properties();
			try {
				prop.load(ServiceConstants.class.getClassLoader()
						.getResourceAsStream("Constansts.properties"));
				Field[] fields = ServiceConstants.class.getDeclaredFields();
				for (Field field : fields) {
					System.out.println(field.getModifiers());
					if (field.getModifiers() == 10) {
						continue;
					}
					field.setAccessible(true);
					String fieldName = field.getName();
					Object value = Integer
							.parseInt(prop.getProperty(fieldName));
					field.set(SERVICE_CONSTANTS, value);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/*
	 * public static void main(String[] args) { int result =
	 * ServiceConstants.getSC().PAGE_SIZE; System.out.println(result); }
	 */
}
