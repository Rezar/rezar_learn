package cn.wenhao.WebWork.example.pageFormModel;

import java.util.List;

public class PageBean<T> {

	private int currentPage;
	private int pageSize;
	private int totalRecord;
	private List<T> beanList;
	private String url;

	/**
	 * 获取总页数
	 * 
	 * @return
	 */
	public int getTotalPage() {
		int result = this.totalRecord / this.pageSize;
		return this.totalRecord % this.pageSize == 0 ? result : result + 1;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}

	public List<T> getBeanList() {
		return beanList;
	}

	public void setBeanList(List<T> beanList) {
		this.beanList = beanList;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "PageBean [currentPage=" + currentPage + ", pageSize="
				+ pageSize + ", totalRecord=" + totalRecord + ", url=" + url
				+ "]";
	}
}
