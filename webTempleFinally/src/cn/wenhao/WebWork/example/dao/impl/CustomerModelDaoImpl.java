package cn.wenhao.WebWork.example.dao.impl;
import cn.wenhao.WebWork.example.model.CustomerModel;
import cn.wenhao.WebWork.example.model.queryModel.CustomerQueryModel;
import cn.wenhao.WebWork.example.dao.dao.CustomerModelDao;
import cn.wenhao.framework.dao.AbstractHibernateDaoImpl;
public class CustomerModelDaoImpl extends AbstractHibernateDaoImpl<CustomerModel,CustomerQueryModel> implements CustomerModelDao{}