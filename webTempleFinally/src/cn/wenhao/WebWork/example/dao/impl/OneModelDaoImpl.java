package cn.wenhao.WebWork.example.dao.impl;
import cn.wenhao.WebWork.example.model.OneModel;
import cn.wenhao.WebWork.example.model.queryModel.OneQueryModel;
import cn.wenhao.WebWork.example.dao.dao.OneModelDao;
import cn.wenhao.framework.dao.AbstractHibernateDaoImpl;
public class OneModelDaoImpl extends AbstractHibernateDaoImpl<OneModel,OneQueryModel> implements OneModelDao{}