package cn.wenhao.WebWork.example.dao.impl;
import cn.wenhao.WebWork.example.model.GoodsModel;
import cn.wenhao.WebWork.example.model.queryModel.GoodsQueryModel;
import cn.wenhao.WebWork.example.dao.dao.GoodsModelDao;
import cn.wenhao.framework.dao.AbstractHibernateDaoImpl;
public class GoodsModelDaoImpl extends AbstractHibernateDaoImpl<GoodsModel,GoodsQueryModel> implements GoodsModelDao{}