package cn.wenhao.WebWork.example.dao.impl;
import cn.wenhao.WebWork.example.model.CateGoryModel;
import cn.wenhao.WebWork.example.model.queryModel.CateGoryQueryModel;
import cn.wenhao.WebWork.example.dao.dao.CateGoryModelDao;
import cn.wenhao.framework.dao.AbstractHibernateDaoImpl;
public class CateGoryModelDaoImpl extends AbstractHibernateDaoImpl<CateGoryModel,CateGoryQueryModel> implements CateGoryModelDao{}