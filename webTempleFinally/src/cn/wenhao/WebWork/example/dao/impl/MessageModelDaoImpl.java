package cn.wenhao.WebWork.example.dao.impl;
import cn.wenhao.WebWork.example.model.MessageModel;
import cn.wenhao.WebWork.example.model.queryModel.MessageQueryModel;
import cn.wenhao.WebWork.example.dao.dao.MessageModelDao;
import cn.wenhao.framework.dao.AbstractHibernateDaoImpl;
public class MessageModelDaoImpl extends AbstractHibernateDaoImpl<MessageModel,MessageQueryModel> implements MessageModelDao{}