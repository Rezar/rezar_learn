package cn.wenhao.WebWork.example.dao.impl;
import cn.wenhao.WebWork.example.model.UserModel;
import cn.wenhao.WebWork.example.model.queryModel.UserQueryModel;
import cn.wenhao.WebWork.example.dao.dao.UserModelDao;
import cn.wenhao.framework.dao.AbstractHibernateDaoImpl;
public class UserModelDaoImpl extends AbstractHibernateDaoImpl<UserModel,UserQueryModel> implements UserModelDao{}