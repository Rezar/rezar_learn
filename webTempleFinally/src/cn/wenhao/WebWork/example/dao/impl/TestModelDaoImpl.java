package cn.wenhao.WebWork.example.dao.impl;
import cn.wenhao.WebWork.example.model.TestModel;
import cn.wenhao.WebWork.example.model.queryModel.TestQueryModel;
import cn.wenhao.WebWork.example.dao.dao.TestModelDao;
import cn.wenhao.framework.dao.AbstractHibernateDaoImpl;
public class TestModelDaoImpl extends AbstractHibernateDaoImpl<TestModel,TestQueryModel> implements TestModelDao{}