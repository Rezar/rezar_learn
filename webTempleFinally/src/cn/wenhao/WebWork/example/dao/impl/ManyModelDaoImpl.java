package cn.wenhao.WebWork.example.dao.impl;
import cn.wenhao.WebWork.example.model.ManyModel;
import cn.wenhao.WebWork.example.model.queryModel.ManyQueryModel;
import cn.wenhao.WebWork.example.dao.dao.ManyModelDao;
import cn.wenhao.framework.dao.AbstractHibernateDaoImpl;
public class ManyModelDaoImpl extends AbstractHibernateDaoImpl<ManyModel,ManyQueryModel> implements ManyModelDao{}