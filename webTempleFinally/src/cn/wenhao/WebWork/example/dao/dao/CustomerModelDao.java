package cn.wenhao.WebWork.example.dao.dao;
import cn.wenhao.WebWork.example.model.CustomerModel;
import cn.wenhao.WebWork.example.model.queryModel.CustomerQueryModel;
import cn.wenhao.framework.dao.Dao;
public interface CustomerModelDao extends Dao<CustomerModel,CustomerQueryModel>{}