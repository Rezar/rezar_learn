package cn.wenhao.WebWork.example.dao.dao;
import cn.wenhao.WebWork.example.model.GoodsModel;
import cn.wenhao.WebWork.example.model.queryModel.GoodsQueryModel;
import cn.wenhao.framework.dao.Dao;
public interface GoodsModelDao extends Dao<GoodsModel,GoodsQueryModel>{}