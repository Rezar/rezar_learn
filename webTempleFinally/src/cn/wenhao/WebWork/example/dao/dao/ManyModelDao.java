package cn.wenhao.WebWork.example.dao.dao;
import cn.wenhao.WebWork.example.model.ManyModel;
import cn.wenhao.WebWork.example.model.queryModel.ManyQueryModel;
import cn.wenhao.framework.dao.Dao;
public interface ManyModelDao extends Dao<ManyModel,ManyQueryModel>{}