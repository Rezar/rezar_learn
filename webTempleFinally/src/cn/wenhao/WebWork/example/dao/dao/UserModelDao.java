package cn.wenhao.WebWork.example.dao.dao;
import cn.wenhao.WebWork.example.model.UserModel;
import cn.wenhao.WebWork.example.model.queryModel.UserQueryModel;
import cn.wenhao.framework.dao.Dao;
public interface UserModelDao extends Dao<UserModel,UserQueryModel>{}