package cn.wenhao.WebWork.example.dao.dao;
import cn.wenhao.WebWork.example.model.MessageModel;
import cn.wenhao.WebWork.example.model.queryModel.MessageQueryModel;
import cn.wenhao.framework.dao.Dao;
public interface MessageModelDao extends Dao<MessageModel,MessageQueryModel>{}