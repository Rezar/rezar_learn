package cn.wenhao.WebWork.example.dao.dao;
import cn.wenhao.WebWork.example.model.CateGoryModel;
import cn.wenhao.WebWork.example.model.queryModel.CateGoryQueryModel;
import cn.wenhao.framework.dao.Dao;
public interface CateGoryModelDao extends Dao<CateGoryModel,CateGoryQueryModel>{}