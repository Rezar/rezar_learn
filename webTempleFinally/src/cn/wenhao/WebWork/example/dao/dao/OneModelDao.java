package cn.wenhao.WebWork.example.dao.dao;
import cn.wenhao.WebWork.example.model.OneModel;
import cn.wenhao.WebWork.example.model.queryModel.OneQueryModel;
import cn.wenhao.framework.dao.Dao;
public interface OneModelDao extends Dao<OneModel,OneQueryModel>{}