package cn.wenhao.WebWork.example.dao.dao;
import cn.wenhao.WebWork.example.model.TestModel;
import cn.wenhao.WebWork.example.model.queryModel.TestQueryModel;
import cn.wenhao.framework.dao.Dao;
public interface TestModelDao extends Dao<TestModel,TestQueryModel>{}