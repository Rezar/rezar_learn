package cn.wenhao.WebWork.example.bussiness.ebi;
import cn.wenhao.WebWork.example.model.CateGoryModel;
import cn.wenhao.WebWork.example.model.queryModel.CateGoryQueryModel;
import cn.wenhao.framework.bussiness.Ebi;
public interface CateGoryEbi extends Ebi<CateGoryModel,CateGoryQueryModel> {}