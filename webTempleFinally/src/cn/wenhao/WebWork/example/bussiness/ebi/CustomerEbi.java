package cn.wenhao.WebWork.example.bussiness.ebi;
import cn.wenhao.WebWork.example.model.CustomerModel;
import cn.wenhao.WebWork.example.model.queryModel.CustomerQueryModel;
import cn.wenhao.framework.bussiness.Ebi;
public interface CustomerEbi extends Ebi<CustomerModel,CustomerQueryModel> {}