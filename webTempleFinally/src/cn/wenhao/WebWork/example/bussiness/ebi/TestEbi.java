package cn.wenhao.WebWork.example.bussiness.ebi;
import cn.wenhao.WebWork.example.model.TestModel;
import cn.wenhao.WebWork.example.model.queryModel.TestQueryModel;
import cn.wenhao.framework.bussiness.Ebi;
public interface TestEbi extends Ebi<TestModel,TestQueryModel> {}