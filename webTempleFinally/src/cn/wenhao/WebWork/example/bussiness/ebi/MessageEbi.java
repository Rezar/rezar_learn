package cn.wenhao.WebWork.example.bussiness.ebi;
import cn.wenhao.WebWork.example.model.MessageModel;
import cn.wenhao.WebWork.example.model.queryModel.MessageQueryModel;
import cn.wenhao.framework.bussiness.Ebi;
public interface MessageEbi extends Ebi<MessageModel,MessageQueryModel> {}