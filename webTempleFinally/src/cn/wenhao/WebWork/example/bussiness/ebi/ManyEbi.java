package cn.wenhao.WebWork.example.bussiness.ebi;
import cn.wenhao.WebWork.example.model.ManyModel;
import cn.wenhao.WebWork.example.model.queryModel.ManyQueryModel;
import cn.wenhao.framework.bussiness.Ebi;
public interface ManyEbi extends Ebi<ManyModel,ManyQueryModel> {}