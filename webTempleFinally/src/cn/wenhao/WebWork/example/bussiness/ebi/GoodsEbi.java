package cn.wenhao.WebWork.example.bussiness.ebi;
import cn.wenhao.WebWork.example.model.GoodsModel;
import cn.wenhao.WebWork.example.model.queryModel.GoodsQueryModel;
import cn.wenhao.framework.bussiness.Ebi;
public interface GoodsEbi extends Ebi<GoodsModel,GoodsQueryModel> {}