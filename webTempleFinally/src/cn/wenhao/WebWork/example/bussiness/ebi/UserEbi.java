package cn.wenhao.WebWork.example.bussiness.ebi;

import cn.wenhao.WebWork.example.model.UserModel;
import cn.wenhao.WebWork.example.model.queryModel.UserQueryModel;
import cn.wenhao.framework.bussiness.Ebi;

public interface UserEbi extends Ebi<UserModel, UserQueryModel> {
}