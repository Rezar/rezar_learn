package cn.wenhao.WebWork.example.bussiness.ebi;
import cn.wenhao.WebWork.example.model.OneModel;
import cn.wenhao.WebWork.example.model.queryModel.OneQueryModel;
import cn.wenhao.framework.bussiness.Ebi;
public interface OneEbi extends Ebi<OneModel,OneQueryModel> {}