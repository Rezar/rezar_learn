package cn.wenhao.WebWork.example.bussiness.ebo;
import cn.wenhao.WebWork.example.model.OneModel;
import cn.wenhao.WebWork.example.model.queryModel.OneQueryModel;
import cn.wenhao.WebWork.example.bussiness.ebi.OneEbi;
import cn.wenhao.framework.bussiness.AbstractEbo;
public class OneEbo extends AbstractEbo<OneModel,OneQueryModel> implements OneEbi{}