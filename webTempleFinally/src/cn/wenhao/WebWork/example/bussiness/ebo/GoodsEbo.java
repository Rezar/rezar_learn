package cn.wenhao.WebWork.example.bussiness.ebo;
import cn.wenhao.WebWork.example.model.GoodsModel;
import cn.wenhao.WebWork.example.model.queryModel.GoodsQueryModel;
import cn.wenhao.WebWork.example.bussiness.ebi.GoodsEbi;
import cn.wenhao.framework.bussiness.AbstractEbo;
public class GoodsEbo extends AbstractEbo<GoodsModel,GoodsQueryModel> implements GoodsEbi{}