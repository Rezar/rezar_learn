package cn.wenhao.WebWork.example.bussiness.ebo;
import cn.wenhao.WebWork.example.model.ManyModel;
import cn.wenhao.WebWork.example.model.queryModel.ManyQueryModel;
import cn.wenhao.WebWork.example.bussiness.ebi.ManyEbi;
import cn.wenhao.framework.bussiness.AbstractEbo;
public class ManyEbo extends AbstractEbo<ManyModel,ManyQueryModel> implements ManyEbi{}