package cn.wenhao.WebWork.example.bussiness.ebo;
import cn.wenhao.WebWork.example.model.CateGoryModel;
import cn.wenhao.WebWork.example.model.queryModel.CateGoryQueryModel;
import cn.wenhao.WebWork.example.bussiness.ebi.CateGoryEbi;
import cn.wenhao.framework.bussiness.AbstractEbo;
public class CateGoryEbo extends AbstractEbo<CateGoryModel,CateGoryQueryModel> implements CateGoryEbi{}