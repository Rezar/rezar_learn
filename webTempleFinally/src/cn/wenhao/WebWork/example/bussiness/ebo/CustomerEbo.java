package cn.wenhao.WebWork.example.bussiness.ebo;
import cn.wenhao.WebWork.example.model.CustomerModel;
import cn.wenhao.WebWork.example.model.queryModel.CustomerQueryModel;
import cn.wenhao.WebWork.example.bussiness.ebi.CustomerEbi;
import cn.wenhao.framework.bussiness.AbstractEbo;
public class CustomerEbo extends AbstractEbo<CustomerModel,CustomerQueryModel> implements CustomerEbi{}