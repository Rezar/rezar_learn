package cn.wenhao.WebWork.example.bussiness.ebo;

import cn.wenhao.WebWork.example.model.UserModel;
import cn.wenhao.WebWork.example.model.queryModel.UserQueryModel;
import cn.wenhao.WebWork.example.bussiness.ebi.UserEbi;
import cn.wenhao.framework.bussiness.AbstractEbo;

public class UserEbo extends AbstractEbo<UserModel, UserQueryModel> implements
		UserEbi {
}