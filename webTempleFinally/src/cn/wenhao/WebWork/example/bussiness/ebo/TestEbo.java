package cn.wenhao.WebWork.example.bussiness.ebo;
import cn.wenhao.WebWork.example.model.TestModel;
import cn.wenhao.WebWork.example.model.queryModel.TestQueryModel;
import cn.wenhao.WebWork.example.bussiness.ebi.TestEbi;
import cn.wenhao.framework.bussiness.AbstractEbo;
public class TestEbo extends AbstractEbo<TestModel,TestQueryModel> implements TestEbi{}