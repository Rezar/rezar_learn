package cn.wenhao.WebWork.example.bussiness.ebo;
import cn.wenhao.WebWork.example.model.MessageModel;
import cn.wenhao.WebWork.example.model.queryModel.MessageQueryModel;
import cn.wenhao.WebWork.example.bussiness.ebi.MessageEbi;
import cn.wenhao.framework.bussiness.AbstractEbo;
public class MessageEbo extends AbstractEbo<MessageModel,MessageQueryModel> implements MessageEbi{}