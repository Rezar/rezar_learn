package cn.wenhao.WebWork.example.model;

import cn.wenhao.framework.codeUtils.annotation.NotHql;
import cn.wenhao.framework.codeUtils.annotation.One2OneDoublePrimaryKey;
import cn.wenhao.framework.codeUtils.annotation.PrimaryKey;
import cn.wenhao.framework.model.QueryModel;

public class CustomerModel extends QueryModel {

	/**
	 * 
	 */
	@NotHql
	private static final long serialVersionUID = 6608364407630056427L;

	@PrimaryKey
	private String id;

	// @One2OneDoubleWithOne2OneP(name = "testModel")
	@One2OneDoublePrimaryKey()
	private TestModel testModel;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TestModel getTestModel() {
		return testModel;
	}

	public void setTestModel(TestModel testModel) {
		this.testModel = testModel;
	}

}
