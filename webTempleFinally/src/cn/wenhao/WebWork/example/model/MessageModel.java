package cn.wenhao.WebWork.example.model;

import cn.wenhao.framework.codeUtils.annotation.Many2OneMapped;
import cn.wenhao.framework.codeUtils.annotation.PrimaryKey;
import cn.wenhao.framework.model.QueryModel;

public class MessageModel extends QueryModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6612529924867620384L;

	@PrimaryKey
	private String id;
	@Many2OneMapped
	private CustomerModel customer;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CustomerModel getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerModel customer) {
		this.customer = customer;
	}

}