package cn.wenhao.WebWork.example.model;

import java.util.HashSet;
import java.util.Set;

import cn.wenhao.framework.codeUtils.annotation.Column;
import cn.wenhao.framework.codeUtils.annotation.GeneralField;
import cn.wenhao.framework.codeUtils.annotation.Many2OneMapped;
import cn.wenhao.framework.codeUtils.annotation.NotHql;
import cn.wenhao.framework.codeUtils.annotation.One2ManySetMapped;
import cn.wenhao.framework.codeUtils.annotation.PrimaryKey;
import cn.wenhao.framework.codeUtils.annotation.PropertyField;
import cn.wenhao.framework.model.QueryModel;

public class CateGoryModel extends QueryModel {

	/**
	 * 
	 */
	@NotHql
	private static final long serialVersionUID = 1651876203676769576L;

	@PrimaryKey
	private String id;
	@PropertyField(property = @GeneralField(length = 20))
	private String cname; // 分类的名称
	@Many2OneMapped
	private CateGoryModel parent; // 分类的父分类
	@NotHql
	@One2ManySetMapped
	private Set<CateGoryModel> childeCategory = new HashSet<CateGoryModel>(0);
	@PropertyField(column = @Column(name = "order_"))
	private Integer order; // 用于排序

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public CateGoryModel getParent() {
		return parent;
	}

	public void setParent(CateGoryModel parent) {
		this.parent = parent;
	}

	public Set<CateGoryModel> getChildeCategory() {
		return childeCategory;
	}

	public void setChildeCategory(Set<CateGoryModel> childeCategory) {
		this.childeCategory = childeCategory;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "CateGoryModel [id=" + id + ", cname=" + cname + ", parent="
				+ parent + ", order=" + order + "]";
	}

}
