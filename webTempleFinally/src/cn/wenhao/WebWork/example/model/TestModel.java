package cn.wenhao.WebWork.example.model;

import cn.wenhao.framework.codeUtils.annotation.Generator;
import cn.wenhao.framework.codeUtils.annotation.One2OneSignalPrimaryKey;
import cn.wenhao.framework.codeUtils.annotation.PrimaryKey;
import cn.wenhao.framework.codeUtils.annotation.PropertyField;
import cn.wenhao.framework.model.QueryModel;

// 配置在类上面，指定该类的联合主键
/*
 * @Composite(keyPropertys = { @KeyProperty(name = "firstName"),
 * 
 * @KeyProperty(name = "lastName") })
 */
public class TestModel extends QueryModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3198246456846378250L;

	private String firstName;
	private String lastName;

	// @PrimaryKey(id = @IdAnnotation(name = "id"), generator =
	// @Generator(class_ = "foreigh", param = "customer"))
	// @PrimaryKey() 单独的主键
	@PrimaryKey(generator = @Generator(class_ = "foreign", param = "customer"))
	private String id;

	// @PropertyField(property = @GeneralField(name = "name"), column =
	// @Column(name = "name"))
	@PropertyField()
	private String name;

	// @PropertyField(property = @GeneralField(name = "age", type = "integer"),
	// column = @Column(name = "age", default_ = "13"))
	@PropertyField()
	private int age;

	// @PropertyField(property = @GeneralField(name = "alipayNumber", length =
	// 30))
	@PropertyField()
	private String alipayNumber;

	// @PropertyField(property = @GeneralField(name = "baBiBalance"))
	@PropertyField()
	private Double baBiBalance;

	// @PropertyField(property = @GeneralField(name = "creaditRating"), column =
	// @Column(name = "creaditRating_", not_null = false))
	@PropertyField()
	private int creaditRating;

	// @One2OneSignalWithMany2OneP(name = "customer")
	// @One2OneSignalForeign()
	@One2OneSignalPrimaryKey
	private CustomerModel customer;

	public CustomerModel getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerModel customer) {
		this.customer = customer;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAlipayNumber() {
		return alipayNumber;
	}

	public void setAlipayNumber(String alipayNumber) {
		this.alipayNumber = alipayNumber;
	}

	public Double getBaBiBalance() {
		return baBiBalance;
	}

	public void setBaBiBalance(Double baBiBalance) {
		this.baBiBalance = baBiBalance;
	}

	public int getCreaditRating() {
		return creaditRating;
	}

	public void setCreaditRating(int creaditRating) {
		this.creaditRating = creaditRating;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
