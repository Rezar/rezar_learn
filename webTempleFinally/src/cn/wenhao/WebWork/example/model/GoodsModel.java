package cn.wenhao.WebWork.example.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

import cn.wenhao.framework.codeUtils.annotation.Column;
import cn.wenhao.framework.codeUtils.annotation.Many2OneMapped;
import cn.wenhao.framework.codeUtils.annotation.NotHql;
import cn.wenhao.framework.codeUtils.annotation.PrimaryKey;
import cn.wenhao.framework.codeUtils.annotation.PropertyField;
import cn.wenhao.framework.model.QueryModel;


public class GoodsModel extends QueryModel {

	/**
	 * /webTempleFinally
	 */
	@NotHql
	private static final long serialVersionUID = 8262276956039901218L;

	@PrimaryKey
	private String id;
	@PropertyField
	private String goodsName;
	@PropertyField
	private UserModel user;
	@Many2OneMapped
	private CateGoryModel cateGory;
	@PropertyField
	private double sellPrice;
	@PropertyField
	private double currentPrice;
	@PropertyField
	private double discount;
	@PropertyField
	private String sellTime;
	@PropertyField(column = @Column(name = "desc_"))
	private String desc;
	// 小图路径
	@PropertyField
	private String img_b;
	// 大图路径
	@PropertyField
	private String img_w;
	// 该商品是否是拍卖商品（0,：代表是普通交换商品，1：代表是拍卖商品）
	@PropertyField
	private Integer isAuction;

	// 当前该商品的浏览量,可以当做排序的标准
	@PropertyField
	private Integer orderBy;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getOrderBy() {
		return orderBy;
	}

	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}

	public Integer getIsAuction() {
		return isAuction;
	}

	public void setIsAuction(Integer isAuction) {
		this.isAuction = isAuction;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

	public CateGoryModel getCateGory() {
		return cateGory;
	}

	public void setCateGory(CateGoryModel cateGory) {
		this.cateGory = cateGory;
	}

	public double getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(double sellPrice) {
		this.sellPrice = sellPrice;
	}

	public double getDiscount() {
		BigDecimal b1 = new BigDecimal(this.sellPrice + "");
		BigDecimal b2 = new BigDecimal(this.currentPrice + "");
		this.discount = 1 - b2.divide(b1, 2, RoundingMode.HALF_UP)
				.doubleValue();
		/*
		 * BigDecimal b3 = new BigDecimal(this.discount, new MathContext(
		 * MathContext.UNLIMITED.getPrecision(), RoundingMode.HALF_UP));
		 */
		return this.discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getSellTime() {
		return sellTime;
	}

	public void setSellTime(String sellTime) {
		this.sellTime = sellTime;
	}

	public String getImg_b() {
		return img_b;
	}

	public void setImg_b(String img_b) {
		this.img_b = img_b;
	}

	public String getImg_w() {
		return img_w;
	}

	public void setImg_w(String img_w) {
		this.img_w = img_w;
	}

	@Override
	public String toString() {
		return "Goods [id=" + id + ", goodsName=" + goodsName + ", sellPrice="
				+ sellPrice + ", discount=" + discount + ", sellTime="
				+ sellTime + ", img_b=" + img_b + ", img_w=" + img_w + "]";
	}
}
