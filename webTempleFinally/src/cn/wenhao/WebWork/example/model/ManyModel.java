package cn.wenhao.WebWork.example.model;

import java.util.HashSet;
import java.util.Set;

import cn.wenhao.framework.codeUtils.annotation.Many2ManySetMapped;
import cn.wenhao.framework.codeUtils.annotation.PrimaryKey;
import cn.wenhao.framework.model.QueryModel;

public class ManyModel extends QueryModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5061770676751110510L;

	@PrimaryKey
	private String id;

//	 @Many2OneMapped(column = "one_id", name = "one")
	@Many2ManySetMapped
	private Set<OneModel> one = new HashSet<OneModel>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Set<OneModel> getOne() {
		return one;
	}

	public void setOne(Set<OneModel> one) {
		this.one = one;
	}

}
