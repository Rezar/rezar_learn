package cn.wenhao.WebWork.example.model;

import java.util.HashSet;
import java.util.Set;

import cn.wenhao.framework.codeUtils.annotation.NotHql;
import cn.wenhao.framework.codeUtils.annotation.One2ManySetMapped;
import cn.wenhao.framework.codeUtils.annotation.PrimaryKey;
import cn.wenhao.framework.model.QueryModel;

public class OneModel extends QueryModel {

	/**
	 * 
	 */
	@NotHql
	private static final long serialVersionUID = 3832770664184268338L;

	@PrimaryKey()
	private String id;

	 @One2ManySetMapped()
//	@Many2ManySetMapped
	private Set<ManyModel> manys = new HashSet<ManyModel>();

	public Set<ManyModel> getManys() {
		return manys;
	}

	public void setManys(Set<ManyModel> manys) {
		this.manys = manys;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
