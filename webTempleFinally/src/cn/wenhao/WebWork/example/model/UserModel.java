package cn.wenhao.WebWork.example.model;

import java.util.HashMap;
import java.util.Map;

import cn.wenhao.framework.codeUtils.annotation.NotHql;
import cn.wenhao.framework.codeUtils.annotation.PropertyField;
import cn.wenhao.framework.codeUtils.annotation.PrimaryKey;
import cn.wenhao.framework.model.QueryModel;

public class UserModel extends QueryModel {

	/**
	 * 
	 */
	@NotHql
	private static final long serialVersionUID = 4546480214633361157L;

	@PrimaryKey
	private String id;
	@PropertyField
	private String loginName;
	@PropertyField
	private String nikeName;
	@PropertyField
	private String phone;
	@PropertyField
	private String qq;
	@PropertyField
	private String loginPassword;
	@PropertyField
	private String email;
	@PropertyField
	private Integer state ; // 已经激活或者仍未激活
	@PropertyField
	private String verifyCode;
	
	@PropertyField
	private String loginPasswordRep;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getNikeName() {
		return nikeName;
	}

	public void setNikeName(String nikeName) {
		this.nikeName = nikeName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public String getLoginPasswordRep() {
		return loginPasswordRep;
	}

	public void setLoginPasswordRep(String loginPasswordRep) {
		this.loginPasswordRep = loginPasswordRep;
	}

	/**
	 * 注册页面需要的校验方法
	 */
	public Map<String, String> validate() {
		HashMap<String, String> result = new HashMap<String, String>(0);
		if (this.loginName != null
				&& (this.loginName.length() > 20 || this.loginName.length() < 3)) {
			result.put("loginName", "用户名的长度在4到20位");
		}
		if (this.loginPassword != null
				&& (this.loginPassword.length() > 20 || this.loginPassword
						.length() < 6)) {
			result.put("loginPassword", "用户登陆密码的长度在6到20位");
		}
		if (this.loginPasswordRep != null
				&& (!this.loginPassword.equals(this.loginPasswordRep))) {
			result.put("loginPasswordRep", "两次输入密码不匹配！");
		}
		if (this.email != null && this.email.equals("")) {
			result.put("email", "用户邮箱不正确！");
		}
		return result;
	}

}
