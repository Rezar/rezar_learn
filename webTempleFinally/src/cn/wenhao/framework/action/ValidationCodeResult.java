package cn.wenhao.framework.action;

import java.awt.image.BufferedImage;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.Result;

/**
 * 在struts.xml中对Result的配置
 * 
 * <result-types> <result-type name="validationCodeResult"
 * class="cn.wenhao.framework.action.ValidationCodeResult" /> </result-types>
 * 
 * <action name="validationCode"
 * class="cn.wenhao.framework.action.ValidationCodeAction" > <result
 * name="success" type="validationCodeResult" /> </action>
 * 
 * @author Administrator
 */
public class ValidationCodeResult implements Result, SessionAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3844809682189445419L;

	private Map<String, Object> sessionMap;

	@Override
	public void execute(ActionInvocation arg0) throws Exception {
		ValidationCodeAction action = (ValidationCodeAction) arg0.getAction();
		HttpServletResponse response = ServletActionContext.getResponse();
		// 禁止图片缓存
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache_Control", "no-cache");
		response.setDateHeader("Expires", 0);

		// 设置要发送内容的媒体类型
		response.setContentType(action.getContextType());

		VerifyCode vc = new VerifyCode();

		ServletOutputStream sos = response.getOutputStream();

		BufferedImage image = vc.getImage();
		this.sessionMap.put("vCode", vc.getText());
		VerifyCode.output(image, response.getOutputStream());
		/*
		 * // 输出验证码图像数据 sos.write(action.getImageInByte());
		 */
		sos.close();
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sessionMap = arg0;
	}

}
