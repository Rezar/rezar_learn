package cn.wenhao.framework.action;

import java.io.IOException;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 在Session中保存的键为：validationCode
 * 
 * @author Administrator
 * 
 */
public class ValidationCodeAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3659839737299520088L;

	public String execute() throws IOException {
		return SUCCESS;
	}

	// 用于向自定义结果返回图像的MIME类型
	public String getContextType() {
		return "image/jpeg";
	}

}
