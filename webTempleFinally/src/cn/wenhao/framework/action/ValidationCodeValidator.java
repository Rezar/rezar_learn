package cn.wenhao.framework.action;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

public class ValidationCodeValidator extends FieldValidatorSupport {

	// 接收session中的验证码
	private String sessionValidationCode;
	// 只是是否删除字符串首位的字符
	private boolean trim;

	public boolean isTrim() {
		return trim;
	}

	public void setTrim(boolean trim) {
		this.trim = trim;
	}

	public String getSessionValidationCode() {
		return sessionValidationCode;
	}

	public void setSessionValidationCode(String sessionValidationCode) {
		this.sessionValidationCode = sessionValidationCode;
	}

	@Override
	public void validate(Object arg0) throws ValidationException {
		//获取到配置文件中填写的session中保存的验证码的值
		/**
		 * 在验证文件配置验证规则时，我们通过<param>元素想验证器实例传递sessionValidationCode属性值，为了取出保存在
		 * session中的该属性值，可以直接使用ognl表达式#session.validationCode,组成这个表达式的字符串将原封不动的
		 * 传递给验证器实例，为了能够解析这个表达式，从而取出session的属性值，可以利用基类的getFieldValue()方法来解析
		 * 表达式，从而获取在session中的验证码值。
		 */
		Object objValidateCode = getFieldValue(sessionValidationCode, arg0);
		System.out.println(objValidateCode);
		
		//得到字段名
		String fieldName = getFieldName();
		//得到字段的值
		String fieldValue = (String) getFieldValue(fieldName, arg0);
		
		//如果字段值为null,直接返回
		if(fieldName == null){
			return;
		}
		
		//如果trim为true，则删除字段值首位的空格字符
		if(this.trim){
			fieldValue = fieldValue.trim();
		}
		
		//如果字段值为"",直接返回
		if(fieldName.length() == 0){
			return;
		}
		
		if(!fieldValue.equalsIgnoreCase((String) objValidateCode)){
			addFieldError(fieldName, arg0);
		}
	}

}
