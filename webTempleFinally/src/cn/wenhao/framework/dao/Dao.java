package cn.wenhao.framework.dao;

import java.io.Serializable;
import java.util.List;

import cn.wenhao.framework.model.Model;

public interface Dao<M extends Model, QM extends M> {

	/**
	 * 增
	 * 
	 * @param model
	 * @return
	 */
	public boolean create(M model);

	/**
	 * 删
	 * 
	 * @param model
	 * @return
	 */
	public boolean delete(Serializable id);

	/**
	 * 改
	 * 
	 * @param model
	 * @return
	 */
	public boolean update(M model);

	/**
	 * 查
	 * 
	 * @param id
	 * @return
	 */
	public M getSingle(Serializable id);

	/**
	 * 根据一般检索条件来查询单个对象
	 */
	public M getSingle(QM qm);

	/**
	 * find
	 */
	/* public M getSigleByQueryModel(QM m); */

	/**
	 * 获取所有的记录 返回 -1 代表出错
	 */
	public int getAll();

	/**
	 * 获取记录总数，根据查询条件
	 * 
	 * @param qm
	 * @return
	 */
	public int getByConditionCount(QM qm);

	/**
	 * 查询分页
	 * 
	 * @param firstIndex
	 * @param maxRecords
	 * @return
	 */
	public List<M> getAll(int firstIndex, int maxRecords);

	/**
	 * 根据查询条件进行分页的查询
	 * 
	 * @param qm
	 * @param firstIndex
	 * @param maxRecords
	 * @return
	 */
	public List<M> getByCondition(QM qm, int firstIndex, int maxRecords);

	public List<M> getByCondition(QM qm, int firstIndex);

	public int getMaxCount(String fieldName);

	/**
	 * 数据在保存的时候不需要保证在完全的同一个事务中， 按照一定的大小分段保存数据 当某一段数据发生了异常时，不影响后续数据的插入
	 * 
	 * @param sourceList
	 */
	public void batchCreateWithTransaction(List<M> sourceList);

	/**
	 * 数据在插入的过程中保持完全的一致性，即原子性操作
	 * 
	 * @param sourceList
	 */
	public void batchCreate(List<M> sourceList);

	/**
	 * 批量更新
	 * 
	 * @param sourceList
	 */
	public void batchUpdate(List<M> sourceList);

}
