package cn.wenhao.framework.dao;

import java.lang.reflect.Field;
import java.util.List;

import org.hibernate.Query;

import cn.wenhao.framework.model.Expression;
import cn.wenhao.framework.model.Model;
import cn.wenhao.framework.model.QueryModel;
import cn.wenhao.framework.utils.Utils;

public class AbstractHibernateDaoImplUtils<M extends Model, QM extends M> {

	protected String orderBy(Class<?> clazz) {
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			String fieldName = field.getName();
			if (fieldName.contains("order")
					&& (field.getType() == Integer.class || field.getType() == int.class)) {
				return "o." + fieldName;
			}
		}
		return null;
	}

	protected String getOrderByFieldName(Class<?> class_) {
		String orderF = this.orderBy(class_);
		if (orderF != null) {
			return orderF;
		}
		return "o.orderBy";
	}

	/**
	 * 该方法进行了空实现，如果子类需要对查询条件进行限定，需要同时覆写该方法和prepareQuery方法 用于指定哪些属性需要攒与到查询条件中去
	 * 
	 * @param qm
	 * @return
	 */
	protected String generateHQLWhere(QM qm) {

		StringBuilder sb = new StringBuilder();

		List<Field> allField = null;
		try {
			allField = Utils.getAllNotNullField(qm);
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (Field field : allField) {
			final String fieldName = field.getName();
			System.out.println(fieldName);
			setWhereSql(qm, fieldName, sb);
		}

		return sb.toString();
	}

	/**
	 * 该方法进行了空实现，如果子类需要对查询条件进行限定并对限定属性赋值，需要同时覆写该方法和generateHQL方法
	 * 
	 * 用于对query方法设置相关查询条件的值
	 * 
	 * @param query
	 * @param qm
	 */
	protected void prepareQueryValue(Query query, QM qm) {
		try {
			List<Field> notNullField = Utils.getAllNotNullField(qm);
			for (Field field : notNullField) {
				field.setAccessible(true);
				setQuery(query, qm, field, field.get(qm));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 为某个属性设置hibernate query参数，利用如 and o.id=:id 的格式
	 * 
	 * @param qm
	 *            ：当前的查询对象，待作为where字句的参数都在其中进行了赋值
	 * @param fieldName
	 *            :当前不为空的一个属性的属性名
	 * @param sb
	 *            :用于拼凑query SQL 语句的 StringBuilder对象
	 */
	private void setWhereSql(Object qm, String fieldName, StringBuilder sb) {
		/**
		 * 
		 * Expression es: 如果该对象为null，说明不存在自定义的检索条件， ①： 使用默认的属性和该属性的值作为条件
		 * eg:qm:new BookQueryModel() fieldName:author 拼凑后的语句为： and
		 * o.author=:author
		 * 
		 * ②： 查询的对象，在该对象中设置有相关的查询信息 eg:new Expression("age",">",34);
		 * 该对象的第一个参数为：当前待作为查询条件的属性的名称，与属性的名称保持一致
		 * ①.1:如果该第一个属性没有设置，则使用属性名作为拼凑的属性字符串的值 即为：and o.author=:author
		 * ①.2：否则，使用该对象的该第一个参数的值作为拼凑的属性字符串的值 eg: BookQueryModel bqm =
		 * newBookQueryModel(); CateGoryModel cg = new CateGoryModel();
		 * cg.setId("1"); bqm.setCategory(cg); bqm.expression.put("category",new
		 * Expression("category.id","=",cg.getId())); 且有operator的默认操作符为 : =
		 */
		Expression es = ((QueryModel) qm).expressions.get(fieldName);
		sb.append(" and o.");
		if (es == null) {
			sb.append(fieldName).append(" ").append("=").append(":")
					.append(fieldName);
		} else {
			if (Utils.notNullAndNotEmpty(es.getEname())) {
				sb.append(es.getEname());
			} else {
				sb.append(fieldName);
			}
			boolean operatorIsNotNull = Utils.notNullAndNotEmpty(es
					.getOperator());
			sb.append(" ").append(operatorIsNotNull ? es.getOperator() : "=");
			if (!operatorIsNotNull) {
				es.setOperator("=");
			}
			if (!es.getOperator().trim().equalsIgnoreCase("is null")
					&& !es.getOperator().trim().equalsIgnoreCase("is not null")) {
				if (!es.getOperator().contains(":")) {
					sb.append(" :").append(fieldName);
				}
			}
		}
	}

	/**
	 * 为拼凑的query sql 字句进行占位符的实际值的替换
	 * 
	 * @param query
	 *            :Query对象
	 * @param qm
	 *            :当前的查询对象
	 * @param field
	 *            ：当前要进行设值的属性(该属性一定不为空值，对于简单类型的，也部位默认值)
	 * @param value
	 *            ：当前要为该属性进行设置的值
	 */
	private void setQuery(Query query, Object qm, Field field, Object value) {
		try {
			/**
			 * ①：获取到检索条件的对象， 该对象的获取为：利用QueryModel对象的expression参数中存放的该属性的键值对
			 * 其中，每一个Model对象都继承自QueryModel,而QueryModel继承自AbstractModel
			 * 且在QueryModel对象中设置有一个HashMap<String,Expression>的expression对象
			 * 用于添加检索条件，其中的键为待作为检索条件的属性的名称，值为关于该属性的检索条件对象 ②：设值过程：
			 * 如果关联在该属性上的检索对象不存在，或者该关联的检索对象存在但其value为空，
			 * 则使用查询对象QueryModel(子类，eg:BookQueryModel extends BookModel)
			 * 上该属性对应的值设置该占位符上的值 否则，使用该检索对象上的值
			 */
			Expression es = ((QueryModel) qm).expressions.get(field.getName());
			System.out.println(es);
			if (es != null) {
				if ((es.getOperator().trim().equalsIgnoreCase("is null") || es
						.getOperator().trim().equalsIgnoreCase("is not null"))) {
					return;
				}
			}

			if (es == null || es.getValue() == null) {
				query.setParameter(field.getName(), value);
			} else {
				System.out.println("line 158 of abstractHibernateDao "
						+ es.toString());
				query.setParameter(field.getName(), es.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 该方法进行了空实现，如果子类需要对查询语句进行多属性的查询(【多表查询】)，需要覆写该方法以及 * generateHQLFrom, *
	 * generateJoinConidition, * prepareResult 等方法 用于指定在多表查询的时候需要查询出哪些属性
	 * 
	 * @return
	 */
	protected String generateHQLSelect() {
		return "";
	}

	/**
	 * 该方法进行了空实现，如果子类需要进行【多表查询】，需要覆写该方法以及 * generateHQLSelect, *
	 * generateJoinConidition, * prepareResult 等方法 注意，自己写逗号，且不包括主表
	 * 
	 * @return
	 */
	protected String generateHQLFrom() {
		return "";
	}

	/**
	 * 该方法进行了空实现，如果子类需要进行【多表查询】，需要覆写该方法添加多表查询是的where限定条件 以及覆写 *
	 * generateHQLSelect, * generateHQLFrom, * prepareResult 等方法 注意，自己写and
	 * 
	 * @return
	 */
	protected String generateJoinConidition() {
		return "";
	}

	/**
	 * 该方法进行了默认的空实现，如果子类需要对查询的结果进行处理(【多表查询】的情况下),需要对该方法进行覆写 以及覆写 *
	 * generateHQLSelect, * generateHQLFrom, * generateJoinConidition 等方法
	 * 默认返回的为查询出的list集合
	 * 
	 * @param retList
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected List<M> prepareResult(List<?> retList) {
		return (List<M>) retList;
	}

}
