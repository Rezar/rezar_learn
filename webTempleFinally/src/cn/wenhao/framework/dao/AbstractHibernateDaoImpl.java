package cn.wenhao.framework.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import cn.wenhao.framework.interfaceCallback.HibernateCallback;
import cn.wenhao.framework.model.Model;
import cn.wenhao.framework.sessionFactory.HibernateSessionFactory;
import cn.wenhao.framework.utils.Utils;

@SuppressWarnings("unchecked")
public abstract class AbstractHibernateDaoImpl<M extends Model, QM extends M>
		extends AbstractHibernateDaoImplUtils<M, QM> implements Dao<M, QM> {

	private Log logger = LogFactory.getLog(getClass());

	protected final Class<M> modelClazz;
	protected final Class<QM> qumClazz;
	private final String modelName;

	private HibernateSessionFactory hsf;

	public void setHsf(HibernateSessionFactory hsf) {
		this.hsf = hsf;
	}

	/**
	 * 閫氳繃鍙嶅皠鎶�湳鏉ヨ幏鍙栧瓙绫讳紶鍏ョ殑娉涘瀷鐨勪俊鎭�
	 */
	public AbstractHibernateDaoImpl() {
		ParameterizedType parameterizedType = (ParameterizedType) this
				.getClass().getGenericSuperclass();

		Type[] genericTypes = parameterizedType.getActualTypeArguments();
		this.modelClazz = (Class<M>) genericTypes[0];
		this.qumClazz = (Class<QM>) genericTypes[1];
		this.modelName = this.modelClazz.getSimpleName();
	}

	public final Object execute(HibernateCallback action) throws Exception {

		Object ret = null;
		Session session = hsf.getSession();
		Transaction tx2 = session.getTransaction();
		boolean isActive = tx2.isActive();
		if (!isActive) {
			tx2.begin();
		}
		try {
			ret = action.doInSession(session);
			if (!isActive) {
				tx2.commit();
			}
		} catch (Exception e) {
			if (!isActive) {
				tx2.rollback();
			}
			logger.error("发生了异常，事务开始回滚", e.getCause());
			throw e;
		} finally {
			/*
			 * if(!isActive){ session.close(); }
			 */
		}
		return ret;
	}

	protected String orderBy() {
		return super.orderBy(modelClazz);
	}

	public final int getMaxCount(final String fieldName) {
		try {
			return (Integer) this.execute(new HibernateCallback() {
				@Override
				public Object doInSession(Session session) {
					String orderByFieldName = null;
					if (Utils.notNullAndNotEmpty(fieldName)) {
						orderByFieldName = fieldName;
					} else {
						getOrderByFieldName(modelClazz);
					}
					Query query = session.createQuery("Select MAX(?) From "
							+ modelName);
					query.setString(0, orderByFieldName);
					return query.list().size() > 0 ? query.list().get(0) : 0;
				}
			});
		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e1);
			return 0;
		}
	}

	@Override
	public final boolean create(final M model) {

		try {
			return (Boolean) this.execute(new HibernateCallback() {
				@Override
				public Object doInSession(Session session) {
					session.merge(model);
					return true;
				}
			});
		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e1);
			return false;
		}

	}

	@Override
	public final boolean delete(final Serializable id) {

		try {
			return (Boolean) this.execute(new HibernateCallback() {
				@Override
				public Object doInSession(Session session) {
					M model = (M) session.get(modelClazz, id);
					session.delete(model);
					return true;
				}
			});
		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e1);
			return false;
		}
	}

	@Override
	public final boolean update(final M model) {
		try {
			return (Boolean) this.execute(new HibernateCallback() {
				@Override
				public Object doInSession(Session session) {
					session.merge(model);
					return true;
				}
			});
		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e1);
			return false;
		}
	}

	@Override
	public final M getSingle(final Serializable id) {

		try {
			return (M) this.execute(new HibernateCallback() {
				@Override
				public Object doInSession(Session session) {
					return session.get(modelClazz, id);
				}
			});
		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e1);
			return null;
		}
	}

	@Override
	public M getSingle(final QM qm) {
		try {
			return (M) this.execute(new HibernateCallback() {
				@Override
				public Object doInSession(Session session) {
					Query query = session.createQuery(generateHQLSelect()
							+ " From " + modelName + " o " + generateHQLFrom()
							+ " where 1=1 " + generateHQLWhere(qm)
							+ generateJoinConidition() + " order by "
							+ (orderBy() == null ? "o.id" : orderBy()));
					prepareQueryValue(query, qm);
					List<M> retList = prepareResult(query.list());
					if (retList != null && retList.size() > 0) {
						return retList.get(0);
					} else
						return null;
				}
			});
		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e1);
			return null;
		}
	}

	@Override
	public final int getAll() {
		try {
			return (Integer) this.execute(new HibernateCallback() {
				@Override
				public Object doInSession(Session session) {
					Query query = session.createQuery("select count(o) From "
							+ modelName + " o order by "
							+ (orderBy() == null ? "o.id" : orderBy()));
					List<Object> list = query.list();
					int ret = ((Long) list.get(0)).intValue();
					return ret;
				}
			});
		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e1);
			return -1;
		}
	}

	@Override
	public final List<M> getAll(final int firstIndex, final int maxRecords) {

		try {
			return (List<M>) this.execute(new HibernateCallback() {
				@Override
				public Object doInSession(Session session) {
					Query query = session.createQuery(generateHQLSelect()
							+ " From " + modelName + " o " + generateHQLFrom()
							+ " where 1=1 " + generateJoinConidition()
							+ " order by "
							+ (orderBy() == null ? "o.id" : orderBy()));
					query.setFirstResult(firstIndex);
					query.setMaxResults(maxRecords);
					List<Object[]> ret = query.list();
					return prepareResult(ret);
				}
			});
		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e1);
		}

		return new ArrayList<M>();
	}

	@Override
	public final List<M> getByCondition(final QM qm, final int firstIndex,
			final int maxRecords) {
		try {
			return (List<M>) this.execute(new HibernateCallback() {
				@Override
				public Object doInSession(Session session) {
					Query query = session.createQuery(generateHQLSelect()
							+ " From " + modelName + " o " + generateHQLFrom()
							+ " where 1=1 " + generateHQLWhere(qm)
							+ generateJoinConidition() + " order by "
							+ (orderBy() == null ? "o.id" : orderBy()));
					prepareQueryValue(query, qm);
					query.setFirstResult(firstIndex);
					query.setMaxResults(maxRecords);
					List<Object[]> list = query.list();
					return prepareResult(list);
				}
			});
		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e1);
		}
		return new ArrayList<M>();
	}

	/**
	 * 取特定情况下的所有的记录
	 */
	@Override
	public List<M> getByCondition(QM qm, int firstIndex) {
		return this
				.getByCondition(qm, firstIndex, this.getByConditionCount(qm));
	}

	@Override
	public final int getByConditionCount(final QM qm) {

		try {
			return (Integer) this.execute(new HibernateCallback() {
				@Override
				public Object doInSession(Session session) {
					Query query = session.createQuery("Select count(o) From "
							+ modelName + " o where 1=1 "
							+ generateHQLWhere(qm) + " order by "
							+ (orderBy() == null ? "o.id" : orderBy()));
					prepareQueryValue(query, qm);
					List<Object> list = query.list();
					return ((Long) (list.get(0))).intValue();
				}
			});
		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e1);
			return -1;
		}
	}

	@Override
	public void batchCreateWithTransaction(List<M> sourceList) {
		Session session = null;
		try {
			session = hsf.getSession();
			int i = 0;
			for (M model : sourceList) {
				if (session.contains(model)) {
					continue;
				}
				session.save(model);
				i++;
				if (i % 20 == 0) {
					session.getTransaction().commit();
					session.flush();
					session.clear();
					session.beginTransaction();
				}
			}
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e);
			throw new RuntimeException(e);
		} finally {
			session.close();
		}
	}

	@Override
	public void batchCreate(final List<M> sourceList) {
		try {
			execute(new HibernateCallback() {

				@Override
				public Object doInSession(Session session) {
					for (M model : sourceList) {
						if (session.contains(model)) {
							continue;
						}
						session.save(model);
					}
					return null;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("发生了异常，事务开始回滚", e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public void batchUpdate(List<M> sourceList) {
		try {
			this.execute(new HibernateCallback() {

				@Override
				public Object doInSession(Session session) {
					/*
					 * Connection conn = session.connection(); String sql =
					 * "delete from "; conn.prepareStatement(sql );
					 */
					return null;
				}
			});
		} catch (Exception e) {
			logger.error("发生了异常，事务开始回滚", e);
			e.printStackTrace();
		}
	}

}
