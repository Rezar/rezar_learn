package cn.wenhao.framework.dao.factory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ModelDaoFactory {
	private ModelDaoFactory(){}
	
	private static final Properties prop = new Properties();
	private static final ModelDaoFactory factory = new ModelDaoFactory();
	
	static{
		InputStream in = ModelDaoFactory.class.getClassLoader().getResourceAsStream("ModelDao_factory.properties");
		try {
			prop.load(in);
		} catch (IOException e) {
			throw new RuntimeException("" + e);
		}
	}
	
	public static ModelDaoFactory getFactory(){
		return factory;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T create(Class<T> clazz){
		String simpleName = clazz.getSimpleName();
		System.out.println(simpleName);
		String value = prop.getProperty(simpleName);
		try {
			Class<?> dao = Class.forName(value);
			return (T) dao.newInstance();
		}catch (Exception e) {
			throw new RuntimeException("" + e);
		}
	}
}
