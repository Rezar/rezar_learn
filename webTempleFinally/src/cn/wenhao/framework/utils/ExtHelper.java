package cn.wenhao.framework.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class ExtHelper {

	public static <T, M> String getJsonFromList(long recordTotal,
			List<T> beanList, Class<M> clazz, String[] fieldNames)
			throws Exception, IllegalAccessException {
		TotalJson total = new TotalJson();
		total.setResults(recordTotal);

		List<T> newList = new ArrayList<T>(beanList.size());
		for (T t : beanList) {
			newList.add(proccess(t, clazz, fieldNames));
		}
		total.setItems(newList);
		// JsonConfig jsonConfig = new JsonConfig();
		/*
		 * // jsonConfig.addIgnoreFieldAnnotation(annotationClass); String[]
		 * excludes = {}; jsonConfig.setExcludes(excludes);
		 */
		JSONObject jsonArray = JSONObject.fromObject(total);
		return jsonArray.toString();
	}

	public static String getJsonFromList(List<?> beanList, String[] excludes) {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(excludes);
		JSONArray jsonArray = JSONArray.fromObject(beanList, jsonConfig);
		return jsonArray.toString();
	}

	@SuppressWarnings("unchecked")
	private static <T> T proccess(T object, Class<?> clazz, String[] fieldNames)
			throws InstantiationException, IllegalAccessException {
		List<String> fieldNameList = null;
		if (fieldNames != null && fieldNames.length != 0)
			fieldNameList = Arrays.asList(fieldNames);
		java.lang.reflect.Field[] fields = clazz.getDeclaredFields();
		Class<?> classOfObject = object.getClass();
		T newObject = (T) classOfObject.newInstance();
		for (Field field : fields) {
			field.setAccessible(true);
			String name = field.getName();
			Object value = field.get(object);
			if (fieldNameList != null && fieldNameList.contains(name)) {
				field.set(newObject, value);
			} else if (fieldNameList == null) {
				field.set(newObject, value);
			}
		}
		return newObject;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String getXmlFromList(long recordTotal, List<?> beanList,
			Class<?> clazz, String[] exclFields) {
		Total total = new Total();
		total.setResults(recordTotal);
		List results = new ArrayList();
		results.add(total);
		results.addAll(beanList);

		DomDriver domDriver = new DomDriver();
		XStream sm = new XStream(domDriver);
		for (String fieldName : exclFields)
			sm.omitField(clazz, fieldName);
		for (int i = 0; i < results.size(); i++) {
			Class c = results.get(i).getClass();
			String b = c.getName();
			String[] temp = b.split("\\.");
			sm.alias(temp[temp.length - 1], c);
		}
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
				+ sm.toXML(results);
		return xml;
	}

	/*public static void main(String[] args) throws IllegalAccessException,
			Exception {
		long recordTotal = 3;
		List<CateGoryModel> list = new ArrayList<CateGoryModel>();
		CateGoryModel e = new CateGoryModel();
		e.setId("123");
		e.setCname("图书");
		e.setOrder(2);
		e.setParent(null);
		list.add(e);

		CateGoryModel e1 = new CateGoryModel();
		e1.setId("456");
		e1.setCname("图书2");
		e1.setOrder(3);
		e1.setParent(e);
		list.add(e1);

		String[] fieldNames = { "childeCategory", "parent" };

		String result = getJsonFromList(recordTotal, list, CateGoryModel.class,
				fieldNames);

		// String result = getJsonFromList(list, fieldNames);
		System.out.println(result);
	}*/

}
