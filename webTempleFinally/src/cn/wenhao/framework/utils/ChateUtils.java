package cn.wenhao.framework.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@SuppressWarnings("restriction")
public class ChateUtils {

	private final static String DATE_FORMAT_PATTERN = "yyyy_MM_dd";
	private final static SimpleDateFormat simpleDateF;
	private final static String LINE_SEPARATOR = System
			.getProperty("line.separator");
	static {
		simpleDateF = new SimpleDateFormat(DATE_FORMAT_PATTERN);
	}

	/**
	 * 根据传送过来的文件文本，将其中的图片信息转换为： xxxx<img src="" />xxxx 此时，生成的数据格式为：<img
	 * src="messageAction_getMessage.do?fileName=xxx.png" /> 在获取该图片的时候，首先需要获取到
	 * 聊天双方的ID，然后通过一个唯一性算法来生成一个保存的目录,这个目录就 是保存了这个图片的目录 <font
	 * color="FF0000">哈哈哈费大幅度发大水发</font><br>
	 * <img src=
	 * "messageAction_getMessage.do?fileName=\2015_03_31\1427810213187_75_4391.png"
	 * />
	 * 
	 * @return
	 */
	public static String textToNoBaseImg(String text, File baseFilePath) {
		List<File> files = textToImgFile(text, baseFilePath);
		while (text.indexOf("<img src=\"data:image/png;base64,") != -1) {
			text = text.replaceAll("(.*)<img src=\".*\" *alt=\"\">(.*)",
					"$1<img src=\"F\" />$2");
		}
		int index = text.length();
		System.out.println(text);
		int i = files.size() - 1;
		while ((index = text.lastIndexOf("<img src=\"F\" />", index)) != -1) {
			String str1 = text.substring(0, index + 10);
			String str2 = text.substring(index + 11);
			String allPath = files.get(i).getAbsolutePath();
			int k = allPath.lastIndexOf(File.separator);
			allPath = allPath.substring(0, k) + "S" + allPath.substring(k + 1);
			int j = allPath.lastIndexOf(File.separator, k - 1);
			String path = allPath.substring(j + 1);
			// 这里 采用的是相对于 BasePath 的相对路径
			text = str1 + "message/messageAction_getMessageImg.do?filePath="
					+ path + str2;
			i--;
		}
		return text;
	}

	/**
	 * 根据两个用户的id来生成保存其聊天内容的目录，为聊天内容文本和聊天图片等文件保存的根目录
	 * 
	 * @param baseFilePath
	 * @param user1_id
	 * @param user2_id
	 * @return
	 * @throws IOException
	 */
	public static File getChatFileDir(File baseFilePath, String user1_id,
			String user2_id) throws IOException {
		BASE64Encoder encoder = new BASE64Encoder();
		String dirStr = user1_id + user2_id;
		byte[] bytes = dirStr.getBytes();
		String str = encoder.encode(bytes);
		File file = new File(baseFilePath, File.separator + str);
		if (file.exists()) {
			return file;
		}
		bytes = (user2_id + user1_id).getBytes();
		str = encoder.encode(bytes);
		file = new File(baseFilePath, File.separator + str);
		if (!file.exists()) {
			System.out.println(file.getAbsolutePath() + "不存在，正在创建！");
			file.mkdirs();
		}
		return file;
	}

	/**
	 * 获取聊天的文件
	 * 
	 * @param baseFilePath
	 * @param user1_id
	 * @param user2_id
	 * @return
	 * @throws IOException
	 */
	public static File getChatTextFile(File baseFilePath, String user1_id,
			String user2_id) throws IOException {
		File saveDir = getChatFileDir(baseFilePath, user1_id, user2_id);
		File chatFile = new File(saveDir, "chat.properties");
		if (!chatFile.exists()) {
			chatFile.createNewFile();
		}
		return chatFile;
	}

	/**
	 * 根据传输的文件文本，将其中的图片信息保存到服务器后，返回相对饮的图片文件对象
	 * 
	 * @param text
	 * @param imgFileBasePath
	 * @return
	 */
	public static ArrayList<File> textToImgFile(String text,
			File imgFileBasePath) {

		int[] start_end = getImgStart(text, 0);
		ArrayList<File> files = new ArrayList<File>();
		int index = 0;
		while (start_end != null) {
			File file = readAndOutput(text, start_end, imgFileBasePath);
			if (file != null) {
				files.add(file);
			}
			index = start_end[1];
			start_end = getImgStart(text, index);
		}
		return files;
	}

	/**
	 * 读取文本中的图片数据，然后写入到文件中去
	 * 
	 * @param text
	 * @param start_end
	 * @param imgFileBasePath
	 * @return
	 */
	private static File readAndOutput(String text, int[] start_end,
			File imgFileBasePath) {
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			String resource = text.substring(start_end[0], start_end[1]);
			File imgFile = getImgFile(start_end, imgFileBasePath);
			FileOutputStream out = new FileOutputStream(imgFile);
			byte[] decoderByte = decoder.decodeBuffer(resource);
			out.write(decoderByte);
			out.flush();
			out.close();
			System.out.println("图片文件保存完成" + imgFile);
			return imgFile;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取该图片文件应该保存的位置
	 * 
	 * @param start_end
	 * @param imgFileBasePath
	 * @return
	 * @throws IOException
	 */
	private static File getImgFile(int[] start_end, File imgFileBasePath)
			throws IOException {
		// 获取当前的日期信息，添加到文件名的保存路径中去，作为每天聊天内容的划分
		String dataStr = getData();
		File file = new File(imgFileBasePath, File.separator + "images"
				+ File.separator + dataStr + File.separator
				+ new Date().getTime() + "_" + start_end[0] + "_"
				+ start_end[1] + ".png");
		System.out.println(file.getAbsolutePath());
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			file.createNewFile();
		}
		return file;
	}

	/**
	 * 获取当前的日期信息
	 * 
	 * @return
	 */
	private static String getData() {
		return simpleDateF.format(new Date());
	}

	/**
	 * 获取每次图片保存的信息的文本的开始和结尾段位置
	 * 
	 * @param text
	 * @param begin
	 * @return
	 */
	private static int[] getImgStart(String text, int begin) {
		int[] ret = new int[2];

		int start = text.indexOf("<img src=\"data:image/png;base64,", begin);
		if (start == -1) {
			return null;
		}
		ret[0] = start + "<img src=\"data:image/png;base64,".length();
		int end = text.indexOf("\" alt=\"\">", start);
		ret[1] = end;
		if (ret[1] == -1)
			return null;
		return ret;
	}

	// public static void main(String[] args) throws Exception {
	//
	// File file = new File("c:\\imgTest.txt");
	// BufferedReader br = new BufferedReader(new InputStreamReader(
	// new FileInputStream(file), "utf-8"));
	// String line = "";
	// StringBuilder sb = new StringBuilder();
	// while ((line = br.readLine()) != null) {
	// sb.append(line).append("\n");
	// }
	// br.close();
	// File basePath = new File("c:/chat");
	// if (!basePath.exists())
	// basePath.mkdirs();
	//
	// String user1_id = "e278b2344c8-6e8d-416a-b855-67c7c099dacb";
	// String user2_id = "1243tr34";
	// File file2 = getChatFileDir(basePath, user1_id, user2_id);
	// File chatFile = getChatTextFile(basePath, user1_id, user2_id);
	// System.out.println(user1_id + "同" + user2_id + "的聊天文件为："
	// + chatFile.getAbsolutePath());
	//
	// File userChatFileDir = getChatFileDir(basePath, user1_id, user2_id);
	// String filePath = "2015_04_01S1427823906340_75_4391.png";
	// filePath = filePath.replace("S", File.separator);
	// File imgFile = new File(userChatFileDir, File.separator + "images"
	// + File.separator + filePath);
	// System.out.println(imgFile.getAbsolutePath());
	//
	// // saveChat(chatFile, user1_id, sb.toString());
	//
	// /*
	// * File fileImg = new File(file2, "images"); if (!fileImg.exists())
	// * fileImg.mkdirs();
	// *
	// * List<File> files = textToImgFile(sb.toString(), fileImg); for (File
	// * file_ : files) { System.out.println(file_.getAbsolutePath()); }
	// *
	// * System.out.println(textToNoBaseImg(sb.toString(), fileImg,
	// * "web/user_id/")); System.out.println(file.getAbsolutePath());
	// *
	// * String[] context = getContext(chatFile,user1_id,user2_id, 20, 37);
	// * System.out.println(context[2].toString());
	// */
	// }

	/**
	 * 获取用户的聊天记录(从起始行到结束行)
	 * 
	 * @param chatFile
	 * @param user_id
	 *            : 当前用户的id
	 * @param userOther_id
	 *            :另外的用户ID
	 * @param begin
	 * @param end
	 * @return
	 * @throws FileNotFoundException
	 */
	public static String[] getContext(File chatFile, String user_id,
			String userOther_id, int begin, int end) throws Exception {
		synchronized (DATE_FORMAT_PATTERN) {
			Reader reader = new FileReader(chatFile);
			StringBuilder sb = new StringBuilder();
			LineNumberReader lnr = new LineNumberReader(reader);

			int allCount = 0;
			String line = null;
			while ((line = lnr.readLine()) != null) {
				allCount++;
			}
			lnr.close();

			Reader reader2 = new FileReader(chatFile);
			LineNumberReader lnr2 = new LineNumberReader(reader2);
			for (int i = 0; i < end; i++) {
				lnr2.readLine();
			}
			for (int i = end; i < allCount; i++) {
				line = lnr2.readLine();
				readAndSetChatContext(user_id, userOther_id, sb, line);
			}

			end = allCount;
			/*
			 * if (begin == 0 && end == 0) { String line = ""; int i = 0; while
			 * ((line = lnr.readLine()) != null && i < 20) {
			 * readAndSetChatContext(user_id, userOther_id, sb, line); } begin =
			 * 0; begin = 20; } else { for (int i = 0; i < end; i++) {
			 * lnr.readLine(); } begin = end; String line = ""; int i; for (i =
			 * end; (line = lnr.readLine()) != null; i++) {
			 * readAndSetChatContext(user_id, userOther_id, sb, line); } end =
			 * i; System.out.println("begin" + begin + ",end" + end); }
			 */
			lnr2.close();
			return new String[] { end + "", sb.toString() };
		}
	}

	private static void readAndSetChatContext(String user_id,
			String userOther_id, StringBuilder sb, String line) {
		if (line.startsWith(user_id)) {
			sb.append(getUserLi(line.substring(user_id.length()), true));
		} else {
			sb.append(getUserLi(line.substring(userOther_id.length()), false));
		}
	}

	/**
	 * 返回聊天记录，当前用户的聊天记录标记为class='one'
	 * 
	 * @param line
	 * @param isFirst
	 * @return
	 */
	private static Object getUserLi(String line, boolean isFirst) {
		StringBuilder sb = new StringBuilder();
		if (isFirst)
			sb.append("<li class=\"one\">").append(line).append("</li>");
		else
			sb.append("<li class=\"two\">").append(line).append("</li>");
		return sb.toString();
	}

	/**
	 * 保存用户的聊天记录
	 * 
	 * @param chatFile
	 * @param user1_id
	 * @param context
	 * @throws IOException
	 */
	public static void saveChat(File chatFile, String user1_id, String context)
			throws IOException {
		context = textToNoBaseImg(context, chatFile.getParentFile());
		synchronized (DATE_FORMAT_PATTERN) {
			PrintWriter pw = new PrintWriter(new FileWriter(chatFile, true),
					true);
			pw.print(user1_id + context + LINE_SEPARATOR);
			pw.flush();
			pw.close();
		}
	}

}
