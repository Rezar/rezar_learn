package cn.wenhao.framework.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import cn.wenhao.framework.codeUtils.codeMakerUtils.CodeMarkerUtils;
import cn.wenhao.framework.codeUtils.generaUtils.GeneralUtils;

public class XmlUtils {

	private static String filepath;

	public static Document getDocumentInClass(String xmlPath) throws Exception {
		filepath = GeneralUtils.getWebDefaultTruePath(xmlPath);
		System.out.println(filepath);
		SAXReader reader = new SAXReader();
		Document document = reader.read(new File(filepath));
		return document;
	}

	public static Document getDocumentInConfig(String xmlPath) throws Exception {
		filepath = GeneralUtils.getWebDefaultTruePath(xmlPath);
		System.out.println(filepath);
		String srcPath = CodeMarkerUtils.getSrcPath(new File(filepath));
		System.out.println(" A srcPath is :" + srcPath);
		return getDocument(srcPath);
	}

	public static Document getDocument(String srcPath) throws DocumentException {
		SAXReader reader = new SAXReader(false);
		Document document = reader.read(new File(srcPath));
		return document;
	}

	/**
	 * 获取一个XML文档的Document对象，在忽略DTD文件校验的情况下
	 * 
	 * @param path
	 * @return
	 * @throws DocumentException
	 */
	public static Document getDocumentIgnoreDtD(String path)
			throws DocumentException {

		SAXReader reader = new SAXReader(false);
		// 忽略DTD文件的校验
		reader.setEntityResolver(new IgnoreDTDEntityResolver());
		Document document = reader.read(new File(path));

		return document;
	}

	public static void write2XmlInClass(Document document, String xmlPath)
			throws IOException {
		filepath = GeneralUtils.getWebDefaultTruePath(xmlPath);
		writeToXml(document, filepath);
	}

	public static void write2XmlInConfig(Document document, String xmlPath)
			throws IOException {
		filepath = GeneralUtils.getWebDefaultTruePath(xmlPath);
		String srcPath = CodeMarkerUtils.getSrcPath(new File(filepath));
		System.out.println("srcPath is :" + srcPath);
		writeToXml(document, srcPath);
	}

	public static void writeToXml(Document document, String filePath)
			throws UnsupportedEncodingException, FileNotFoundException,
			IOException {
		OutputFormat format = OutputFormat.createPrettyPrint();
		format.setEncoding("UTF-8");
		XMLWriter writer = new XMLWriter(new FileOutputStream(filePath), format);
		writer.write(document);
		writer.close();
	}

}
