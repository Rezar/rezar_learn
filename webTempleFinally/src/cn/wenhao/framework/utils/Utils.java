package cn.wenhao.framework.utils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.misc.BASE64Encoder;
import cn.wenhao.framework.codeUtils.annotation.NotHql;

@SuppressWarnings("restriction")
public class Utils {

	private final static String DATE_FORMAT_PATTERN = "yyyy年MM月dd日 E kk点mm分";
	private final static SimpleDateFormat simpleDateF;

	static {
		simpleDateF = new SimpleDateFormat(DATE_FORMAT_PATTERN);
	}

	private Utils() {
	}

	/*
	 * public static <T> T form2Bean(HttpServletRequest req, Class<T> clazz) {
	 * try{ T bean = clazz.newInstance(); Map map = req.getParameterMap();
	 * BeanUtils.populate(bean, map); return bean; }catch(Exception e){ throw
	 * new RuntimeException("Form2Bean fail:" + e); } }
	 */

	public static <T> T myForm2Bean(Map<String, String[]> params, Class<T> clazz) {
		try {
			T bean = clazz.newInstance();
			Set<Map.Entry<String, String[]>> entrys = params.entrySet();
			for (Map.Entry<String, String[]> entry : entrys) {
				String name = entry.getKey();
				String value = (String) entry.getValue()[0];
				System.out.println("key:" + name + "-----value:" + value);
				try {
					Field field = clazz.getDeclaredField(name);
					field.setAccessible(true);
					field.set(bean, value);
				} catch (Exception e) {
					System.out.println(name + "没有找到");
					continue;
				}
			}
			return bean;
		} catch (Exception e) {
			throw new RuntimeException("Form2Bean fail:" + e);
		}
	}

	/**
	 * 将真实值的时间数据转换成表示值的数据模式
	 * 
	 * @param time
	 * @return
	 */
	public static String long2Date(Long time) {
		Timestamp tt = new Timestamp(time);
		return tt.toString();
	}

	/**
	 * 将表示值的数据模式转换为真实值的数据模式
	 */
	public static Long date2Long(String timeStr) {
		SimpleDateFormat sdf = new SimpleDateFormat();
		try {
			Date date = sdf.parse(timeStr);
			return date.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new Long(0);
	}

	/**
	 * 通过原密码获取记过Base64编码的安全密码
	 * 
	 * @param password
	 * @return
	 */
	public static String getSafePassword(String password) {
		try {
			MessageDigest md = MessageDigest.getInstance("md5");
			byte[] buff = md.digest(password.getBytes());
			BASE64Encoder encoder = new BASE64Encoder();
			return encoder.encode(buff);
		} catch (NoSuchAlgorithmException e) {
			return password;
		}
	}

	/**
	 * 获取UUID
	 * 
	 * @return
	 */
	public static String uuid() {
		return UUID.randomUUID().toString();
	}

	/**
	 * 通过邮箱的地址找到其邮箱的服务器
	 * 
	 * @param email
	 * @return
	 */
	public static String getSmtpHost(String email) {
		String host = email.substring(email.indexOf("@") + 1);
		String realMailType = host.substring(0, host.indexOf("."));
		System.out.println(realMailType);
		Properties prop = new Properties();
		try {
			prop.load(Utils.class.getClassLoader().getResourceAsStream(
					"mailType.properties"));
			String smtpHost = prop.getProperty(realMailType);
			return smtpHost;
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 判断某个字符创是否为空
	 * 
	 * @param str
	 * @return 字符串不为空且不是空字符串的时候，返回true,否则返回false;
	 */
	public static boolean notNullAndNotEmpty(String str) {
		if (str != null && !str.equals("")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 判断一个对象中的某个属性是否为普通的属性，且判断该属性是否为默认值
	 * 
	 * @param field
	 * @param qm
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static boolean isNotNull(Field field, Object qm)
			throws IllegalArgumentException, IllegalAccessException {
		boolean notNull = true;
		Class<?> type = field.getType();
		if (type == int.class) {
			notNull = ((Integer) field.get(qm) == 0 ? false : true);
		}
		if (type == double.class) {
			notNull = ((Double) field.get(qm) == 0.0 ? false : true);
		}
		if (type == float.class) {
			notNull = ((Float) field.get(qm) == 0.0 ? false : true);
		}
		if (type == boolean.class) {
			notNull = ((Boolean) field.get(qm) == false ? false : true);
		}
		if (type == char.class) {
			notNull = ((Character) field.get(qm) == '\u0000' ? false : true);
		}
		if (type == byte.class) {
			notNull = ((Byte) field.get(qm) == 0 ? false : true);
		}
		if (type == short.class) {
			notNull = ((Short) field.get(qm) == 0 ? false : true);
		}
		return notNull;
	}

	/**
	 * 获取某个对象中的不是空值的属性的值
	 * 
	 * @param qm
	 * @return
	 * @throws Exception
	 */
	public static List<Field> getAllNotNullField(Object qm) throws Exception {
		ArrayList<Field> notNull = new ArrayList<Field>();
		Field[] allField = qm.getClass().getDeclaredFields();
		if (allField.length == 0) {
			allField = qm.getClass().getSuperclass().getDeclaredFields();
		}
		if (allField.length == 0) {
			throw new Exception("无法获取到属性的数组！");
		}
		for (Field field : allField) {
			field.setAccessible(true);
			// 此时为私有静态，不是目标字段
			if (field.getModifiers() == 26) {
				continue;
			}
			if (!isNotNull(field, qm)) {
				continue;
			}

			// 判断该字段是否有不需要参与HQL语句的注解
			NotHql nh = field.getAnnotation(NotHql.class);
			if (nh != null) {
				continue;
			}

			Object value = null;
			try {
				value = field.get(qm);
				if (value != null) {
					notNull.add(field);
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return notNull;
	}

	/**
	 * 页面请求跳转
	 * 
	 * @param req
	 * @param string
	 * @throws IOException
	 * @throws ServletException
	 */
	public static void forward(HttpServletRequest req,
			HttpServletResponse resp, String uri) throws ServletException,
			IOException {
		if (notNullAndNotEmpty(uri)) {
			req.getRequestDispatcher(uri).forward(req, resp);
		}
	}

	/**
	 * 页面请求跳转
	 * 
	 * @param req
	 * @param string
	 * @throws IOException
	 */
	public static void sendRedirect(HttpServletResponse resq, String url)
			throws IOException {
		if (notNullAndNotEmpty(url)) {
			resq.sendRedirect(url);
		}
	}

	/**
	 * 对日期进行格式化成字符串
	 * 
	 * @param date
	 * @return
	 */
	public static String DateFormatToString(Date date) {
		return simpleDateF.format(date);
	}

	/**
	 * 将字符串转为指定格式的Date对象
	 * 
	 * @param dateStr
	 * @return
	 * @throws ParseException
	 */
	public static Date StringFormatToDate(String dateStr) throws ParseException {
		return simpleDateF.parse(dateStr);
	}

}
