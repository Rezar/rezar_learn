package cn.wenhao.framework.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * 字符串工具类
 * 
 * @author Administrator
 * 
 */
public class StringUtils extends antlr.StringUtils {

	/**
	 * 比较两个字符串，将字符排序在前的字符串放在前面，用连接符连接起来返回结果
	 * 
	 * @param otherModelName
	 * @param modelSimpleName
	 * @param linkStr
	 *            指定的连接符
	 * @return
	 */
	public static String sortString(String otherModelName,
			String modelSimpleName, String linkStr) {

		int result = otherModelName.compareTo(modelSimpleName);
		if (result < 0) {
			return otherModelName + linkStr + modelSimpleName;
		} else {
			return modelSimpleName + linkStr + otherModelName;
		}
	}

	/**
	 * 将文件路径的URL格式(中文路径会进行BASE64编码)的路径进行解码
	 * 
	 * @param urlPath
	 * @return
	 */
	public static String decodeString(String urlPath) {
		try {
			urlPath = URLDecoder.decode(urlPath, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return urlPath;
	}

}
