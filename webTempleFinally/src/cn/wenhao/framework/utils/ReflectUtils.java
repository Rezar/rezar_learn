package cn.wenhao.framework.utils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * 反射工具类
 * 
 * @author Administrator
 * 
 */
public class ReflectUtils {

	/**
	 * 获取一个Set集合中的泛型类型
	 * 
	 * @param field
	 * @return
	 */
	public static Class<?> getGenericClass(Field field) {

		Class<?> fieldClass = field.getType(); //  得到field的class及类型全路径
		if (fieldClass.isAssignableFrom(HashSet.class)
				|| fieldClass.isAssignableFrom(Set.class)
				|| fieldClass.isAssignableFrom(List.class)
				|| fieldClass.isAssignableFrom(ArrayList.class)
				|| fieldClass.isAssignableFrom(LinkedList.class)) {
			Type fc = field.getGenericType();
			if (fc == null) {
				return null;
			}
			if (fc instanceof ParameterizedType) {
				ParameterizedType pt = (ParameterizedType) fc;
				Class<?> genericClass = (Class<?>) pt.getActualTypeArguments()[0];
				return genericClass;
			}
		}
		return null;
	}

}
