package cn.wenhao.framework.utils;

import java.util.List;

public class TotalJson {

	private long results;
	@SuppressWarnings("rawtypes")
	private List items;

	public long getResults() {
		return results;
	}

	public void setResults(long results) {
		this.results = results;
	}

	@SuppressWarnings("rawtypes")
	public List getItems() {
		return items;
	}

	@SuppressWarnings("rawtypes")
	public void setItems(List items) {
		this.items = items;
	}

}
