package cn.wenhao.framework.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 文件操作的工具类
 * 
 * @author Administrator
 * 
 */
public class FileUtils {

	/**
	 * 文件复制
	 * 
	 * @param sourseFile
	 * @param targetFile
	 * @throws IOException
	 */
	public static void copyFile(File sourseFile, File targetFile)
			throws IOException {
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		try {
			bis = new BufferedInputStream(new FileInputStream(sourseFile));
			bos = new BufferedOutputStream(new FileOutputStream(targetFile));
			int length = 0;
			while ((length = bis.read()) != -1) {
				bos.write(length);
			}
			bos.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeInputStream(bis);
			closeOutputStream(bos);
		}
	}

	/**
	 * 关闭输出流
	 * 
	 * @param bos
	 */
	public static void closeOutputStream(OutputStream out) {
		try {
			if (out != null)
				out.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("关闭输出流失败！");
		}
	}

	/**
	 * 关闭输入流
	 * 
	 * @param bis
	 */
	public static void closeInputStream(InputStream in) {
		try {
			if (in != null)
				in.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("关闭输入流失败！");
		}
	}

}
