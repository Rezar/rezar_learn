package cn.wenhao.framework.interfaceCallback;

import org.hibernate.Session;

public interface HibernateCallback {
	
	Object doInSession(Session session);

}
