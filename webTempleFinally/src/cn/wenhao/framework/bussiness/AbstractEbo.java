package cn.wenhao.framework.bussiness;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import cn.wenhao.framework.dao.Dao;
import cn.wenhao.framework.model.Model;

public abstract class AbstractEbo<M extends Model, QM extends M> implements
		Ebi<M, QM> {
	public Dao<M, QM> dao;

	@Required
	public void setDao(Dao<M, QM> dao) {
		this.dao = dao;
	}

	@Override
	public boolean create(M model) {
		return dao.create(model);
	}

	@Override
	public boolean delete(Serializable id) {
		return dao.delete(id);
	}

	@Override
	public boolean update(M model) {
		return dao.update(model);
	}

	@Override
	public M getSingle(Serializable id) {
		return dao.getSingle(id);
	}

	@Override
	public M getSingle(QM qm) {
		return dao.getSingle(qm);
	}

	@Override
	public int getAll() {
		return dao.getAll();
	}

	@Override
	public int getByConditionCount(QM qm) {
		return dao.getByConditionCount(qm);
	}

	@Override
	public List<M> getAll(int firstIndex, int maxRecords) {
		return dao.getAll(firstIndex, maxRecords);
	}

	@Override
	public List<M> getByCondition(QM qm, int firstIndex, int maxRecords) {
		return dao.getByCondition(qm, firstIndex, maxRecords);
	}

	@Override
	public List<M> getByCondition(QM qm, int firstIndex) {
		return dao.getByCondition(qm, firstIndex);
	}

}
