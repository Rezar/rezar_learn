package cn.wenhao.framework.bussiness;

import java.io.Serializable;
import java.util.List;

import cn.wenhao.framework.model.Model;

public interface Ebi<M extends Model, QM extends M> {

	/**
	 * 增
	 * 
	 * @param model
	 * @return
	 */
	public boolean create(M model);

	/**
	 * 删
	 * 
	 * @param model
	 * @return
	 */
	public boolean delete(Serializable id);

	/**
	 * 根据一般检索条件来查询单个对象
	 */
	public M getSingle(QM qm);

	/**
	 * 改
	 * 
	 * @param model
	 * @return
	 */
	public boolean update(M model);

	/**
	 * 查
	 * 
	 * @param id
	 * @return
	 */
	public M getSingle(Serializable id);

	/**
	 * 获取所有的记录 返回 -1 代表出错
	 */
	public int getAll();

	/**
	 * 获取记录总数，根据查询条件
	 * 
	 * @param qm
	 * @return
	 */
	public int getByConditionCount(QM qm);

	/**
	 * 查询分页
	 * 
	 * @param firstIndex
	 * @param maxRecords
	 * @return
	 */
	public List<M> getAll(int firstIndex, int maxRecords);

	/**
	 * 根据查询条件进行分页的查询
	 * 
	 * @param qm
	 * @param firstIndex
	 * @param maxRecords
	 * @return
	 */
	public List<M> getByCondition(QM qm, int firstIndex, int maxRecords);

	/**
	 * 查询特定情况下的所有记录
	 * 
	 * @param qm
	 * @param firstIndex
	 * @return
	 */
	public List<M> getByCondition(QM qm, int firstIndex);

}
