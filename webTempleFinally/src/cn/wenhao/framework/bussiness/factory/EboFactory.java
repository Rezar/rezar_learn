package cn.wenhao.framework.bussiness.factory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class EboFactory {
	private EboFactory(){}
	
	private static final Properties prop = new Properties();
	private static final EboFactory factory = new EboFactory();
	
	static{
		InputStream in = EboFactory.class.getClassLoader().getResourceAsStream("ebo_factory.properties");
		try {
			prop.load(in);
		} catch (IOException e) {
			throw new RuntimeException("" + e);
		}
	}
	
	public static EboFactory getFactory(){
		return factory;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T create(Class<T> clazz){
		String simpleName = clazz.getSimpleName();
		System.out.println(simpleName);
		String value = prop.getProperty(simpleName);
		try {
			Class<?> dao = Class.forName(value);
			return (T) dao.newInstance();
		}catch (Exception e) {
			throw new RuntimeException("" + e);
		}
	}
}
