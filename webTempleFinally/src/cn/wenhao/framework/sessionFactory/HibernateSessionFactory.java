package cn.wenhao.framework.sessionFactory;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 使用spring管理该对象的时候，由于spring中的bean对象都是单例的，所以在整个容器中都只有一个该对象的实例
 * 
 * @author Administrator
 * 
 */
public class HibernateSessionFactory implements ApplicationContextAware {

	private SessionFactory sessionFactory;
	private ApplicationContext applicationContext;
	private ThreadLocal<Session> sessionLocal = new ThreadLocal<Session>();

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	// 私有构造方法，防止用户自己创建该类的对象
	private HibernateSessionFactory() {

	}

	// 获取session对象
	public Session getSession() {
		System.out.println(sessionFactory);
		Session session = null;
		try {
			session = this.sessionFactory.getCurrentSession();
			sessionLocal.set(session);
		} catch (HibernateException e) {
			// e.printStackTrace();
			System.out.println("当前线程上没有绑定Session!");
			session = this.sessionFactory.openSession();
			sessionLocal.set(session);
		}
		if (session == null) {
			// 如果sessionFactory对象为空，说明此时的sessionFactory对象没有进行初始化
			if (sessionFactory == null) {
				rebuildSessionFactory();
			}
			session = this.sessionFactory.openSession();
			sessionLocal.set(session);
			session = (sessionFactory != null) ? session : null;
		}
		return session;
	}

	private void rebuildSessionFactory() {
		try {
			sessionFactory = (SessionFactory) this.applicationContext
					.getBean("sessionFactory");
		} catch (Exception e) {
			System.out.println("创建sessionFactory对象失败！");
			e.printStackTrace();
		}
	}

	public void closeSession() {
		Session session = null;
		try {
			session = this.sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			// e.printStackTrace();
			System.out.println("关闭连接的时候，当前线程上没有绑定Session!");
			session = sessionLocal.get();
		}finally{
			sessionLocal.remove();
		}
		if (session != null) {
			session.close();
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		this.applicationContext = arg0;
	}
}
