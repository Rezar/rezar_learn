package cn.wenhao.framework.model;

public class Expression {

	private String ename;
	private String operator = "=";
	private Object value;

	public String getEname() {
		return ename;
	}
	
	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
	
	/**
	 * index = 1 : 设置的为 this.ename;
	 * index = 2 : 设置的为this.operator
	 * index = 3 : 设置的为this.value
	 * @param index ：需要进行初始化的属性的下标
	 * @param value	:需要进行为指定属性设置的值
	 */
	public Expression(int index , Object value){
		if(index == 1){
			this.ename = (String) value;
		}else if(index == 2){
			this.operator = (String) value;
		}else{
			this.value = value;
		}
	}
	

	public Expression(String ename, String operator, Object value) {
		this.ename = ename;
		this.operator = operator;
		this.value = value;
	}

	public Expression() {
	}

	@Override
	public String toString() {
		return "Expression [ename=" + ename + ", operator=" + operator
				+ ", value=" + value + "]";
	}

}
