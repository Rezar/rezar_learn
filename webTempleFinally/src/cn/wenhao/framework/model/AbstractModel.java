package cn.wenhao.framework.model;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * 这个抽象类可以放置所有的Model共同的成员属性
 * 
 * @author Administrator
 * 
 */
@SuppressWarnings("serial")
public abstract class AbstractModel implements Model {

	@Test
	public void autoCreateCurrentModel() {
		cn.wenhao.framework.codeUtils.codeMakerUtils.AutoCreate
				.autoCurrentModel(this);
	}

	/*private void collectionAllField(Class<?> class1, List<Field> col) {
		col.addAll(Arrays.asList(class1.getDeclaredFields()));
		if (class1.getSuperclass() != Object.class) {
			collectionAllField(this.getClass().getSuperclass(), col);
		}
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		Class currentclazz = this.getClass();
		List<Field> allField = new ArrayList<Field>();
		collectionAllField(this.getClass(), allField);
		sb.append(this.getClass().getSimpleName()).append(": ");
		for (Field field : allField) {
			final String fieldName = field.getName();
			field.setAccessible(true);
			Object value = null;
			try {
				value = field.get(this);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			sb.append(fieldName).append("=").append(value).append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}*/

	public Map<String, Expression> expressions = new HashMap<String, Expression>(
			0);

}
