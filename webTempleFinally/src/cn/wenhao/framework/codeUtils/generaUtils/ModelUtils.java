package cn.wenhao.framework.codeUtils.generaUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import cn.wenhao.framework.codeUtils.codeMakerUtils.CodeMarkerUtils;
import cn.wenhao.framework.codeUtils.codeMakerUtils.XmlFileMarkerUtils;
import cn.wenhao.framework.model.Model;

/**
 * 对Model对象的一些名称操作
 * 
 * @author Administrator
 * 
 */
public class ModelUtils {

	private static Properties prop = new Properties();
	static {

		InputStream in = CodeMarkerUtils.class.getClassLoader()
				.getResourceAsStream(CUtils.PROP_FILE_NAME);
		try {
			prop.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 获取到Model对象的src路径下的XML文件存放位置
	 * 
	 * @param model
	 * @return
	 */
	public static String srcXmlSavePath(Model model) {
		File[] xmlSaveFile = XmlFileMarkerUtils.createNewXml(model, true);
		System.out.println(model.getClass().getName() + "的src路径下的XML文件存放在："
				+ xmlSaveFile[0].getAbsolutePath());
		return xmlSaveFile[0].getAbsolutePath();
	}

	/**
	 * 获取到Model对象的class路径下的XML文件存放位置
	 * 
	 * @param model
	 * @return
	 */
	public static String classXmlSavePath(Model model) {
		File[] xmlSaveFile = XmlFileMarkerUtils.createNewXml(model, true);
		System.out.println(model.getClass().getName() + "的Class路径下的XML文件存放在："
				+ xmlSaveFile[1].getAbsolutePath());
		return xmlSaveFile[1].getAbsolutePath();
	}

	/**
	 * 获取当前Model对象基于web项目的实际路径
	 * 
	 * @param model
	 * @return
	 */
	public static String getWebClassPath(Model model) {
		return prop.getProperty("classPath");
	}

	/**
	 * 获取Model对象所在的包的全限定名
	 * 
	 * @param model
	 * @return
	 */
	public static String getmodelPath(Model model) {
		Package package_ = model.getClass().getPackage();
		return package_.getName();
	}

	/**
	 * 获取Model对象的全限定名
	 * 
	 * @param model
	 * @return
	 */
	public static String getModelFullName(Model model) {
		String fullName = model.getClass().getName();
		return fullName;
	}

	/**
	 * 获取Model的QUeryModel对象的简单名
	 * 
	 * @param model
	 * @return
	 */
	public static String getQueryModelSimpleName(Model model) {
		String modelFullName = model.getClass().getSimpleName();
		int index = modelFullName.indexOf("Model");
		return modelFullName.substring(0, index) + "QueryModel";
	}

	/**
	 * 获取某个Model对象的QUeryModel对象,全限定名
	 * 
	 * @param model
	 * @return
	 */
	public static String getQuerymodelFullName(Model model) {
		String queryModelSavePath = prop.getProperty("queryModel.savePath");
		String queryModel = getQueryModelSimpleName(model);
		return queryModelSavePath + "." + queryModel;
	}

	/**
	 * 获取Model对象的Dao,全限定名
	 * 
	 * @param model
	 * @return
	 */
	public static Object getModelDaoInterface(Model model) {
		String daoSavePath = prop.getProperty("dao.savePath");
		String modelName = model.getClass().getSimpleName();
		return daoSavePath + "." + modelName + "Dao";
	}

	/**
	 * 获取Model对象的DaoImpl,全限定名
	 * 
	 * @param model
	 * @return
	 */
	public static String getModelDaoImplFullName(Model model) {
		String daoImplSavePath = prop.getProperty("daoImpl.savePath");
		String modelName = model.getClass().getSimpleName();
		return daoImplSavePath + "." + modelName + "DaoImpl";
	}

	/**
	 * 获取Model对象的简单名称
	 * 
	 * @param model
	 * @return
	 */
	public static String getModelSimpleName(Model model) {
		return model.getClass().getSimpleName();
	}

	/**
	 * 获取Model对象的Dao的简单名称
	 * 
	 * @param model
	 * @return
	 */
	public static String getModelIDaoSimpleName(Model model) {
		String modelName = getModelSimpleName(model);
		return modelName + "Dao";
	}

	/**
	 * 获取Model对象的DaoImpl的简单名
	 * 
	 * @param model
	 * @return
	 */
	public static String getModelDaoImplSimleName(Model model) {
		String modelName = getModelSimpleName(model);
		return modelName + "DaoImpl";
	}

	/**
	 * 获取Ebi的简单名
	 * 
	 * @param model
	 * @return
	 */
	public static String getEbiSimpleName(Model model) {
		String modelFullName = model.getClass().getSimpleName();
		int index = modelFullName.indexOf("Model");
		return modelFullName.substring(0, index) + "Ebi";
	}

	/**
	 * 获取Ebi的全限定名
	 * 
	 * @param model
	 * @return
	 */
	public static String getEbiFullName(Model model) {
		String ebiSavePath = prop.getProperty("ebi.savePath");
		return ebiSavePath + "." + getEbiSimpleName(model);
	}

	/**
	 * 获取Ebo的简单名
	 * 
	 * @param model
	 * @return
	 */
	public static String getSimpleEboName(Model model) {
		String modelFullName = model.getClass().getSimpleName();
		int index = modelFullName.indexOf("Model");
		return modelFullName.substring(0, index) + "Ebo";
	}

	/**
	 * 获取Ebo的全限定名
	 * 
	 * @param model
	 * @return
	 */
	public static String getFullEboName(Model model) {
		String eboSavePath = prop.getProperty("ebo.savePath");
		String eboSimpleName = getSimpleEboName(model);
		return eboSavePath + "." + eboSimpleName;
	}

}
