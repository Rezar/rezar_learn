package cn.wenhao.framework.codeUtils.generaUtils;

import java.io.File;

/**
 * 程序中可能使用的到的系统的一些系统属性
 * 
 * @author Administrator
 * 
 */
public class CUtils {

	/* 当前系统的行分隔符 */
	public static final String LINE_SEPARATOR = System
			.getProperty("line.separator");

	/* 当前系统的文件路径分隔符 */
	public static final String FILE_SEPARATOR = File.separator;

	/* 配置文件的保存文件名 */
	public static final String PROP_FILE_NAME = "classSavePath.properties";

	/* 配置文件的保存文件名 */
	public static final String MODE_CHANGE_INFO = "modelInfos.properties";

}
