package cn.wenhao.framework.codeUtils.generaUtils;

import cn.wenhao.framework.utils.StringUtils;

public class GeneralUtils {
	

	/**
	 * 获取Web项目相对于文件系统的绝对起始路径
	 * 
	 * @return
	 */
	public static String getWebDefaultTruePath() {

		String classPath = GeneralUtils.class.getResource("/").getPath();
		classPath = StringUtils.decodeString(classPath);
		return classPath;
	}
	
	/**
	 * 获取某个文件在Web项目相对于文件系统的绝对起始路径
	 * 
	 * @return
	 */
	public static String getWebDefaultTruePath(String fileName) {

		String classPath = GeneralUtils.class.getClassLoader()
				.getResource(fileName).getPath();
		classPath = StringUtils.decodeString(classPath);
		return classPath;
	}

}
