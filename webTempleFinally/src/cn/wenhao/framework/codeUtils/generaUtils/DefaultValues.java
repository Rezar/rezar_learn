package cn.wenhao.framework.codeUtils.generaUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DefaultValues implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8357021900792454326L;
	private static final Map<String, String> defaultValue = new HashMap<String, String>();
	static {

		defaultValue.put("name", "");
		defaultValue.put("unsaved_value", "");
		defaultValue.put("check", "");
		defaultValue.put("default_", "");
		defaultValue.put("param", "");
		defaultValue.put("sql_type", "");
		defaultValue.put("class_", "");
		defaultValue.put("index", "");
		defaultValue.put("order_by", "");
		defaultValue.put("length", "255");
		defaultValue.put("unique_key", "");
		defaultValue.put("precision", "");
		defaultValue.put("not_null", "false");
		defaultValue.put("unique", "false");
		defaultValue.put("type", "");
		defaultValue.put("table", "");
		defaultValue.put("access", "");
		defaultValue.put("column", "");
		defaultValue.put("formula", "");
		defaultValue.put("generated", "");
		defaultValue.put("lazy", "false");
		defaultValue.put("node", "");
		defaultValue.put("scale", "");
		defaultValue.put("cascade", "");
		defaultValue.put("fetch", "");
		defaultValue.put("insert", "true");
		defaultValue.put("inverse", "false");
		defaultValue.put("update", "true");
		defaultValue.put("optimistic_lock", "true");
	}

	public static String getDefault(String key) {
		return defaultValue.get(key);
	}

	public static final String GENERATOR_ASSIGNED = "assigned";
	public static final String GENERATOR_INCREMENT = "increment";
	public static final String GENERATOR_SEQUENCE = "sequence";
	public static final String GENERATOR_UUID = "uuid";
	public static final String GENERATOR_GUID = "guid";
	public static final String GENERATOR_NATIVE = "native";
	// 程序判断是否需要自动创建注解的默认值的标示符
	public static final String MAKE_IDENTIFIE;
	static {
		MAKE_IDENTIFIE = serialVersionUID + "GENERATOR_NATIVE";
	}

	/*
	 * public static final String unsaved_value = "";
	 * 
	 * public static final String check = "";
	 * 
	 * public static final String default_ = "";
	 * 
	 * public static final String sql_type = "";
	 * 
	 * public static final String index = "";
	 * 
	 * public static final int length = 255;
	 * 
	 * public static final String unique_key = "";
	 * 
	 * public static final String precision = "";
	 * 
	 * public static final boolean not_null = true;
	 * 
	 * public static final boolean unique = false;
	 * 
	 * public static final String type = "";
	 * 
	 * public static final String access = "";
	 * 
	 * public static final String column = "";
	 * 
	 * public static final String formula = "";
	 * 
	 * public static final String generated = "";
	 * 
	 * public static final boolean lazy = false;
	 * 
	 * public static final String node = "";
	 * 
	 * public static final String scale = "";
	 * 
	 * public static final boolean insert = true;
	 * 
	 * public static final boolean update = true;
	 * 
	 * public static final boolean optimistic_lock = true;
	 */

}
