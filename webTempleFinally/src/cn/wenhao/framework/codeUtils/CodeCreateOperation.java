package cn.wenhao.framework.codeUtils;

import java.io.IOException;

import org.junit.Test;

import cn.wenhao.framework.codeUtils.codeMakerUtils.AutoCreate;
import cn.wenhao.framework.codeUtils.codeMakerUtils.PropertiesFileUtils;
import cn.wenhao.framework.codeUtils.generaUtils.CUtils;

public class CodeCreateOperation {

	/**
	 * 更新指定路径下的所有的Model类的信息
	 */
	@Test
	public void updateAll() {
		try {
			PropertiesFileUtils.setFileFirst();
			AutoCreate.autoCreate(true);
		} catch (IOException e) {
			System.out.println("无法更新配置文件：" + CUtils.PROP_FILE_NAME);
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 更新指定路径下的有些Model类的信息，此类应该是添加上了JustThisClass的注解
	 */
	@Test
	public void updateSome() {
		try {
			PropertiesFileUtils.setFileFirst();
			AutoCreate.autoCreate(false);
		} catch (IOException e) {
			System.out.println("无法更新配置文件：" + CUtils.PROP_FILE_NAME);
			e.printStackTrace();
		}
	}

}
