package cn.wenhao.framework.codeUtils.annotationUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;

import cn.wenhao.framework.codeUtils.generaUtils.DefaultValues;
import cn.wenhao.framework.codeUtils.generaUtils.ModelUtils;
import cn.wenhao.framework.model.Model;
import cn.wenhao.framework.utils.ReflectUtils;
import cn.wenhao.framework.utils.StringUtils;
import cn.wenhao.framework.utils.XmlUtils;

public class WriteToXMlForModel {

	/**
	 * 向XML文件中写入Property元素
	 * 
	 * @param field
	 * 
	 * @param classElement
	 * @param annts
	 * @param model
	 * @throws Exception
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public static void writeProperty(Field field, Element classElement,
			Map<String, Annotation> annts, Model model) throws Exception {

		Element propertyElement = null;
		Element columnElement = null;
		Map<String, String> propertyNeedSet = null;
		Map<String, String> columnNeedSet = null;
		for (Map.Entry<String, Annotation> entry : annts.entrySet()) {
			String elementsName = entry.getKey();
			Annotation annt = entry.getValue();
			Map<String, String> needSet = analyzeAnntotation(annt);
			if (elementsName.equals("property")) {

				String nameValue = needSet.get("name");
				if (nameValue.equals("MAKE#")) {
					needSet.put("name", field.getName());
				}

				propertyNeedSet = needSet;
				if (propertyElement != null) {
					continue;
				}
				propertyElement = classElement.addElement("property");
			} else if (elementsName.equals("column")) {
				columnNeedSet = needSet;
				if (propertyElement == null) {
					propertyElement = classElement.addElement("property");
				}
				if (columnNeedSet.size() > 0) {
					columnElement = propertyElement.addElement("column");
				}
			}
		}
		handlerNeedSetMap(propertyNeedSet, columnNeedSet);

		makeElement(propertyElement, model, propertyNeedSet);
		if (columnNeedSet.size() > 0) {
			makeElement(columnElement, model, columnNeedSet);
		}

	}

	private static void handlerNeedSetMap(Map<String, String> property,
			Map<String, String> column) {
		String excludeName = "name";
		for (Map.Entry<String, String> entry : property.entrySet()) {
			String keyName = entry.getKey();
			if (keyName.equals(excludeName)) {
				continue;
			} else if (column.get(keyName) != null) {
				column.remove(keyName);
			}
		}
	}

	/**
	 * 为Model对象添加ID元素的属性内容
	 * 
	 * @param field
	 * 
	 * @param annts
	 * @param model
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws DocumentException
	 */
	public static void writeId(Field field, Element rootOfClass,
			Map<String, Annotation> annts, Model model)
			throws IllegalArgumentException, IllegalAccessException, Exception {

		Element idElement = null;
		Element columnElement = null;
		Element generatorElement = null;
		for (Map.Entry<String, Annotation> entry : annts.entrySet()) {
			String elementsName = entry.getKey();
			Annotation annt = entry.getValue();
			Map<String, String> needSet = analyzeAnntotation(annt);
			if (elementsName.equals("id")) {
				String value = needSet.get("name");
				if (value.equals("MAKE#")) {
					String nameValue = field.getName();
					needSet.put("name", nameValue);
				}
				idElement = rootOfClass.addElement("id");
				makeElement(idElement, model, needSet);
			} else if (elementsName.equals("column")) {
				if (needSet.size() > 0) {
					columnElement = idElement.addElement("column");
					makeElement(columnElement, model, needSet);
				}
			} else if (elementsName.equals("generator")) {
				generatorElement = idElement.addElement("generator");
				String clazzName = needSet.get("class");
				System.out.println(clazzName.equals("foreign")
						+ ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
						+ needSet.get("param"));
				if (clazzName.equals("foreign") && needSet.get("param") != null) {
					Element paramElement = generatorElement.addElement("param");
					Map<String, String> needSet_ = new HashMap<String, String>();
					needSet_.put("name", "property");
					paramElement.addText(needSet.get("param"));
					makeElement(paramElement, model, needSet_);
				}
				needSet.remove("param");
				makeElement(generatorElement, model, needSet);
			}
		}
	}

	/**
	 * 向XML文件中写入Class元素节点
	 * 
	 * @param model
	 * @param needSet
	 * @return
	 */
	public static Document writeClass(Model model, Map<String, String> needSet) {
		// 向src路径下的该映射文件中写入Class信息
		try {
			String filePath = ModelUtils.srcXmlSavePath(model);
			Document document = XmlUtils.getDocumentIgnoreDtD(filePath);
			Element root = document.getRootElement();
			makeClassElement(root, model, needSet);
			return document;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 在XML文件中创建一个Class元素
	 * 
	 * @param root
	 * @param model
	 * @param needSet
	 */
	@SuppressWarnings("deprecation")
	private static void makeClassElement(Element root, Model model,
			Map<String, String> needSet) {
		Element classElement = root.addElement("class");
		for (Map.Entry<String, String> entry : needSet.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			classElement.setAttributeValue(key, value);
		}
	}

	/**
	 * 创建一个Element元素，并且将Map中的值作为其属性值设入
	 * 
	 * @param idElement
	 * @param model
	 * @param needSet
	 */
	@SuppressWarnings("deprecation")
	public static void makeElement(Element idElement, Model model,
			Map<String, String> needSet) {
		for (Map.Entry<String, String> entry : needSet.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			if (value instanceof String
					&& (value == null || ((String) value).length() == 0)) {
				continue;
			}
			idElement.setAttributeValue(key, value);
		}
	}

	/**
	 * 分析注解并将其上面的属性保存到一个Map集合中
	 * 
	 * @param annt
	 * @return
	 */
	public static Map<String, String> analyzeAnntotation(Annotation annt)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException, SecurityException, NoSuchFieldException {

		Map<String, String> needSet = new HashMap<String, String>();
		Method[] methods = annt.getClass().getDeclaredMethods();
		for (Method method : methods) {
			String methodName = method.getName();
			// 排除非必要的方法
			if (AnalyzeAnnotation.excludeMethod(methodName)) {
				continue;
			}
			String value = method.invoke(annt, new Object[] {}) + "";
			if (isDefaultValue(methodName, value)) {
				continue;
			}
			methodName = JavaModifyToXml(methodName);
			needSet.put(methodName, "" + value);
		}
		return needSet;
	}

	/**
	 * 判断是否为默认值
	 * 
	 * @param methodName
	 * @param value
	 * @return
	 */
	public static boolean isDefaultValue(String methodName, String value) {
		boolean isDefault = false;
		String dv = null;
		try {
			// 判断是否为默认值，如果为默认值，则不对该属性进行设置，默认使用Hibernate映射文件的默认值
			dv = DefaultValues.getDefault(methodName);
			// System.out.println(methodName + "默认值为：" + dv);
			if (dv != null && dv.equals(value + "")) {
				isDefault = true;
			}
		} catch (Exception e) {
		}
		return isDefault;
	}

	/**
	 * 将java标示符中不允许使用的-用_替代，在存入XML文件前转换回来
	 * 
	 * @param methodName
	 * @return
	 */
	private static String JavaModifyToXml(String methodName) {
		// 将_转换为-(在java文件中不允许使用-作为标示符字符，而在XML文件中，为-)
		if (methodName.endsWith("_")) {
			methodName = methodName.substring(0, methodName.length() - 1);
		}
		if (methodName.indexOf("_") != -1) {
			// System.out.println(">>>>>>>>>>>>>>>>" + methodName
			// + "===================中含有_");
			methodName = methodName.replaceAll("_", "-");
		}
		return methodName;
	}

	/**
	 * 配置One2One标签
	 * 
	 * @param classElement
	 * @param needSet
	 * @param model
	 */
	public static void writeMappendOne2One(Field field, Element classElement,
			Map<String, String> needSet, Model model) {
		solveNeedSet(field, needSet);
		Element one2oneElement = classElement.addElement("one-to-one");
		makeMappendValue(needSet, one2oneElement);

	}

	/**
	 * * 配置Many2One标签
	 * 
	 * @param classElement
	 * @param needSet
	 * @param model
	 */
	public static void writeMappendMany2One(Field field, Element classElement,
			Map<String, String> needSet, Model model) {
		solveNeedSet(field, needSet);
		Element one2oneElement = classElement.addElement("many-to-one");
		makeMappendValue(needSet, one2oneElement);
	}

	private static void solveNeedSet(Field field, Map<String, String> needSet) {
		String nameValue = needSet.get("name");
		if ("MAKE#".equals(nameValue)) {
			needSet.put("name", field.getName());
		}

		String columnValue = needSet.get("column");
		if ("MAKE#".equals(columnValue)) {
			needSet.put("column", field.getName().toLowerCase() + "_id");
		}
	}

	public static void writeDoubleMappendOne2One(Element classElement,
			Map<String, String> needSet, Model model) {

	}

	/**
	 * 为某个Element属性配置属性值
	 * 
	 * @param needSet
	 * @param one2oneElement
	 */
	@SuppressWarnings("deprecation")
	private static void makeMappendValue(Map<String, String> needSet,
			Element one2oneElement) {
		for (Map.Entry<String, String> entry : needSet.entrySet()) {
			String name = entry.getKey();
			String value = entry.getValue();
			if (isDefaultValue(name, value)) {
				continue;
			}
			name = JavaModifyToXml(name);
			one2oneElement.setAttributeValue(name, value);
		}
	}

	/**
	 * 创建一对多的关系映射
	 * 
	 * @param field
	 * 
	 * @param classElement
	 * @param childAnnts
	 * @param model
	 * @throws IllegalArgumentException
	 */
	public static void writeUnknow2ManyMapped(Field field,
			Element classElement, Map<String, Annotation> childAnnts,
			Model model, boolean isMany2Many) throws Exception {
		Element setElement = null;
		for (Map.Entry<String, Annotation> entry : childAnnts.entrySet()) {
			String elementsName = entry.getKey();
			elementsName = JavaModifyToXml(elementsName);
			Annotation annt = entry.getValue();
			Map<String, String> needSet = analyzeAnntotation(annt);
			if (elementsName.equals("set")) {
				setElement = solveSetMapped(field, classElement, model,
						setElement, needSet, isMany2Many);
			} else if (elementsName.equals("key")) {
				setElement = solveKey(classElement, model, setElement, needSet);
			} else {
				setElement = solveOne2ManyOrMany2Many(field, classElement,
						model, isMany2Many, setElement, needSet);
			}
		}
	}

	private static Element solveOne2ManyOrMany2Many(Field field,
			Element classElement, Model model, boolean isMany2Many,
			Element setElement, Map<String, String> needSet) {
		Element Unknow2ManyElement;
		String classValue = needSet.get("class");
		if (classValue.equals("MAKE#")) {
			System.out.println(field.getName());
			Class<?> clazz = ReflectUtils.getGenericClass(field);
			if (clazz != null)
				needSet.put("class", clazz.getName());
		}
		if (setElement == null) {
			setElement = classElement.addElement("set");
		}
		if (!isMany2Many)
			Unknow2ManyElement = setElement.addElement("one-to-many");
		else {
			String columnValue = needSet.get("column");
			if (columnValue.equals("MAKE#")) {
				Class<?> clazz = ReflectUtils.getGenericClass(field);
				String otherModelName = clazz.getSimpleName();
				needSet.put("column", otherModelName.toLowerCase() + "_id");
			}
			Unknow2ManyElement = setElement.addElement("many-to-many");
		}
		makeElement(Unknow2ManyElement, model, needSet);
		return setElement;
	}

	private static Element solveKey(Element classElement, Model model,
			Element setElement, Map<String, String> needSet) {
		Element keyElement;
		String columnValue = needSet.get("column");
		if (columnValue.equals("MAKE#")) {
			String modelName = ModelUtils.getModelSimpleName(model);
			needSet.put("column", modelName.toLowerCase() + "_id");
		}
		if (needSet.size() > 0) {
			if (setElement == null) {
				setElement = classElement.addElement("set");
			}
			keyElement = setElement.addElement("key");
			makeElement(keyElement, model, needSet);
		}
		return setElement;
	}

	private static Element solveSetMapped(Field field, Element classElement,
			Model model, Element setElement, Map<String, String> needSet,
			boolean isMany2Many) {
		String nameValue = needSet.get("name");
		if (nameValue.equals("MAKE#")) {
			String fieldName = field.getName();
			needSet.put("name", fieldName);
		}
		String tableValue = needSet.get("table");
		if (tableValue.equals("MAKE#")) {
			String tableName = null;
			if (isMany2Many) {
				Class<?> clazz = ReflectUtils.getGenericClass(field);
				String otherModelName = clazz.getSimpleName();
				String modelSimpleName = ModelUtils.getModelSimpleName(model);
				tableName = StringUtils.sortString(otherModelName,
						modelSimpleName, "_");
				needSet.put("table", tableName);
			} else {
				needSet.remove("table");
			}
		}
		if (setElement == null)
			setElement = classElement.addElement("set");
		makeElement(setElement, model, needSet);
		return setElement;
	}
}
