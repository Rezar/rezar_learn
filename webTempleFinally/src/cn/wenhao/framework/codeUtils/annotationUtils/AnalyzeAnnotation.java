package cn.wenhao.framework.codeUtils.annotationUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;

import cn.wenhao.framework.codeUtils.annotation.Column;
import cn.wenhao.framework.codeUtils.annotation.Composite;
import cn.wenhao.framework.codeUtils.annotation.GeneralField;
import cn.wenhao.framework.codeUtils.annotation.Generator;
import cn.wenhao.framework.codeUtils.annotation.IdAnnotation;
import cn.wenhao.framework.codeUtils.annotation.KeyMapped;
import cn.wenhao.framework.codeUtils.annotation.KeyProperty;
import cn.wenhao.framework.codeUtils.annotation.Many2ManyMapped;
import cn.wenhao.framework.codeUtils.annotation.Many2ManySetMapped;
import cn.wenhao.framework.codeUtils.annotation.Many2OneMapped;
import cn.wenhao.framework.codeUtils.annotation.One2ManyMapped;
import cn.wenhao.framework.codeUtils.annotation.One2ManySetMapped;
import cn.wenhao.framework.codeUtils.annotation.One2OneDoubleForeignKey;
import cn.wenhao.framework.codeUtils.annotation.One2OneDoublePrimaryKey;
import cn.wenhao.framework.codeUtils.annotation.One2OneSignalForeignKey;
import cn.wenhao.framework.codeUtils.annotation.One2OneSignalPrimaryKey;
import cn.wenhao.framework.codeUtils.annotation.PrimaryKey;
import cn.wenhao.framework.codeUtils.annotation.PropertyField;
import cn.wenhao.framework.codeUtils.annotation.SetMapped;
import cn.wenhao.framework.codeUtils.annotation.Table;
import cn.wenhao.framework.codeUtils.generaUtils.ModelUtils;
import cn.wenhao.framework.exception.PrimaryKeyException;
import cn.wenhao.framework.exception.TooManyAnnotationForFieldException;
import cn.wenhao.framework.model.Model;
import cn.wenhao.framework.utils.XmlUtils;

public class AnalyzeAnnotation {

	public static void analyzeModel(Model model) throws Exception {

		boolean isCompositePrimaryKey = false;
		// 配置该Model对象的Class属性
		Document document = makeClassXml(model);
		Element root = document.getRootElement();
		Element classElement = root.element("class");

		Annotation composite = model.getClass().getAnnotation(Composite.class);
		if (composite != null) {
			isCompositePrimaryKey = true;
			// 创建联合主键
			makeComposite(classElement, composite, model);
		}

		Field fields[] = model.getClass().getDeclaredFields();
		Map<Field, Annotation> primaryKey = new HashMap<Field, Annotation>(0);
		Map<Field, List<Annotation>> fieldValueAnnt = new HashMap<Field, List<Annotation>>();
		Map<Field, List<Annotation>> fieldMappendAnnt = new HashMap<Field, List<Annotation>>();
		for (int i = 0; fields != null && i < fields.length; i++) {
			Field f = fields[i];
			f.setAccessible(true);
			Annotation[] anns = f.getAnnotations();
			if (anns == null || anns.length == 0) {
				continue;
			}

			List<Annotation> annsList = Arrays.asList(anns);
			List<Annotation> generalFieldAnnt = new ArrayList<Annotation>();
			List<Annotation> mappend = new ArrayList<Annotation>();

			for (Annotation annt : annsList) {
				if (annt instanceof PropertyField) {
					System.out.println(annt);
					generalFieldAnnt.add(annt);
				} else if (annt instanceof PrimaryKey) {
					if (isCompositePrimaryKey) {
						throw new PrimaryKeyException("已经配置了联合主键，不允许在配置单独的主键！");
					}
					if (primaryKey.size() > 0) {
						throw new PrimaryKeyException(
								"该Model类对应了超过一个主键，请重新检查注解配置！");
					}
					primaryKey.put(f, annt);
				} else {
					mappend.add(annt);
				}
			}

			if (generalFieldAnnt.size() != 0) {
				fieldValueAnnt.put(f, generalFieldAnnt);
			}

			if (mappend.size() != 0) {
				fieldMappendAnnt.put(f, mappend);
			}
		}

		// 配置主键的映射信息
		if (primaryKey.size() == 1) {
			// 为该Model类对象配置主键
			makePrimaryKey(classElement, primaryKey, model);
		} else if (!isCompositePrimaryKey) {
			throw new PrimaryKeyException(model.getClass().getName()
					+ "该Model类没有配置主键，请重新检查注解配置！");
		}

		// 配置普通属性的映射信息
		if (fieldValueAnnt.size() > 0) {
			makeFieldXmlValue(classElement, fieldValueAnnt, model);
		}

		// 配置映射关系
		makeMappedXmlValue(classElement, fieldMappendAnnt, model);

		// 将文档写入到ＸＭＬ文件中
		String filePath = ModelUtils.srcXmlSavePath(model);
		XmlUtils.writeToXml(document, filePath);
	}

	/**
	 * * 配置该属性的映射关系
	 * 
	 * @param classElement
	 * @param fieldMappendAnnt
	 * @param model
	 * @throws Exception
	 */
	private static void makeMappedXmlValue(Element classElement,
			Map<Field, List<Annotation>> fieldMappendAnnt, Model model)
			throws Exception {
		for (Map.Entry<Field, List<Annotation>> entry : fieldMappendAnnt
				.entrySet()) {
			List<Annotation> annts = entry.getValue();
			boolean isTooManyMappedAnnt = isTooManyMappedAnnt(annts);
			if (isTooManyMappedAnnt) {
				throw new TooManyAnnotationForFieldException(
						"该属性字段上配置了超过一个映射注解，请检查Model对象的注解设置！");
			}
			Field field = entry.getKey();
			Annotation annt = getMappedAnnt(annts);
			Map<String, String> needSet = null;
			needSet = makeMappendElement(classElement, model, annts.get(0));
			if (annt instanceof One2OneSignalPrimaryKey) {
				// 配置基于主键的单向一对一
				WriteToXMlForModel.writeMappendOne2One(field, classElement,
						needSet, model);
			} else if (annt instanceof One2OneSignalForeignKey) {
				// 基于外键的单向一对一
				WriteToXMlForModel.writeMappendMany2One(field, classElement,
						needSet, model);
			} else if (annt instanceof One2OneDoublePrimaryKey) {
				// 基于主键的双向一对一
				WriteToXMlForModel.writeMappendOne2One(field, classElement,
						needSet, model);
			} else if (annt instanceof One2OneDoubleForeignKey) {
				// 基于外键的双向一对一
				WriteToXMlForModel.writeMappendOne2One(field, classElement,
						needSet, model);
			} else if (annt instanceof Many2OneMapped) {
				// 配置单向的多对一
				WriteToXMlForModel.writeMappendMany2One(field, classElement,
						needSet, model);
			} else if (annt instanceof One2ManySetMapped) {
				// 基于外键的一对多映射
				solveUnknow2ManyMapped(field, classElement, annt, model, false);
			} else if (annt instanceof Many2ManySetMapped) {
				// 基于中间表的多对多映射
				solveUnknow2ManyMapped(field, classElement, annt, model, true);
			}
		}
	}

	/**
	 * 获取多个注解中的有效的配置注解
	 * 
	 * @param annts
	 * @return
	 */
	private static Annotation getMappedAnnt(List<Annotation> annts) {
		for (Annotation annt : annts) {
			if (annt instanceof PrimaryKey || annt instanceof PropertyField
					|| annt instanceof One2OneDoubleForeignKey
					|| annt instanceof One2OneDoublePrimaryKey
					|| annt instanceof One2OneSignalPrimaryKey
					|| annt instanceof One2OneSignalForeignKey
					|| annt instanceof One2ManySetMapped
					|| annt instanceof Many2OneMapped
					|| annt instanceof Many2ManySetMapped
					|| annt instanceof KeyMapped
					|| annt instanceof IdAnnotation) {
				return annt;
			}
		}
		return null;
	}

	/**
	 * 判断某个字段上是否配置了超过一个 配置注解(不包括非配置注解的注解)
	 * 
	 * @param annts
	 * @return
	 */
	private static boolean isTooManyMappedAnnt(List<Annotation> annts) {
		int anntNumber = 0;
		for (Annotation annt : annts) {
			if (annt instanceof PrimaryKey || annt instanceof PropertyField
					|| annt instanceof One2OneDoubleForeignKey
					|| annt instanceof One2OneDoublePrimaryKey
					|| annt instanceof One2OneSignalPrimaryKey
					|| annt instanceof One2OneSignalForeignKey
					|| annt instanceof One2ManySetMapped
					|| annt instanceof Many2OneMapped
					|| annt instanceof Many2ManySetMapped
					|| annt instanceof Many2ManyMapped
					|| annt instanceof SetMapped
					|| annt instanceof One2ManyMapped
					|| annt instanceof KeyProperty || annt instanceof KeyMapped
					|| annt instanceof IdAnnotation
					|| annt instanceof Generator
					|| annt instanceof GeneralField || annt instanceof Column) {
				anntNumber++;
			}
		}
		if (anntNumber > 1) {
			return true;
		}
		return false;
	}

	/**
	 * 基于外键的一对多映射
	 * 
	 * @param field
	 * 
	 * @param classElement
	 * @param annt
	 * @param model
	 * @throws Exception
	 */
	private static void solveUnknow2ManyMapped(Field field,
			Element classElement, Annotation annt, Model model,
			boolean isMany2Many) throws Exception {
		Method[] methods = annt.getClass().getDeclaredMethods();
		Map<String, Annotation> childAnnts = new LinkedHashMap<String, Annotation>();
		for (Method method : methods) {
			String methodName = method.getName();
			if (excludeMethod(methodName)) {
				continue;
			}
			Annotation childAnnt = (Annotation) method.invoke(annt,
					new Object[] {});
			childAnnts.put(methodName, childAnnt);
		}
		WriteToXMlForModel.writeUnknow2ManyMapped(field, classElement,
				childAnnts, model, isMany2Many);
	}

	/**
	 * 配置属性的基于主键的一对一关系，使用<one-to-one>标签
	 * 
	 * @param classElement
	 * @param model
	 * @param annotation
	 * @throws Exception
	 */
	private static Map<String, String> makeMappendElement(Element classElement,
			Model model, Annotation annotation) throws Exception {

		Map<String, String> needSet = new HashMap<String, String>();
		Method[] methods = annotation.getClass().getDeclaredMethods();
		for (Method method : methods) {
			String methodName = method.getName();
			if (excludeMethod(methodName)) {
				continue;
			}
			String value = method.invoke(annotation, new Object[] {}) + "";
			needSet.put(methodName, value);
		}
		return needSet;
	}

	/**
	 * 配置联合主键的XML信息
	 * 
	 * @param classElement
	 * @param composite
	 * @param model
	 * @throws Exception
	 */
	private static void makeComposite(Element classElement,
			Annotation composite, Model model) throws Exception {

		Element compositeElement = classElement.addElement("composite-id");

		Method method = composite.getClass().getMethod("keyPropertys",
				new Class[] {});
		KeyProperty[] keyPs = (KeyProperty[]) method.invoke(composite,
				new Object[] {});
		for (KeyProperty kp : keyPs) {
			Map<String, String> needSet = WriteToXMlForModel
					.analyzeAnntotation(kp);
			Element keyP = compositeElement.addElement("key-property");
			WriteToXMlForModel.makeElement(keyP, model, needSet);
		}
	}

	/**
	 * 生成该Model对象的配置文件中的Class配置属性
	 * 
	 * @param model
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	private static Document makeClassXml(Model model)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {

		System.out.println("开始配置Class信息");
		Annotation annt = model.getClass().getAnnotation(Table.class);
		Map<String, String> needSet = new HashMap<String, String>();
		Method[] methods = annt.getClass().getDeclaredMethods();
		for (Method method : methods) {
			String methodName = method.getName();
			if (excludeMethod(methodName)) {
				continue;
			}
			Object value = method.invoke(annt, new Object[] {});
			if (value instanceof String) {
				String strValue = value + "";
				if (strValue.equals("MAKE#")) {
					if (methodName.equals("name")) {
						value = model.getClass().getName();
					} else if (methodName.equals("table")) {
						value = "t_"
								+ model.getClass().getSimpleName()
										.toLowerCase();
					}
				}
			}
			needSet.put(methodName, "" + value);
		}

		return WriteToXMlForModel.writeClass(model, needSet);

	}

	/**
	 * 配出注解类中不需要的方法
	 * 
	 * @param methodName
	 * @return
	 */
	public static boolean excludeMethod(String methodName) {
		if (methodName.equals("toString") || methodName.equals("equals")
				|| methodName.equals("annotationType")
				|| methodName.equals("hashCode")
				|| methodName.equals("getClass")) {
			return true;
		} else
			return false;
	}

	/**
	 * 配置主键映射属性
	 * 
	 * @param classElement
	 * 
	 * @param primaryKey
	 * @param model
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	private static void makePrimaryKey(Element classElement,
			Map<Field, Annotation> primaryKey, Model model)
			throws IllegalArgumentException, Exception {

		PrimaryKey idAnnt = null;
		Field field = null;
		for (Map.Entry<Field, Annotation> entry : primaryKey.entrySet()) {
			field = entry.getKey();
			idAnnt = (PrimaryKey) entry.getValue();
		}

		Map<String, Annotation> annts = new LinkedHashMap<String, Annotation>();
		Method[] methods = idAnnt.getClass().getDeclaredMethods();
		for (Method method : methods) {
			String methodName = method.getName();
			if (excludeMethod(methodName)) {
				continue;
			}
			Object value = method.invoke(idAnnt, new Object[] {});
			annts.put(methodName, (Annotation) value);
		}

		WriteToXMlForModel.writeId(field, classElement, annts, model);

	}

	/**
	 * 创建普通的属性的映射节点
	 * 
	 * @param field
	 * @param classElement
	 * @param model
	 * @param annt
	 * @throws Exception
	 */
	public static <T extends Annotation> void makePropertyElement(Field field,
			Element classElement, Model model, T annt) throws Exception {

		Map<String, Annotation> annts = new LinkedHashMap<String, Annotation>();
		Method[] methods = annt.getClass().getDeclaredMethods();
		for (Method method : methods) {
			String methodName = method.getName();
			if (excludeMethod(methodName)) {
				continue;
			}
			Object value = method.invoke(annt, new Object[] {});
			annts.put(methodName, (Annotation) value);
		}

		WriteToXMlForModel.writeProperty(field, classElement, annts, model);
	}

	/**
	 * 配置该属性字段的XML映射文件中的数据
	 * 
	 * @param fieldValueAnnt
	 * @param model
	 * @throws Exception
	 */
	private static void makeFieldXmlValue(Element classElement,
			Map<Field, List<Annotation>> fieldValueAnnt, Model model)
			throws Exception {
		for (Map.Entry<Field, List<Annotation>> entry : fieldValueAnnt
				.entrySet()) {
			List<Annotation> annt = entry.getValue();
			if (annt.size() > 1) {
				throw new TooManyAnnotationForFieldException(
						"该属性字段上配置了超过一个映射注解，请检查Model对象的注解设置！");
			}
			makePropertyElement(entry.getKey(), classElement, model,
					annt.get(0));
		}
	}

}
