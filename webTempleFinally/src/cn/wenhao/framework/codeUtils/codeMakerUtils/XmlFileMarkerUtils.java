package cn.wenhao.framework.codeUtils.codeMakerUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import cn.wenhao.framework.codeUtils.generaUtils.CUtils;
import cn.wenhao.framework.codeUtils.generaUtils.ModelUtils;
import cn.wenhao.framework.model.Model;
import cn.wenhao.framework.utils.FileUtils;

public class XmlFileMarkerUtils {


	private static final Properties prop = new Properties();
	static {
		InputStream in = XmlFileMarkerUtils.class.getClassLoader()
				.getResourceAsStream(CUtils.PROP_FILE_NAME);
		try {
			prop.load(in);
		} catch (IOException e) {
			e.printStackTrace();
			throw new Error("FileNotFound.....:" + CUtils.PROP_FILE_NAME);
		}

	}

	/**
	 * 获取MOdel对象的配置文件应该存放的位置 包括 src 路径下的保存位置 和 web 应用下的class 路径下的位置
	 * 
	 * @return
	 */
	public static String[] getXmlFileSavePath() {

		// 获取到Model对象存放的位置
		String modelFolder = prop.getProperty("model.savePath");
		String srcPath = prop.getProperty("srcPath");
		String classPath = prop.getProperty("classPath");

		String xmlSrcPath = srcPath
				+ modelFolder.replaceAll("\\.", "\\"
						+ CUtils.FILE_SEPARATOR);

		String xmlClassPath = classPath
				+ modelFolder.replaceAll("\\.", "\\"
						+ CUtils.FILE_SEPARATOR);
		System.out.println(xmlSrcPath);
		System.out.println(new File(xmlClassPath).getAbsolutePath());
		return new String[] { xmlSrcPath,
				new File(xmlClassPath).getAbsolutePath() };
	}

	public static File[] createNewXml(Model model, boolean reflesh) {
		// 首先，生成该Model对象的配置空白文件并返回该XML配置文件
		File[] srcAndClassFile = makeAndGetModelXmlFile(model, reflesh);
		return srcAndClassFile;
	}

	/**
	 * 该句Model对象生成该model对象的空白配置文件
	 * 
	 * @param model
	 */
	private static File[] makeAndGetModelXmlFile(Model model, boolean refresh) {
		// 获取到模板Hibernate的映射文件路径
		String templetXmlFilePath = PropertiesFileUtils
				.getSrcOrConfig("config");
		System.out.println("===" + templetXmlFilePath);
		// 获取该文件
		File templetXmlFile = new File(templetXmlFilePath + "Model.hbm.xml");
		// 获取到model对象的XML文件应该存放到的src路径和class路径(绝对路径)
		String[] modelXmlFile = getXmlFileSavePath();
		// 需要创建的该model对象的XML文件(src)
		File modelSrcXmlFile = new File(modelXmlFile[0] + CUtils.FILE_SEPARATOR
				+ ModelUtils.getModelSimpleName(model) + ".hbm.xml");
		// 需要创建的该model对象的XML文件(class)
		File modelClassXmlFile = new File(modelXmlFile[1]
				+ CUtils.FILE_SEPARATOR + ModelUtils.getModelSimpleName(model)
				+ ".hbm.xml");

		// 如果需要强制更新该配置文件或者改配置文件仍未存在
		if (!modelSrcXmlFile.exists() || refresh) {
			try {
				FileUtils.copyFile(templetXmlFile, modelSrcXmlFile);
				FileUtils.copyFile(templetXmlFile, modelClassXmlFile);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("文件不存在！");
			}
		}
		return new File[] { modelSrcXmlFile, modelClassXmlFile };
	}

	public static void main(String[] args) {
		// XmlFileMarkerUtils.getXmlFileSavePath();
//		XmlFileMarkerUtils.createNewXml(new UserModel(), true);
	}

}
