package cn.wenhao.framework.codeUtils.codeMakerUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.util.Properties;

import cn.wenhao.framework.codeUtils.annotation.JustThisClass;
import cn.wenhao.framework.codeUtils.generaUtils.CUtils;
import cn.wenhao.framework.codeUtils.generaUtils.GeneralUtils;
import cn.wenhao.framework.model.Model;
import cn.wenhao.framework.utils.FileUtils;
import cn.wenhao.framework.utils.StringUtils;

/**
 * 自动检测所有的Model类，并且根据相关注解进行映射文件的生成(包含了CodeMarkUtils的相关功能)
 * 
 * @author Administrator
 * 
 */
public class AutoCreate {
	private static Properties prop = new Properties();
	private static String modelPath = null;
	private static String path;
	static {
		InputStream in = AutoCreate.class.getClassLoader().getResourceAsStream(
				CUtils.PROP_FILE_NAME);
		InputStream inOfModelInfos = AutoCreate.class.getClassLoader()
				.getResourceAsStream(CUtils.MODE_CHANGE_INFO);
		path = AutoCreate.class.getClassLoader()
				.getResource(CUtils.MODE_CHANGE_INFO).getPath();
		path = StringUtils.decodeString(path);
		try {
			prop.load(in);
			modelPath = prop.getProperty("model.savePath");
			prop.clear();
			prop = new Properties();
			prop.load(inOfModelInfos);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			FileUtils.closeInputStream(in);
		}
	}

	/**
	 * 
	 * 该方法开始进行Model类的相关配置文件和QueryModel,DaoInterface,DaoImpl,Ebi,Ebo,以及两个
	 * spring文件的配置的操作
	 * 
	 * 是否强制更新所有的配置
	 * 
	 * @param refresh
	 */
	public static void autoCreate(boolean refresh) {
		String path = modelPath.replaceAll("\\.", "/");
		String classPath = GeneralUtils.getWebDefaultTruePath();
		// output: /J:/WebTemplet/WebRoot/WEB-INF/classes/
		System.out.println(classPath + path);
		try {
			findClasses(new File(classPath + path), refresh);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 查找指定目录下的所有实体字节码文件
	 * 
	 * @param directory
	 * @param refresh
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private static void findClasses(File directory, boolean refresh)
			throws ClassNotFoundException {

		File[] files = directory.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".class");
			}
		});

		for (File file : files) {
			String fileName = file.getName();
			fileName = fileName.replaceAll(".class", "");
			String classPath = modelPath + "." + fileName;
			System.out.println(classPath);
			/*
			 * long longTime = file.lastModified(); String longBefore =
			 * prop.getProperty(classPath); if (!refresh && (longBefore != null
			 * && longBefore.equals(longTime + ""))) {
			 * System.out.println(classPath + "没有修改，不再进行自动的映射文件编写！"); continue;
			 * }
			 */
			Class<? extends Model> clazz = (Class<? extends Model>) Class
					.forName(classPath);
			checkModel(clazz, refresh);
			prop.setProperty(classPath, file.lastModified() + "");
		}

		OutputStream out = null;
		try {
			System.out.println(path);
			out = new FileOutputStream(path);
			prop.store(out,
					"############model's lastmodified time ##############");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("config目录下不存在modelInfos.properties配置文件，请创建！");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("无法写入Model的最后修改信息！");
		} finally {
			FileUtils.closeOutputStream(out);
		}

	}

	private static void checkModel(Class<? extends Model> clazz, boolean refresh) {
		Annotation[] annts = clazz.getAnnotations();
		for (Annotation annt : annts) {

			/**
			 * 如果是只刷新特定的类，加上JustThisClass 注解
			 */
			if (annt instanceof JustThisClass && !refresh) {
				try {
					CodeMarkerUtils.codeMakeAll(clazz.newInstance(), refresh);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("在：" + clazz.getName()
							+ "代码自动化生成失败或者Model的映射文件自动生成失败！");
				}
			}

			if (refresh
					&& annt instanceof cn.wenhao.framework.codeUtils.annotation.AutoCreate) {
				try {
					CodeMarkerUtils.codeMakeAll(clazz.newInstance(), refresh);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("在：" + clazz.getName()
							+ "代码自动化生成失败或者Model的映射文件自动生成失败！");
				}
			}
		}
	}

	/**
	 * 仅仅更新当前的Model对象
	 * 
	 * @param clazz
	 * @param refresh
	 */
	public static void autoCurrentModel(Model model) {
		try {
			CodeMarkerUtils.codeMakeAll(model, true);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("在：" + model.getClass().getName()
					+ "代码自动化生成失败或者Model的映射文件自动生成失败！");
		}
	}
}
