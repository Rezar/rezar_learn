package cn.wenhao.framework.codeUtils.codeMakerUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Properties;

import cn.wenhao.framework.codeUtils.generaUtils.CUtils;
import cn.wenhao.framework.codeUtils.generaUtils.GeneralUtils;

public class PropertiesFileUtils {

	public static final String LINE_SEPARATOR = System
			.getProperty("line.separator");

	public static final String FILE_SEPARATOR = File.separator;

	/**
	 * 在应用程序启动的时候，先对该配置文件的改属性进行配置，关键是配置该应用的src路径的绝对值
	 * 
	 * @throws IOException
	 */
	public static void setFileFirst() throws IOException {

		try {
			String path = GeneralUtils
					.getWebDefaultTruePath(CUtils.PROP_FILE_NAME);
			updateFile(path, true);
			path = getSrcOrConfig("config") + "classSavePath.properties";
			updateFile(path, true);
		} catch (IOException e) {
			throw new FileNotFoundException(
					"资源文件不存在，请重新配置classSavePath.properties文件");
		}

	}

	/**
	 * 根据传入的标记获取该标记相对于该应用程序的绝对路径而不是相对于WebRoot的绝对路径 eg:传入src:
	 * 返回：E:\struts\WebTemplet\src\
	 * 
	 * @param fileTag
	 * @return
	 */
	public static String getSrcOrConfig(String fileTag) {
		String pathStr = GeneralUtils.getWebDefaultTruePath();
		File file = new File(pathStr);
		String path = file.getAbsolutePath();

		System.out.println("path:" + path);

		String webPath = FILE_SEPARATOR + "WebRoot" + FILE_SEPARATOR
				+ "WEB-INF" + FILE_SEPARATOR + "classes";
		int index3 = path.indexOf(webPath);
		System.out.println("index3:" + index3);
		String srcPath = path.substring(0, index3) + FILE_SEPARATOR + fileTag
				+ path.substring(index3 + webPath.length());
		return srcPath + File.separator;
	}

	/**
	 * 根性配置文件
	 * 
	 * @param path
	 *            ： 当前配置文件保存的位置
	 * @param refresh
	 *            ：是否进行强制更新
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static void updateFile(String path, boolean refresh)
			throws FileNotFoundException, IOException {
		String classPath = GeneralUtils.getWebDefaultTruePath();
		System.out.println("classPath:" + classPath);
		String srcPath = getSrcOrConfig("src");
		updateSrcAndClass(path, refresh, srcPath, "srcPath");
		updateSrcAndClass(path, refresh, classPath, "classPath");
	}

	private static void updateSrcAndClass(String path, boolean refresh,
			String srcOrClassPath, String key) throws FileNotFoundException,
			IOException {
		Properties prop = new Properties();
		InputStream in = null;
		System.out.println(path);
		in = new FileInputStream(path);
		prop.load(in);
		if (prop.get(key) == null) {
			updateProperties(path, srcOrClassPath, key);
		} else {
			if (refresh) {
				OutputStream out = new FileOutputStream(path);
				prop.setProperty(key, srcOrClassPath);
				prop.store(out, "更新于：" + new Date());
			}
		}
	}

	/**
	 * 当配置文件还没有该SrcPath的键的时候，通过文件写入的方式进行资源的设置
	 * 
	 * @param filepath
	 * @param srcPath
	 * @throws FileNotFoundException
	 */
	private static void updateProperties(String filepath,
			String srcOrClassPath, String key) throws FileNotFoundException {
		System.out.println("文件路径为：" + filepath);
		OutputStream out = new FileOutputStream(new File(filepath), true);
		PrintWriter pw = new PrintWriter(out, true);
		pw.println(key + "=" + srcOrClassPath);
		pw.close();
	}
}
