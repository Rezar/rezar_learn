package cn.wenhao.framework.codeUtils.codeMakerUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;

import cn.wenhao.framework.codeUtils.annotationUtils.AnalyzeAnnotation;
import cn.wenhao.framework.codeUtils.generaUtils.CUtils;
import cn.wenhao.framework.codeUtils.generaUtils.ModelUtils;
import cn.wenhao.framework.model.Model;
import cn.wenhao.framework.utils.XmlUtils;

/**
 * 根据Mode对象自动创建其QueryModel,Dao,DaoImpl,Ebi,Ebo等对象以及向XML配置文件中注册数据源信息
 * 
 * @author Administrator
 * 
 */
public class CodeMarkerUtils {

	private static Properties prop = new Properties();
	static {

		InputStream in = CodeMarkerUtils.class.getClassLoader()
				.getResourceAsStream(CUtils.PROP_FILE_NAME);
		try {
			prop.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 该方法只应该在Model对象创建后的第一次调用且只应该调用一次
	 * 
	 * @param model
	 */
	public static void codeMakeAll(Model model, boolean refresh) {

		makeQueryModel(model);
		makeDaoInterface(model);
		makeDaoImpl(model);
		makeModelEbi(model);
		makeModelEbo(model);

		writeDaoBeanToXml(model, refresh);
		writeEbiBeanToXml(model, refresh);

		try {
			// 创建映射文件
			AnalyzeAnnotation.analyzeModel(model);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("创建Model类的映射文件失败！");
		}

	}

	/**
	 * <bean id="messageEbi"
	 * class="cn.wenhao.HHB.example.bussiness.ebo.MessageEbo"
	 * parent="ebo_parent"> <property name="dao" ref="messageModelDao" />
	 * </bean>
	 * 
	 * 
	 * @param model
	 * @param refresh
	 */
	private static void writeEbiBeanToXml(Model model, boolean refresh) {
		try {
			String xmlPath = prop.getProperty("ebo_beans");
			Document document = XmlUtils.getDocumentInClass(xmlPath);
			Element root = document.getRootElement();
			System.out.println(root.getName());
			makeElements(model, root, refresh);
			XmlUtils.write2XmlInClass(document, xmlPath);

			Document document2 = XmlUtils.getDocumentInConfig(xmlPath);
			System.out.println(document2.getName());
			Element root2 = document2.getRootElement();
			System.out.println(root2.getName());
			makeElements(model, root2, refresh);
			XmlUtils.write2XmlInConfig(document2, xmlPath);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 生成Ebi的XML配置文件
	 * 
	 * @param model
	 * @param root
	 * @param refresh
	 */
	@SuppressWarnings("deprecation")
	private static void makeElements(Model model, Element root, boolean refresh) {

		Element bean_tag = beanIsExites(model, root,
				ModelUtils.getEbiSimpleName(model));
		boolean isExites = bean_tag == null ? false : true;
		if (isExites) {
			if (refresh) {
				root.remove(bean_tag);
			} else
				return;
		}
		bean_tag = root.addElement("bean");
		bean_tag.setAttributeValue("id", ModelUtils.getEbiSimpleName(model));
		bean_tag.setAttributeValue("class", ModelUtils.getFullEboName(model));
		bean_tag.setAttributeValue("parent", "ebo_parent");

		Element prop_tag = bean_tag.addElement("property");
		prop_tag.setAttributeValue("name", "dao");
		prop_tag.setAttributeValue("ref",
				ModelUtils.getModelIDaoSimpleName(model));
	}

	/**
	 * <bean id="messageModelDao"
	 * class="cn.wenhao.HHB.example.dao.impl.MessageModelDaoImpl" parent="ahdi"
	 * />
	 * 
	 * @param model
	 */

	private static void writeDaoBeanToXml(Model model, boolean refresh) {
		try {
			String xmlPath = prop.getProperty("daoimpl_beans");
			Document document = XmlUtils.getDocumentInClass(xmlPath);
			Element root = document.getRootElement();
			makeDaoElement(model, root, refresh);
			XmlUtils.write2XmlInClass(document, xmlPath);

			Document document2 = XmlUtils.getDocumentInConfig(xmlPath);
			Element root2 = document2.getRootElement();
			makeDaoElement(model, root2, refresh);
			XmlUtils.write2XmlInConfig(document2, xmlPath);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 判断某个节点在XML文件中是否存在了已经
	 * 
	 * @param model
	 * @param root
	 * @param eleNull
	 * @return
	 */
	private static Element beanIsExites(Model model, Element root,
			String matchStr) {
		@SuppressWarnings("unchecked")
		List<Element> list = root.elements();
		for (Element ele : list) {
			String name = ele.getName();
			if (name.equals("bean")) {
				Attribute idAttr = ele.attribute("id");
				String value = idAttr.getValue();
				if (value.equals(matchStr)) {
					System.out.println("id为：" + value + "的节点存在！");
					return ele;
				}
			}
		}
		return null;
	}

	/**
	 * 创建一个Dao的spring配置文件的节点
	 * 
	 * @param model
	 * @param root
	 * @param refresh
	 */
	@SuppressWarnings("deprecation")
	private static void makeDaoElement(Model model, Element root,
			boolean refresh) {

		Element bean_tag = beanIsExites(model, root,
				ModelUtils.getModelIDaoSimpleName(model));
		boolean isExites = bean_tag == null ? false : true;

		if (isExites && refresh) {
			System.out.println("在配置" + ModelUtils.getModelFullName(model)
					+ "的dao bean节点时，该节点已经存在，进行属性更新！");
			bean_tag.setAttributeValue("class",
					ModelUtils.getModelDaoImplFullName(model));
			bean_tag.setAttributeValue("parent", "ahdi");
		} else {
			bean_tag = root.addElement("bean");
			bean_tag.setAttributeValue("id",
					ModelUtils.getModelIDaoSimpleName(model));
			bean_tag.setAttributeValue("class",
					ModelUtils.getModelDaoImplFullName(model));
			bean_tag.setAttributeValue("parent", "ahdi");
		}
	}

	public static void compilerFile(File[] makeFile) {
		try {
			complierFile(makeFile[0], makeFile[1]);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static File getClassTruePathFile(Model model,
			String queryModelSavePath) {
		String newQueryModelSavePath = queryModelSavePath.replaceAll("\\.",
				"\\" + File.separator);
		String classTruePath = ModelUtils.getWebClassPath(model)
				+ newQueryModelSavePath;

		File file = new File(classTruePath);
		return file;
	}

	/**
	 * 生成Model的QueryModel对象
	 * 
	 * @param model
	 * @throws FileNotFoundException
	 */
	public static void makeQueryModel(Model model) {
		String queryModelSavePath = prop.getProperty("queryModel.savePath");
		File file = getClassTruePathFile(model, queryModelSavePath);
		File[] makeFile = markQueryJModelJavaFile(file, queryModelSavePath,
				model);
		compilerFile(makeFile);
	}

	/**
	 * 生成Model的Dao接口
	 * 
	 * @param model
	 */
	public static void makeDaoInterface(Model model) {

		String queryModelSavePath = prop.getProperty("dao.savePath");
		File file = getClassTruePathFile(model, queryModelSavePath);
		File[] makeFile = markIDaoJavaFile(file, queryModelSavePath, model);
		compilerFile(makeFile);
	}

	/**
	 * 生成Model的Ebi接口
	 * 
	 * @param model
	 */
	public static void makeModelEbi(Model model) {
		String queryModelSavePath = prop.getProperty("ebi.savePath");
		File file = getClassTruePathFile(model, queryModelSavePath);
		File[] makeFile = markEbiJavaFile(file, queryModelSavePath, model);
		compilerFile(makeFile);
	}

	/**
	 * 生成Model的Ebo
	 * 
	 * @param model
	 */
	public static void makeModelEbo(Model model) {
		String queryModelSavePath = prop.getProperty("ebo.savePath");
		File file = getClassTruePathFile(model, queryModelSavePath);
		File[] makeFile = markEboJavaFile(file, queryModelSavePath, model);
		compilerFile(makeFile);
	}

	private static File[] markEboJavaFile(File file, String eboSavePath,
			Model model) {
		String eboName = ModelUtils.getSimpleEboName(model);
		StringBuilder sb = new StringBuilder();
		sb.append("package ").append(eboSavePath).append(";")
				.append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(ModelUtils.getModelFullName(model))
				.append(";").append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(ModelUtils.getQuerymodelFullName(model))
				.append(";").append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(ModelUtils.getEbiFullName(model))
				.append(";").append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(prop.getProperty("abstractEbo"))
				.append(";").append(CUtils.LINE_SEPARATOR);

		sb.append("public class ").append(eboName)
				.append(" extends AbstractEbo<")
				.append(ModelUtils.getModelSimpleName(model)).append(",")
				.append(ModelUtils.getQueryModelSimpleName(model))
				.append("> implements ")
				.append(ModelUtils.getEbiSimpleName(model)).append("{}");
		File queryModelJavaFile = new File(file, eboName + ".class");

		return writeToFile(sb.toString(), queryModelJavaFile);
	}

	private static File[] markEbiJavaFile(File file, String ebiSavePath,
			Model model) {

		String ebiName = ModelUtils.getEbiSimpleName(model);
		StringBuilder sb = new StringBuilder();
		sb.append("package ").append(ebiSavePath).append(";")
				.append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(ModelUtils.getModelFullName(model))
				.append(";").append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(ModelUtils.getQuerymodelFullName(model))
				.append(";").append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(prop.getProperty("ebi")).append(";")
				.append(CUtils.LINE_SEPARATOR);

		sb.append("public interface ").append(ebiName).append(" extends Ebi<")
				.append(ModelUtils.getModelSimpleName(model)).append(",")
				.append(ModelUtils.getQueryModelSimpleName(model))
				.append("> {}");
		File queryModelJavaFile = new File(file, ebiName + ".class");

		return writeToFile(sb.toString(), queryModelJavaFile);
	}

	/**
	 * 生成Model的DaoImpl
	 * 
	 * @param model
	 */
	public static void makeDaoImpl(Model model) {
		String queryModelSavePath = prop.getProperty("daoImpl.savePath");
		File file = getClassTruePathFile(model, queryModelSavePath);
		File[] makeFile = markDaoImplFile(file, queryModelSavePath, model);
		compilerFile(makeFile);
	}

	private static File[] markDaoImplFile(File file, String daoImplSavePath,
			Model model) {

		String daoImplName = model.getClass().getSimpleName() + "DaoImpl";
		StringBuilder sb = new StringBuilder();
		sb.append("package ").append(daoImplSavePath).append(";")
				.append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(ModelUtils.getModelFullName(model))
				.append(";").append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(ModelUtils.getQuerymodelFullName(model))
				.append(";").append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(ModelUtils.getModelDaoInterface(model))
				.append(";").append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(prop.getProperty("abstractDaoImpl"))
				.append(";").append(CUtils.LINE_SEPARATOR);

		sb.append("public class ").append(daoImplName)
				.append(" extends AbstractHibernateDaoImpl<")
				.append(ModelUtils.getModelSimpleName(model)).append(",")
				.append(ModelUtils.getQueryModelSimpleName(model))
				.append("> implements ")
				.append(ModelUtils.getModelIDaoSimpleName(model)).append("{}");
		File queryModelJavaFile = new File(file, daoImplName + ".class");

		return writeToFile(sb.toString(), queryModelJavaFile);
	}

	/**
	 * 生成Model对象的Dao的接口文件
	 * 
	 * @param file
	 * @param queryModelSavePath
	 * @param model
	 * @return
	 */
	private static File[] markIDaoJavaFile(File file,
			String queryModelSavePath, Model model) {
		String iDaoName = model.getClass().getSimpleName() + "Dao";

		StringBuilder sb = new StringBuilder();
		sb.append("package ").append(queryModelSavePath).append(";")
				.append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(ModelUtils.getModelFullName(model))
				.append(";").append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(ModelUtils.getQuerymodelFullName(model))
				.append(";").append(CUtils.LINE_SEPARATOR);
		sb.append("import ").append(prop.get("superDao")).append(";")
				.append(CUtils.LINE_SEPARATOR);
		sb.append("public interface ").append(iDaoName).append(" extends Dao<")
				.append(model.getClass().getSimpleName()).append(",")
				.append(ModelUtils.getQueryModelSimpleName(model))
				.append(">{}");

		// 此时path文件已经指向了QueryModel的文件夹
		File queryModelJavaFile = new File(file, iDaoName + ".class");

		return writeToFile(sb.toString(), queryModelJavaFile);
	}

	/**
	 * 编译为字节码文件
	 * 
	 * @param makeFile2
	 * 
	 * @param javaFile
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 */
	private static void complierFile(File makeFile, File makeFile2)
			throws Exception {
		String fullQuanlifiedFileName = makeFile.getAbsolutePath();
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		OutputStream err = new FileOutputStream("err.txt");
		int result = compiler.run(null, null, err, fullQuanlifiedFileName);
		System.out.println(result);

		File newClassFile = new File(makeFile.getAbsolutePath().replaceAll(
				"java", "class"));
		if (makeFile2.exists()) {
			makeFile2.delete();
		}
		copyFile(newClassFile, makeFile2);

		System.out.println(fullQuanlifiedFileName);
		newClassFile.delete();
	}

	/**
	 * 文件复制
	 * 
	 * @param newClassFile
	 * @param makeFile2
	 * @throws IOException
	 */
	public static void copyFile(File newClassFile, File makeFile2)
			throws IOException {
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(
				newClassFile));
		BufferedOutputStream bos = new BufferedOutputStream(
				new FileOutputStream(makeFile2));
		int length = 0;
		while ((length = bis.read()) != -1) {
			bos.write(length);
		}
		bis.close();
		bos.flush();
		bos.close();
	}

	/**
	 * 创建QueryModel对象的.java文件
	 * 
	 * @param path
	 * @param package_path
	 * @param model
	 * @return
	 */
	private static File[] markQueryJModelJavaFile(File path,
			String package_path, Model model) {

		Class<?> clazz = model.getClass();
		String modelName = clazz.getSimpleName();
		int index = modelName.indexOf("Model");
		String queryModel = modelName.substring(0, index) + "QueryModel";

		StringBuilder sb = new StringBuilder();

		sb.append("package ").append(package_path)
				.append(CUtils.LINE_SEPARATOR).append(";");
		sb.append("import ").append(ModelUtils.getmodelPath(model)).append(".")
				.append(modelName).append(";").append(CUtils.LINE_SEPARATOR);

		sb.append("public class ").append(queryModel).append(" extends ")
				.append(modelName).append("{}");

		System.out.println(sb.toString());

		File queryModelJavaFile = new File(path, queryModel + ".class");

		return writeToFile(sb.toString(), queryModelJavaFile);

	}

	/**
	 * 想指定的文件总写入数据
	 * 
	 * @param string
	 * @param queryModelJavaFile
	 */
	private static File[] writeToFile(String content, File file) {
		File srcFile = null;
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			String srcPath = getSrcPath(file);
			srcPath = srcPath.replaceAll("class", "java");
			System.out.println("srcPath:" + srcPath);
			srcFile = new File(srcPath);
			if (!srcFile.exists()) {
				srcFile.createNewFile();
			}
			System.out.println("====srcFilePath :" + srcFile.getAbsolutePath());
			PrintWriter pw = new PrintWriter(srcFile, "utf-8");
			pw.print(content);
			pw.flush();
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("发生异常！");
		}
		return new File[] { srcFile, file };
	}

	public static String getSrcPath(File file) {
		String webPath = File.separator + "WebRoot" + File.separator
				+ "WEB-INF" + File.separator + "classes";
		int index3 = file.getAbsolutePath().indexOf(
				File.separator + "WebRoot" + File.separator + "WEB-INF"
						+ File.separator + "classes");
		System.out.println("index3:" + index3);
		String srcPath = file.getAbsolutePath().substring(0, index3)
				+ File.separator + "src"
				+ file.getAbsolutePath().substring(index3 + webPath.length());
		return srcPath;
	}
}
