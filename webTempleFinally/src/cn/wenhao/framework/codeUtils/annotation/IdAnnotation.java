package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IdAnnotation {// IdAnnotation

	String name() default "MAKE#";

	String column() default "";

	String type() default "";

	String access() default "";

	int length() default 255;

	String node() default "";

	// 注意_，需要换成-
	String unsaved_value() default "";

}
