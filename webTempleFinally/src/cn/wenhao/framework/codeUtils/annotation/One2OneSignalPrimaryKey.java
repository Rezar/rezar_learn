package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 定义某个字段与关联类的 一对一关系， 基于主键，可以用于单向或者双向
 * 
 * 此时，这个注解应该同 主键生成策略为 ：foreign
 * 
 * 	此时，配置了这个注解的Model对象不会手动生成主键，而是主键作为外键关联到关联对象的主键
 * 	此时，关联对象的主键用 被该主键注解的字段属性 指定
 * 
 * 	eg:
 * 	Person  和 IDCard 
 * 
 * 	在IDCard 的主键id上使用注解
 * 	“@PrimaryKey(generator = ”@Generator(class_ = "foreign", param = "person"))
 * 	private String id;
 * 
 * 	“@One2OneSignalPrimaryKey
 * 	private Person person;
 * 
 * 	生成的映射文件类似于：
 * 	
 * 	<id name="id">
		<generator class="foreign">
			<param name="property">customer</param>
		</generator>
	</id>
	<one-to-one name="customer" constrained="true" />
 * 
 * @author Administrator
 * 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface One2OneSignalPrimaryKey {

	String name() default "MAKE#";

	String class_() default "";

	boolean constrained() default true;
	
	String fetch() default "";

	String cascade() default "";

}
