package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <!-- xxx属性，我与Xxxr的一对多映射 -->
 * 
 * Waring : this annotation is use for configure one2many relative
 * @author Administrator
 * 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface One2ManySetMapped {

	SetMapped set() default @SetMapped;

	KeyMapped key() default @KeyMapped;

	One2ManyMapped one_to_many() default @One2ManyMapped;

}
