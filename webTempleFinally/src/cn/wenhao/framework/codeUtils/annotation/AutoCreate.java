package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Waring：this annotation is can not use for a general_Fileld,if want to configure a general_fidle,
 * 			you should use PropertyField  
 * 		this annotation is just use for a class which need auto create some useful file
 * 
 * @author Administrator
 * 
 */
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AutoCreate {

}
