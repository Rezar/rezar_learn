package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 定义某个字段与关联类的 一对一关系， 基于外键，单向
 * 
 * 	生成的映射文件类似于：
 * 		<many-to-one name="xxx" 
 * 			column="xxx" class="" 
 * 			unique="true" not-null="true" />
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface One2OneSignalForeignKey {

	String name() default "MAKE#";

	String column() default "MAKE#";

	String class_() default "";

	boolean unique() default true;

	boolean not_null() default true;

	String cascade() default "";

	String fetch() default "";

	boolean insert() default true;

	boolean lazy() default true;

	boolean update() default true;

}
