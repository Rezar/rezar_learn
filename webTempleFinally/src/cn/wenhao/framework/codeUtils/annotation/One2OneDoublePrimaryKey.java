package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 基于主键的一对一双向关联(
 * 	此时，该注解应该配置在提供主键id的一端
 *	eg:
 *	Person 和 IDCard 的关系
 *	一个人对应一张身份证，一张身份证对应一个人
 *	此时，可以将IDCard的主键设置为 外键(即Person的主键)
 *	此时，就需要将当前的注解配置在
 *	Person类中的 IDCard 字段上面
 *	
 *	保存的时候，由于Person 和 IDCard 为级联关系
 *	Person person = new Pserson();
 *	pserson.setId(xxxx);
 *	
 *	IdCard idCard = new IdCard();
 *
 *	idCard.setPerson(person);
 *	pserson.setIDCard(idCard);
 *	
 *	PersonEbi.create(person);
 * )
 * 
 * @author Administrator
 * 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface One2OneDoublePrimaryKey {

	String name() default "MAKE#";

	String fetch() default "";

	String cascade() default "all";
	
	boolean lazy() default false;

}
