package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Many2ManySetMapped {
	SetMapped set() default @SetMapped;

	KeyMapped key() default @KeyMapped;

	Many2ManyMapped many_to_many() default @Many2ManyMapped;
}
