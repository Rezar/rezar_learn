package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 配置Many2One的属性
 * 
 * @author Administrator
 * 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Many2OneMapped {

	String name() default "MAKE#";

	String column() default "MAKE#";

	String class_() default "";

	boolean unique() default false;

	boolean not_null() default true;

	String cascade() default "";

	String fetch() default "";

	boolean insert() default true;

	boolean lazy() default false;

	boolean update() default true;

}
