package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Many2ManyMapped {

	String class_() default "MAKE#";

	String column() default "MAKE#";

	String fetch() default "";

	boolean lazy() default false;

	boolean unique() default false;

	String order_by() default "";

}
