package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注意，在配置了Column注解后，该字段所有在数据库表中对应字段的属性信息不需要再在Property注解中配置
 * 
 * @author Administrator
 * 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PropertyField {
	GeneralField property() default @GeneralField();

	Column column() default @Column();
}
