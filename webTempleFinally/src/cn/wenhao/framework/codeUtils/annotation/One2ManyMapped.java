package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * waring : this annotation is could not use for mapped a one2many relative , 
 * 			if you want to mapped one2many relative , you should use One2ManySetMapped annotation
 * @author Administrator
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface One2ManyMapped {

	String class_() default "MAKE#";

}
