package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 基于外键的一对一双向关联
 * 
 * WithOne2One
 * 
 * 此时，与配置了该注解的Model的对象
 * 
 * @author Administrator
 * 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface One2OneDoubleForeignKey {

	String name() default "MAKE#";

	/* 与之关联的对象中的定义的当前对象的属性名 */
	String property_ref();

	String foreign_key() default "";

	String fetch() default "";

	String cascade() default "";

	boolean lazy() default false;

}
