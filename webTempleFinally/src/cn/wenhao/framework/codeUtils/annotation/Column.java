package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Waring：this annotation is can not use for a general_Fileld,if want to configure a general_fidle,
 * 			you should use PropertyField  
 * 
 * @author Administrator
 *
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {

	String name() default "";

	String check() default "";

	String default_() default "";

	String sql_type() default "";

	String index() default "";

	int length() default 255;

	String unique_key() default "";

	String precision() default "";

	boolean not_null() default false;

	boolean unique() default false;

}
