package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Waring：this annotation is can not use for a general_Fileld,if want to configure a general_fidle,
 * 			you should use PropertyField  
 * 
 * general field annotation,just configure property tag
 * 
 * @author Administrator
 * 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface GeneralField {
	String name() default "MAKE#";;

	String type() default "";

	String access() default "";

	String column() default "";

	String formula() default "";

	String generated() default "";

	String index() default "";

	boolean lazy() default false;

	int length() default 255;

	String node() default "";

	String scale() default "";

	String unique_key() default "";

	String precision() default "";

	boolean insert() default true;

	boolean not_null() default false;

	boolean unique() default false;

	boolean update() default true;

	boolean optimistic_lock() default true;

}
