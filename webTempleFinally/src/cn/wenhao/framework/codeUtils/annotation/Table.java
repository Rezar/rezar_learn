package cn.wenhao.framework.codeUtils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Table {

	// MAKE#指定为需要注解解释程序自定义
	String name() default "MAKE#";

	String table() default "MAKE#";

	boolean lazy() default true;

}
