package cn.wenhao.framework.exception;

public class TooManyAnnotationForFieldException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8167007861368897209L;

	public TooManyAnnotationForFieldException() {
		super();
	}

	public TooManyAnnotationForFieldException(String message, Throwable cause) {
		super(message, cause);
	}

	public TooManyAnnotationForFieldException(String message) {
		super(message);
	}

	public TooManyAnnotationForFieldException(Throwable cause) {
		super(cause);
	}

}
