package cn.wenhao.framework.exception;

public class PrimaryKeyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8167007861368897209L;

	public PrimaryKeyException() {
		super();
	}

	public PrimaryKeyException(String message, Throwable cause) {
		super(message, cause);
	}

	public PrimaryKeyException(String message) {
		super(message);
	}

	public PrimaryKeyException(Throwable cause) {
		super(cause);
	}

}
