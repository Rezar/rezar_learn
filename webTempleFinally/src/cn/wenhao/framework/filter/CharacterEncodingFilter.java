package cn.wenhao.framework.filter;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class CharacterEncodingFilter implements Filter {

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		final HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		System.out.println("OK");
		request.setCharacterEncoding("utf-8");
		arg2.doFilter( (HttpServletRequest) Proxy.newProxyInstance(CharacterEncodingFilter.class.getClassLoader(), request.getClass().getInterfaces(), new InvocationHandler() {
			
			@Override
			public Object invoke(Object proxy, Method method, Object[] args)
					throws Throwable {
				String name = method.getName();
				if(!"getParameter".equals(name)){
					return method.invoke(request, args);
				}
				if(!"get".equalsIgnoreCase(request.getMethod())){
					return method.invoke(request, args);
				}
				String value = (String) method.invoke(request, args);
				if(value == null){
					return null;
				}
				System.out.println("Filter:"+value);
				value = new String(value.getBytes("iso8859-1"),"utf-8");
				return value;
			}
		}), response);
	}
	

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}

}
