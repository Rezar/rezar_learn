package cn.wenhao.framework.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import cn.wenhao.framework.sessionFactory.HibernateSessionFactory;

public class SessionFilter implements Filter {

	private HibernateSessionFactory hsf;

	public void setHsf(HibernateSessionFactory hsf) {
		this.hsf = hsf;
	}

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {

		try {
			this.hsf.getSession();
			arg2.doFilter(arg0, arg1);
		} finally {
			this.hsf.closeSession();
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
