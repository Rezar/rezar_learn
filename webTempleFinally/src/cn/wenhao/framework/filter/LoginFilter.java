package cn.wenhao.framework.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import cn.wenhao.framework.model.Model;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter(
		urlPatterns = { 
				"/jsps/order/*", 
				"/jsps/cart/*"
		}, 
		servletNames = { 
				"OrderServlet", 
				"CartItemServlet"
		})
public class LoginFilter implements Filter {


	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		Model user = (Model) req.getSession().getAttribute("admin");
		System.out.println("登录过滤器开始工作！");
		if(user == null){
			req.setAttribute("code", "error");
			req.setAttribute("msg", "您还未登录,不能访问该资源!");
			req.getRequestDispatcher("/msg.jsp").forward(req, response);
			return;
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}


}
