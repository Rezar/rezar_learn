package cn.wenhao.framework.logger;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class LoggerForEbi {

	private Log log = LogFactory.getLog(this.getClass());

	@Around(value = "execution(* *..*Ebi.*(..))")
	public Object logAround(ProceedingJoinPoint joinPoint) {

		Object result = null;
		log.info("The method " + joinPoint.getSignature().getName()
				+ "() begins with " + Arrays.toString(joinPoint.getArgs()));
		try {
			result = joinPoint.proceed(joinPoint.getArgs());
			log.info("The method " + joinPoint.getSignature().getName()
					+ "() ends with " + Arrays.toString(joinPoint.getArgs()));
		} catch (Throwable e) {
			System.out.println("发生异常了！");
			log.error(
					"Illegal argument " + Arrays.toString(joinPoint.getArgs())
							+ " in " + joinPoint.getSignature().getName()
							+ "()", e);
			e.printStackTrace();
		}
		return result;
	}

}
