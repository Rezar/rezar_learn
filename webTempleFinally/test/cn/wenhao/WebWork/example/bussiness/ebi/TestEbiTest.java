package cn.wenhao.WebWork.example.bussiness.ebi;

import static org.junit.Assert.fail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.wenhao.WebWork.example.model.CustomerModel;
import cn.wenhao.WebWork.example.model.TestModel;
import cn.wenhao.WebWork.example.model.UserModel;
import cn.wenhao.framework.utils.Utils;

public class TestEbiTest {

	TestEbi tEbi = null;
	CustomerEbi cEbi = null;
	static String configFilename = "log4j.properties";
	static Log logger = (Log) LogFactory.getLog(TestEbiTest.class);
	/*static{
		PropertyConfigurator.configure(configFilename);
	}*/

	@Test
	public void testCreate() {
		ApplicationContext ac = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		tEbi= (TestEbi) ac.getBean("TestEbi");
		cEbi = (CustomerEbi) ac.getBean("CustomerEbi");
		System.out.println(tEbi);
		
		TestModel model = new TestModel();
		
		CustomerModel customer = new CustomerModel();
//		customer.setId(Utils.uuid());
		
		model.setCustomer(customer );
		customer.setTestModel(model);
		
		cEbi.create(customer);
		logger.info("Model对象创建成功！");
	}

	@Test
	public void testDelete() {
//		ApplicationContext ac = new ClassPathXmlApplicationContext(
//				"applicationContext.xml");
//		tEbi= (TestEbi) ac.getBean("TestEbi");
//		System.out.println(tEbi);
	}

	@Test
	public void testGetSingleQM() {
		
		ApplicationContext ac = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		UserEbi me  = (UserEbi) ac.getBean("UserEbi");
		UserModel model = new UserModel();
		model.setId(Utils.uuid());
		model.setEmail("1916851825@qq.com");
		me.create(model);
		
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

}
