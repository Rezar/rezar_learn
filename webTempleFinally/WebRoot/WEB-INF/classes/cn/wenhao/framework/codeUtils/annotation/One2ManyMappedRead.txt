配置实体对象间的一对多的关系

 解释用例：
 	OneModel 和 ManyModel
 	
 一：
 	基于外键的单向多对一关系映射
 	
 	在多的一方添加外键咧，使用
 	<many-to-one name="" column="" />
 	
 	在ManyModel类中：
 	
 	@Many2OneMapped(column = "one_id", name = "one")
 	private OneModel one;
 	
 	=============ManytModel.hbm.xml=============
 	 <many-to-one not-null="true" name="one" column="one_id"/>
 
 二：
 	基于外键的一对多惯性映射
 	在一的一方，使用
 	<set name="">
		<key column=""></key>
		<one-to-many class=""/>
	</set>
	
	说明；set中的name：为当前该字段的属性名称
		key中的column:多的一方在关联一的一方时使用的外键咧的列名
		one-to-many中的class:多的一方的类型
		
	在OneModel类中：
	@One2ManySetMapped(key = @KeyMapped(column = "one_id"), set = @SetMapped)
	private Set<ManyModel> manys = new HashSet<ManyModel>();
	
	说明：其中的key和set以及one_to_many皆可以不配置，此时，程序会使用默认值
		默认值分别为：
			set中：name 为当前的属性名称
			key中：column为当前的一的一方的类的简单名称+"_id"
			one-to-many中：class 默认为当前该集合属性中的泛型元素的类型
	=============OneModel.hbm.xml=============
			<set name="manys">
			    <key column="OneModel_id"/>
			    <one-to-many class="cn.wenhao.HHB.example.model.ManyModel"/>
			 </set>
			 
三：
	基于外键的双向一对多或多对一关联
	综合以上配置即可
 